# Libgirl Bot

## Introduction

Libgirl Avatar project. Product Name = Libgirl Bot.

## 準備作業

### **HTTPS webhook**

* Please make a proxy direct to localhost:5000
* You must provide SSL for webhook callback

#### Local Environment(in this example)

* You can use [localtunnel](https://localtunnel.github.io/www/)
* cp .env.example .env (然後修改 .env 裡的環境變數)

### **facebook 開發者後台取得 & 設定臉書粉專 Token**

* 現有或創立紛絲專頁(分類建議選擇：網路應用程式)。
* 從 https://developers.facebook.com/apps 建立apps並開啟messange功能(包括Facebook NPL)。
* apps控制台-> 產品-> messange-> 設定 取得 token(中文網頁：存取權杖)。這個 verify token 會跟程式裡面的 verify token 一樣，作為驗證使用。

```shell script
cd /path/to/libgirlavatar
vim config/default.json
```

* 請在 `config/default.json` 檔案中加入以下設定：(除掉中括號)

```json
$ vim ./default.json
{
  "facebook": {
    "page": {
      "access_token": "[這邊插入 Access Token]"
    }
  }
}
```

*Addition:如果要開啟"Get Started"或是"開始使用"的初訪互動，一樣使用這個token並利用Shell執行：(除掉中括號)
*Addition:開啟"persistent_menu"用以生成選單(互動一樣屬postback event)

```json
$curl -X POST -H "Content-Type: application/json" -d '{ 
  "get_started":{ 
    "payload":"getStarted"
  },
  "persistent_menu": [
    {
      "locale": "default",
      "composer_input_disabled": false,
      "call_to_actions": [
        {
            "type": "postback",
            "title": "總統大選日 President election date",
            "payload": "electionDate"
        },
        {
            "type": "postback",
            "title": "分享萌卷醬 QR Code",
            "payload": "shareAvatar"
        },
        {
            "type": "postback",
            "title": "主選單",
            "payload": "選單"
        }
      ]
    }
  ]
}' "https://graph.facebook.com/v5.0/me/messenger_profile?access_token=[這邊插入 Access Token]"

${"result":"success"} #代表成功啟用 
```

### 建立 LINE Channel 和取得相關 secret 和 token
- 登入 [LINE Developer Console](https://developers.line.biz/console/)
- 左側點選 Provider -> 頁面 create -> 填寫相關資料 
- 左側點選新建好的Provider -> 頁面 create new channel -> 填寫相關資料
- 左側點選新建好的Provider -> 點選新建好的 channel -> 
    - 點選上排選單 basic setting -> 取得 channel secret 
    - 點選上排選單 Massaging API ->
        - LINE Official Account features 全部 disable
        - 取得 channel access token
        - webhook url 填入 [服務起起來的url]/line
- 請在 `config/default.json` 檔案中加入以下設定：(除掉中括號)
```json
{
  "facebook": {
    "page": {
      "access_token": "[這邊插入 Access Token]"
    }
  },
  "port": 5000,
  "line": {
    "channel_access_token": "[這邊插入 channel access token]",
    "channel_secret": "[這邊插入 line channel secret ]
   }
}
```
- 進行以下指令建立主選單。


建立
``` shell script
curl -v -X POST https://api.line.me/v2/bot/richmenu \
-H 'Authorization: Bearer [Channel access token]' \
-H 'Content-Type: application/json' \
-d \
'[pls copy utils/line/uploadMenu.json]'
```
建立後得到[richmenu id]，然後upload image
```shell script
curl -X POST https://api-data.line.me/v2/bot/richmenu/[richmenu id]/content \
-H 'Authorization: Bearer [Channel access token]' \
-H "Content-Type: image/png" \
-T [path of image file]
```
將建立的rich menu設為預設
```shell script
curl -X POST https://api.line.me/v2/bot/user/all/richmenu/[richmenu id] \
-H 'Authorization: Bearer [Channel access token]'
```
[detailed API doc](https://developers.line.biz/en/reference/messaging-api/#create-rich-menu)

### **Cloud Natural Language API金鑰設定**

* 設定環境變數 `GOOGLE_APPLICATION_CREDENTIALS` 為放credential JSON 檔的路徑。注意：重啟 Shell 要再設定一次。

```shell script
export GOOGLE_APPLICATION_CREDENTIALS="/your/path/credential.json"
```

### **安裝 NPM, YARN 以及必要套件**

* 安裝[NPM](https://github.com/nodejs-tw/nodejs-wiki-book/blob/master/zh-tw/node_npm.rst)。
* NPM 用於監聽管理。`提醒：用NPM安裝軟體可能會造成Packages間相容及環境管理上的衝突。`
* 安裝[YARN](https://yarnpkg.com/zh-Hant/docs/install#debian-stable)。
* YARN 用於套件(Packages)管理。執行下列指令安裝所需packages。

``` shell script
$ cd /path/to/libgirlavatar
$ yarn install
```

### **Check redis**

* Check and launch redis listening to default redis port 6379(default).
```
$ redis-server -v
$ redis-server &
```

### **執行 NPM Server**

* 預設聽 5000 port。
* 保持開啟。

```
$ npm start
```
* 成功啟動後，Terminal得到提示 。

```
> Ready on http://localhost:5000
```
### **取得代理伺服器**

* 啟動 localtunnel 或是 ngrok。

```
$ lt -p 5000

or

$ ./ngrok http 5000
```
* 至 https://developers.facebook.com ，並將類似下列的結果的 URL [https://XXX-XXX.localtunnel.me] 設定為 Messenger 後台的 Callback URL。
* 保持開啟。
```
your url is: https://XXX-XXX.localtunnel.me
```
### **測試功能及更新程式碼**

* NPM執行期間若有檔案變動(包括服務主程式或者重新生成載入存取權杖)，NPM會自動重啟並生效。
* NPM Terminal同時具有log功能。

### **AWS Credential 設定**
- 設定環境變數 `AWS_ACCESS_KEY_ID` 和 `AWS_SECRET_ACCESS_KEY` 讓訊息存到 AWS DynamoDB。注意：重啟 Shell 要再設定一次。
```
export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>
export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
```

## Deploy to AWS Lambda

### AWS Lambda 架構
#### 流程
1. ApiGateway 收到 Request 並通知 SNS
1. SNS 發佈一個訊息到主題 new-message，表示收到一個新訊息或 Postback
1. SNS 通知並觸發所有有訂閱 new-message 的 Lambda，目前是兩隻 messenger-response, messenger-imitate-typing

#### Lambda functions 介紹
- 分成 staging 和 production 兩個環境，  
有 staging 結尾的就是測試用，連到 Avatarcy，  
其他的就是 production 或是共用，連到 Libgirl
- messenger-webhook: 負責驗證 FB 的 callback
- messenger-response: 負責處理訊息
- messenger-imitate-typing: 負責模擬打字和已讀

### Compile typescript and zip

#### Put Google Application Credentials into dist/

Please put credential JSON files as ```dist/libgirl-avatar-[YOUR-CREDENTIAL-HASH].json```

#### Then execute commands to compile & zip

If there's no change on dependencies (i.e. there will be no change in node_modules), run `./run_tsc.sh <cmd-for-lambda-function>`, e.g. `./run_tsc.sh response` for `message-response`.
If there's change on dependencies:

1. run `./run_tsc.sh modules` and `./run_tsc.sh <cmd-for-lambda-function>`, or compile & zip everything by `./run_tsc.sh` or `./run_tsc.sh all`.
2. upload the `nodejs.zip` to S3 and update the Layer nodejs.

for all the commands available, pls check the comments in `./run_tsc.sh`.

### Deploy the code to AWS Lambda

#### Prerequisite

Please install aws-cli and set credentials in ~/.aws/credentials as your IAM account.

#### Steps

##### Environment variables settings on AWS Lambda functions

###### Webhook verification lambda function - http get

```sh
verify_token=[VERIFY-TOKEN-WHEN-SETTING-WEBHOOK-FOR-FB-MESSENGER-APP]
```

###### Main Webhook lambda function - http post

```sh
GOOGLE_APPLICATION_CREDENTIALS=libgirl-avatar-[YOUR-CREDENTIAL-HASH].json
NODE_ENV=[production-或是不設此變數]
MESSENGER_PAGE_ID=
MESSENGER_ACCESS_TOKEN=[FB-MESSENGER-APP-ACCESS-TOKEN-也就是權杖]
MESSENGER_APP_ID=
MESSENGER_APP_SECRET=
MESSENGER_VERIFY_TOKEN=[Custom verify token]
LINE_ACCESS_TOKEN=
LINE_CHANNEL_SECRET=
COUPON_URL=[兌換折價卷的網址]
```

##### Then update Lambda functions

With CLI

```sh
$ aws lambda update-function-code \
    --function-name [WEBHOOK-VERIFIVATION-LAMBDA-FUNCTION-NAME] \
    --zip-file fileb://dist/fb-verify-webhook.zip
```

```sh
aws lambda update-function-code \
  --function-name [WEBHOOK-LAMBDA-FUNCTION-NAME] \
  --zip-file fileb://dist/fb-message-webhook.zip
```

```sh
aws lambda update-function-code \
  --function-name [WEBHOOK-LAMBDA-FUNCTION-NAME] \
  --zip-file fileb://dist/fb-imitate-typing.zip
```

Or you can use web console to upload fb-verify-webhook.zip or fb-message-webhook.zip accordingly.

### Set the role of libgirl bot by referral link

#### Line

By [URL Scheme](https://developers.line.biz/en/docs/messaging-api/using-line-url-scheme/#sending-text-messages) sending text message, append the text that can trigger the role-switch:
line://oaMessage/@054mrrnn/?我空虛寂寞覺得冷萌萌子陪陪我
or encode the text:
line://oaMessage/@054mrrnn/?%E6%88%91%E7%A9%BA%E8%99%9B%E5%AF%82%E5%AF%9E%E8%A6%BA%E5%BE%97%E5%86%B7%E8%90%8C%E8%90%8C%E5%AD%90%E9%99%AA%E9%99%AA%E6%88%91

#### Messenger
By [m.me Links](https://developers.facebook.com/docs/messenger-platform/discovery/m-me-links), append the command to be triggered as the `ref` parameter.
http://m.me/libgirldotcom?ref=CMD_SUMMON_GIRLFRIEND
