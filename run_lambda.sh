#!/usr/bin/env bash
# aws lambda create-function \
#   --function-name test-function \
#   --zip-file fileb://dist/function.zip \
#   --handler lambda_verify_webhook.handler \
#   --runtime nodejs10.x \
#   --role arn:aws:iam::978018593471:role/lambda-cli-role

aws lambda update-function-code \
  --function-name message-repeater \
  --zip-file fileb://dist/fb-message-webhook.zip
