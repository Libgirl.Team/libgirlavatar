console.log('getting bottender.config.js');
console.log(process.env.REDIS_PORT);
console.log(process.env.REDIS_HOST);
module.exports = {
    session: {
        driver: process.env.SESSION_DRIVER,
        stores: {
            memory: {
                maxSize: 500,
            },
            file: {
                dirname: '.sessions',
            },
            redis: {
                port: process.env.REDIS_PORT,
                host: process.env.REDIS_HOST,
                password: 'auth',
                db: 0,
            },
            mongo: {
                url: 'mongodb://localhost:27017',
                collectionName: 'sessions',
            },
        },
    },
    initialState: {},
    channels: {
        messenger: {
            enabled: true,
            path: '/messenger',
            pageId: process.env.MESSENGER_PAGE_ID,
            accessToken: process.env.MESSENGER_ACCESS_TOKEN,
            appId: process.env.MESSENGER_APP_ID,
            appSecret: process.env.MESSENGER_APP_SECRET,
            verifyToken: process.env.MESSENGER_VERIFY_TOKEN,
        },
        line: {
            enabled: true,
            path: '/line',
            accessToken: process.env.LINE_ACCESS_TOKEN,
            channelSecret: process.env.LINE_CHANNEL_SECRET,
        },
        telegram: {
            enabled: false,
            path: '/webhooks/telegram',
            accessToken: process.env.TELEGRAM_ACCESS_TOKEN,
        },
        slack: {
            enabled: false,
            path: '/webhooks/slack',
            accessToken: process.env.SLACK_ACCESS_TOKEN,
            verificationToken: process.env.SLACK_VERIFICATION_TOKEN,
        },
        viber: {
            enabled: false,
            path: '/webhooks/viber',
            accessToken: process.env.VIBER_ACCESS_TOKEN,
            sender: {
                name: 'xxxx',
            },
        },
    },
};
