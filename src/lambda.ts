import * as http from "http";
import * as net from "net";
import { bottender } from "bottender";

const app = bottender({ dev: false });
const handle = app.getRequestHandler();

function composeHeaders(headerString: string) {
    return headerString.split(',').reduce((headers: any, each: any) => {
        const [key, ...value] = each.split('=');
        headers[key.trim().toLowerCase()] = value.join('=').trim();
        return headers;
    }, {});
}

function createUrl(req: any) {
    if (req.headers['x-hub-signature']) {
        return '/messenger';
    }
    if (req.headers['x-line-signature']) {
        return '/line';
    }
    throw Error('Unknown platform');
}

function createRequest(sns: any) {
    const req: any = new http.IncomingMessage(new net.Socket());
    req.method = 'POST';
    req.body = JSON.parse(sns.Message.slice(1, -1));
    req.rawBody = sns.Message.slice(1, -1);

    const headerString = sns.MessageAttributes.Headers.Value.slice(2, -2);
    req.headers = composeHeaders(headerString);
    req.url = createUrl(req);

    return req;
}

exports.handler = (event: any) => {
    const sns = event.Records[0].Sns;
    console.log(sns);
    app.prepare().then(() => {
        const req = createRequest(sns);
        const res = new http.ServerResponse(req);
        handle(req, res).then(() => console.log('DONE')).catch((e: any) => console.error(e));
    });
};

