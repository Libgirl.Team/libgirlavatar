const bodyParser = require('body-parser');
const express = require('express');
const { bottender } = require('bottender');

const app = bottender({
    dev: process.env.NODE_ENV && (process.env.NODE_ENV !== 'production'),
});

export const server = express();
console.log('in server.ts.');

export const preparedApp = app.prepare().then(() => {
    server.use(
        bodyParser.json({
            verify: (req: any, _: any, buf: any) => {
                req.rawBody = buf.toString();
            },
        })
    );

    server.get('/api', (req: any, res: any) => {
        res.json({ ok: true });
    });

    server.all('*', (req: any, res: any) => {
        console.log('server.ts get request');
        return app.getRequestHandler()(req, res);
    });    
});

console.log('server.ts preparedApp:');
console.log(app);
