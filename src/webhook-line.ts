import config from 'config';
import express from 'express';
import { Request, Response, NextFunction } from 'express';
import { 
  Client, ClientConfig, MiddlewareConfig,
  middleware as lineMiddleware, 
  TextMessage, StickerMessage, ImageMessage, VideoMessage
}  from '@line/bot-sdk'

const router = express.Router()

const clientConfig: ClientConfig = {
    channelAccessToken: config.get('line.channel_access_token'),
    channelSecret: config.get('line.channel_secret'),
};

const middlewareConfig: MiddlewareConfig = {
  channelAccessToken: config.get('line.channel_access_token'),
  channelSecret: config.get('line.channel_secret'),
};

const asycnMiddleWare:(fn: any) =>  (req: Request, res: Response, next: NextFunction) => void = (fn: any) =>  (req: Request, res: Response, next: NextFunction) => {
  Promise
    .resolve(fn(req, res, next))
    .catch(next)  
}

// Create LINE SDK client
const client = new Client(clientConfig)

const handleEvent = async (event:any):Promise<any> =>{
  console.log(`Incoming event: `, event)
  // For line verify wehook
  if (event.replyToken === '00000000000000000000000000000000' || event.replyToken === 'ffffffffffffffffffffffffffffffff') {
    return Promise.resolve(null);
  }
  // TODO: follow event
  // For text event
  if (event.type === 'message' && event.message.type === 'text') {
    const replyText :TextMessage = { type: 'text', text: event.message.text };
    return client.replyMessage(event.replyToken, replyText);
  }
  // For sticker event
  if (event.type === 'message' && event.message.type === 'sticker') {
    const replySticker :StickerMessage = { type: 'sticker', packageId: '11538', stickerId: '51626501' };
    return client.replyMessage(event.replyToken, replySticker);
  }
  // For image event
  if (event.type === 'message' && event.message.type === 'image') {
    const replyImage :ImageMessage= { 
      type: 'image', 
      originalContentUrl: `https://media.giphy.com/media/5GoVLqeAOo6PK/giphy.gif`,
      previewImageUrl: `https://media.giphy.com/media/5GoVLqeAOo6PK/giphy.gif`
    };
    return client.replyMessage(event.replyToken, replyImage);
  }
  return Promise.resolve(null);
}

const handleEvents = async (req: Request, res: Response)=>{
  try {
    const result = await Promise.all(req.body.events.map(handleEvent))
      console.log(`result: `, result)
    res.json(result)
  } catch (error) {
    console.error(error.message);
    res.status(500).end();
  }
}

router.post('/',lineMiddleware(middlewareConfig), asycnMiddleWare(handleEvents));

export default router





