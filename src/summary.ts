import { calculateAllCount, scheduleUpdateSessions, scheduleUpdateUserCanShare } from "./utils/history";
import SummaryRepository from "./repositories/summary-repository";
import { countCoupon, countRedeemed, listCouponEvent } from "./utils/coupon";
import { allUrl, countUrlsClicks } from "./utils/redirect-url";

const summaryRepository = new SummaryRepository;

const handler = async function (event: any) {
    console.log(event);
    if (event.action === 'scheduleUpdateSessions') {
        await scheduleUpdateSessions();
        return;
    }
    if (event.action === 'getSessionAndSwitchTurn') {
        const data = await calculateAllCount();
        await summaryRepository.saveDashboard('SESSIONS', data);
        return;
    }
    if (event.action === 'scheduleUpdateUserCanShare') {
        await scheduleUpdateUserCanShare();
        return;
    }

    let data: any = null;
    const parameters = event.queryStringParameters;
    const action = parameters?.action;
    if (action === 'query_sessions') {
        const result: any = await summaryRepository.getDashboard('SESSIONS');
        data = result?.data;
    }
    if (action === 'query_coupon') {
        data = await countCoupon(parameters.event, parameters.start, parameters.end);
    }
    if (action === 'list_coupon_event') {
        data = await listCouponEvent();
    }
    if (action === 'count_urls_clicks') {
        data = await countUrlsClicks();
    }
    if (action === 'list_redirect_urls') {
        data = await allUrl();
    }
    if (action === 'count_redeemed') {
        data = await countRedeemed();
    }

    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify({ data })
    };
};

export { handler };
