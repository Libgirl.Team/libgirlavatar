import {pushQueue} from './queue-pusher';

exports.handler = (event: any, context: any) => {
    console.log('lambda-queue-pusher start.');
    pushQueue();
};
