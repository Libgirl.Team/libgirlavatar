import recmds from './strategies/zh/recmd.json'
import { incrementUrlCount, isValid, saveUserClick } from "./utils/redirect-url";
import { isStaff } from "./utils/history";

function errorResponse() {
    return {
        statusCode: 422,
        body: '抱歉，出錯囉',
    };
}

function redirect(url: any) {
    return {
        statusCode: 307,
        headers: {
            location: encodeURI(url),
        },
    };
}

/**
 * Redirect to url in recmds
 * if token is invalid, only increment count not save user click
 */
exports.handler = async (sourceEvent: any) => {
    const input = sourceEvent.queryStringParameters || {};
    console.log(input);
    const { id, user, token } = input;

    if (!id) {
        return errorResponse();
    }

    const url = recmds.find(e => e.event === id)?.url;
    if (!url) {
        return errorResponse();
    }

    if (isStaff(user)) {
        return redirect(url);
    }

    const promises: any[] = [incrementUrlCount(id)];

    if (user && token && isValid(user, id, token)) {
        promises.push(saveUserClick(user, id));
    }

    await Promise.all(promises);

    return redirect(url);
};
