import { GoogleNlpAnalysis } from './utils/google';
import { TextInMsg, CmdInMsg, User, OMsg, QuickReply } from './strategies/definitions';

// take InMsg & call APIs to send messages back
// platform dedecated API should be wraped in in advance.
export interface Replier {
    forText: (
        inMsg: TextInMsg,
        preOutMsgs: OMsg[],
    ) => Promise<string[]>,

    forCmd: (
        inMsg: CmdInMsg,
        preOutMsgs: OMsg[],
    ) => Promise<string[]>,
    
    forNonText: (
        user: User,
        preOutMsgs: OMsg[],
    ) => Promise<string[]>,
}

// a Strategy do:
// 1. check if the inMsg match the condition with googleNlpAnalysis.
// 2. if not match then cal falseCallback
// 3. finally return Promise<string[]>.
export interface TextStrategyOpt {
    googleNlpAnalysis?: GoogleNlpAnalysis,
    falseCallback?: StrategyForText,
    quickReplies?: QuickReply[]
}

export type StrategyForText = (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => Promise<string[]>;

// use a probability to decide whether to perform a strategy.
export function rndWrapForText(
    probability: number,
    strategy: StrategyForText,
): StrategyForText {
    return async (
        inMsg: TextInMsg,
        sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
        preOutMsgs: OMsg[],
        opt?: TextStrategyOpt
    ) => {
        if (Math.random() < probability) {
            return strategy(inMsg, sendAPI, preOutMsgs, opt);
        } else if (opt?.falseCallback) {
            return opt.falseCallback(inMsg, sendAPI, preOutMsgs, { googleNlpAnalysis: opt?.googleNlpAnalysis});
        } else {
            return new Promise((resolve) => { resolve(['ok']); })
        }
    }
}

export type StrategyForCmd = (
    inMsg: CmdInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    falseCallback?: StrategyForCmd
) => Promise<string[]>;

export interface NonTextStrategyOpt {
    falseCallback?: StrategyForNonText,
    quickReplies?: QuickReply[]
}

export type StrategyForNonText = (
    user: User,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: NonTextStrategyOpt
) => Promise<string[]>;

export async function replyForText(
    inMsg: TextInMsg,
    strategies: StrategyForText[],
    defaultStrategy: StrategyForText,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    quickReplies?: QuickReply[]
) {
    const appliedStrategies = strategies.reduceRight(
        (acc, fn) => {
            return (
                inMsg: TextInMsg,
                sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
                preOutMsgs: OMsg[],
                opt?: TextStrategyOpt
            ) => fn(inMsg, sendAPI, preOutMsgs, { falseCallback: acc,
                                                  googleNlpAnalysis: opt?.googleNlpAnalysis,
                                                  quickReplies});
        },
        (inMsg: TextInMsg,
         sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
         preOutMsgs: OMsg[],
         opt?: TextStrategyOpt) => defaultStrategy(inMsg, sendAPI, preOutMsgs, {
             googleNlpAnalysis: opt?.googleNlpAnalysis,
             quickReplies,
         })
    );
    return appliedStrategies(inMsg, sendAPI, preOutMsgs);
}

// (x) => f1(x)
// (x) => f1(x, f2)
// (x) => f1(x, f2(x, f3))

export async function replyForCmd(
    inMsg: CmdInMsg,
    strategies: StrategyForCmd[],
    defaultStrategy: StrategyForCmd,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
) {
    const appliedStrategies = strategies.reduceRight(
        (acc, fn) => {
            return (
                inMsg: CmdInMsg,
                sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
                preOutMsgs: OMsg[],
            ) => fn(inMsg, sendAPI, preOutMsgs, acc);
        },
        defaultStrategy
    );
    return appliedStrategies(inMsg, sendAPI, preOutMsgs);
}

export async function replyForNonText(
    user: User,
    strategies: StrategyForNonText[],
    defaultStrategy: StrategyForNonText,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    quickReplies?: QuickReply[]
) {
    const appliedStrategies = strategies.reduceRight((acc, fn) => {
        return (
            user: User,
            sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
            preOutMsgs: OMsg[],
        ) => fn(user, sendAPI, preOutMsgs, {falseCallback: acc, quickReplies});
    }, defaultStrategy);
    return appliedStrategies(user, sendAPI, preOutMsgs);
}

export async function finishStrategyForCmd(
    inMsg: CmdInMsg,
    sendAPI: (outMsgs: OMsg[] ) => Promise<string[]>,
    preOutMsgs: OMsg[],
    falseCallback?: StrategyForCmd
):Promise<string[]> {
    if (falseCallback) {
        return await falseCallback(inMsg, sendAPI, preOutMsgs);
    } else {
        return new Promise<string[]>((resolve) => { resolve(['ok']); });
    }
}

export async function finishStrategyForNonText(
    user: User,
    sendAPI: (outMsgs: OMsg[] ) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: NonTextStrategyOpt
):Promise<string[]> {
    if (opt?.falseCallback) {
        return await opt.falseCallback(user, sendAPI, preOutMsgs, { quickReplies: opt?.quickReplies });
    } else {
        return new Promise<string[]>((resolve) => { resolve(['ok']); });
    }
}

export async function finishStrategyForText(
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt,
    newGglNlp?: GoogleNlpAnalysis
): Promise<string[]> {
    if (opt?.falseCallback) {
        return opt.falseCallback(
            inMsg,
            sendAPI,
            preOutMsgs,
            { googleNlpAnalysis: newGglNlp ? newGglNlp : opt.googleNlpAnalysis,
              quickReplies: opt.quickReplies }
        );
    } else {
        return new Promise((resolve) => { resolve(['ok']); });
    }
}

