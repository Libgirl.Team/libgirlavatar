import { getMessageModel } from "../utils/models";
import { MessageModel } from "../strategies/definitions";

/**
 * Example:
 * const userRepository = new UserRepository();
 * const result = await userRepository.getMessages(chatId, start, end);
 */
export default class UserRepository {
    private _model: any;

    constructor() {
        this._model = getMessageModel();
    }

    /**
     * Get user history messages
     * @param chatId
     * @param start
     * @param end
     */
    getMessages(chatId: string, start: number, end: number): Promise<MessageModel[]> {
        return this._model
            .query('chatId').eq(chatId)
            .where('time').between(start, end).all().exec();
    }
}
