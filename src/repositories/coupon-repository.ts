import { getUserState, listUserState, saveUserState, scanState } from "../utils/state";
import { CouponContent, UserCoupon } from "../strategies/definitions";
import SummaryRepository from "./summary-repository";

type CouponState = { userId: string, data: CouponContent };

export default class CouponRepository {
    private _eventPrefix = 'COUPON';
    private _serialPrefix = 'SERIAL';
    private _rangeKey = 'COUNT';
    private readonly _summary: SummaryRepository;

    constructor() {
        this._summary = new SummaryRepository();
    }

    async all(): Promise<UserCoupon[] | undefined> {
        const result = await scanState(`${this._eventPrefix}-`);
        return (<CouponState[]>result)?.map(this.itemsToCoupon);
    }

    async list(userId: string, event: string): Promise<UserCoupon[] | undefined> {
        const result = await listUserState(userId, `${this._eventPrefix}-${event}`);
        return (<CouponState[]>result)?.map(this.itemsToCoupon);
    }

    async find(userId: string, event: string, serial: string): Promise<UserCoupon | undefined> {
        const userStateData = await this.getUserCouponState(userId, event, serial);
        if (userStateData === undefined) {
            return undefined;
        }
        return {
            userId: userId,
            ...<CouponContent>userStateData,
        };
    }

    create(userId: string, event: string, serial: string, token: string) {
        return saveUserState(userId, `${this._eventPrefix}-${event}#${this._serialPrefix}-${serial}`, {
            event,
            serial,
            token,
            isUsed: false,
            createdAt: Date.now(),
            updatedAt: Date.now()
        });
    }

    async use(userId: string, event: string, serial: string) {
        const couponData = await this.getUserCouponState(userId, event, serial);
        (<CouponContent>couponData).isUsed = true;
        (<CouponContent>couponData).updatedAt = Date.now();
        return saveUserState(userId, `${this._eventPrefix}-${event}#${this._serialPrefix}-${serial}`, couponData);
    }

    async increment(event: string) {
        const keys = { hashKey: this.hashKey(event), rangeKey: this._rangeKey };
        await this._summary.getOrCreate(keys, {
            hashKey: this.hashKey(event),
            rangeKey: this._rangeKey,
            event: event,
            count: 0,
        });
        return this._summary.update(keys, { $ADD: { count: 1 } });
    }

    async count(event: string) {
        const result: any = await this._summary.getRedeemedCouponCount(event);
        return { event: event, count: result?.count || 0 };
    }

    private itemsToCoupon(userState: CouponState) {
        return {
            userId: userState.userId,
            ...userState.data,
        };
    }

    private getUserCouponState(userId: string, event: string, serial: string) {
        return getUserState(userId, `${this._eventPrefix}-${event}#${this._serialPrefix}-${serial}`);
    }

    private hashKey(id: string) {
        return `${this._eventPrefix}-${id}`;
    }
}
