import { getSummaryModel } from "../utils/models";
import { RedirectUrlModel, SummaryModel } from "../strategies/definitions";

export default class SummaryRepository {
    private readonly _model: any;

    constructor() {
        this._model = getSummaryModel();
    }

    getOrNewScheduleSession() {
        return this.getOrCreate({ hashKey: 'SCHEDULE', rangeKey: 'UPDATE_SESSIONS' }, {
            hashKey: 'SCHEDULE',
            rangeKey: 'UPDATE_SESSIONS',
            executeTo: 0
        });
    }

    async getOrCreate(getParam: { rangeKey: string; hashKey: string }, createParam: SummaryModel): Promise<any> {
        const summary = await this._model.get(getParam);
        if (summary) {
            return summary;
        }
        return this._model.create(createParam);
    }

    getOrCreateShare(key: string): Promise<any> {
        return this.getOrCreate({ hashKey: 'SHARE', rangeKey: key }, {
            hashKey: 'SHARE',
            rangeKey: key,
            start: 0,
            end: 0,
            switchTurnThreshold: 15,
            sessionThreshold: 30
        });
    }

    saveDashboard(key: string, data: any) {
        const summary = new this._model({ hashKey: 'DASHBOARD', rangeKey: key, data });
        return summary.save();
    }

    getDashboard(key: string) {
        return this._model.get({ hashKey: 'DASHBOARD', rangeKey: key });
    }

    getUrlCount(urlId: string): Promise<RedirectUrlModel | undefined> {
        return this._model.get({ hashKey: `URL-${urlId}`, rangeKey: 'COUNT' });
    }

    getRedeemedCouponCount(event: string) {
        return this._model.get({ hashKey: `COUPON-${event}`, rangeKey: 'COUNT' });
    }

    saveUserCanShare(userId: string) {
        const summary = new this._model({ hashKey: 'CAN_SHARE', rangeKey: userId });
        return summary.save();
    }

    update(keys: { rangeKey: string; hashKey: string }, data: any) {
        return this._model.update(keys, data);
    }
}
