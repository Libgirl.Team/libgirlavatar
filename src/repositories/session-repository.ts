import { getSessionModel } from "../utils/models";
import { SessionModel } from "../strategies/definitions";

export default class SessionRepository {
    private readonly _model: any;

    constructor() {
        this._model = getSessionModel();
    }

    createOrUpdate(session: any, message: any): Promise<SessionModel> | undefined {
        if (!session) {
            return this._model.create({
                chatId: message.chatId,
                start: message.time,
                end: message.time,
                switchTurnCount: 0,
                lastMessageAt: message.time,
            });
        }
        if (session && message.time > session.end) {
            session.end = message.time;
            session.lastMessageAt = Math.max(message.time, session.lastMessageAt);
            return session.save();
        }
    }

    // For not querying all history message, set a reasonable start
    findLast(chatId: string, searchFrom: number, after: number): Promise<SessionModel> {
        return this._model.queryOne('chatId').eq(chatId)
            .where('start').ge(searchFrom)
            .filter('end').ge(after)
            .descending().exec();
    }

    addSwitchTurn(chatId: string, start: number, time: number) {
        return this._model.update({ chatId: chatId, start: start }, {
            $ADD: { switchTurnCount: 1 },
            $PUT: { lastMessageAt: time }
        });
    }

    all(): Promise<SessionModel[]> {
        return this._model.scan().all().exec()
    };
};
