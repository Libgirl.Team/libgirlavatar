import { getMessageModel } from "../utils/models";
import { MessageModel } from "../strategies/definitions";

export default class MessageRepository {
    private readonly _model: any;

    constructor() {
        this._model = getMessageModel();
    }

    save(messageModel: MessageModel) {
        const message = new this._model(messageModel);
        return message.save();
    }

    async all(start: number, end: number): Promise<any[]> {
        const result = await Promise.all([this.listByPlatform('FB', start, end), this.listByPlatform('LINE', start, end)]);
        return result.reduce((prev, curr) => [...prev, ...curr], [])
    }

    private async listByPlatform(platform: string, start: number, end: number) {
        const result: any = await this._model
            .query('platform').eq(platform)
            .where('time').between(start, end).all().exec();

        console.log(platform, ' message count:', result.length);
        return result;
    }
}
