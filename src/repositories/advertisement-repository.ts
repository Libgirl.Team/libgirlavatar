import { getSummaryModel } from "../utils/models";
import { SummaryModel } from "../strategies/definitions";

/**
 * Example:
 * const adRepo = new AdvertisementRepository();
 * const result = await adRepo.all();
 */
export default class AdvertisementRepository {
    private readonly _model: any;
    private _hashKey = 'ADVERTISEMENT';
    private _adPrefix = 'AD';

    constructor() {
        this._model = getSummaryModel();
    }

    /**
     * Get all ad-ids
     * @return [ad-id]
     */
    async all(): Promise<string[]> {
        const result: SummaryModel[] = await this._model.query('hashKey').eq(this._hashKey).all().exec();
        return result.map(e => e.rangeKey);
    }

    /**
     * Save ad-id to ad list
     * @param adId
     */
    save(adId: string): Promise<SummaryModel> {
        const model = new this._model({ hashKey: this._hashKey, rangeKey: adId });
        return model.save();
    }

    /**
     * Save user to ad-id targets
     * @param adId
     * @param userId
     */
    saveUser(adId: string, userId: string): Promise<SummaryModel> {
        const model = new this._model({ hashKey: `${this._adPrefix}-${adId}`, rangeKey: userId });
        return model.save();
    }

    /**
     * List all users which came from the ad
     * @param adId
     * @return [userId]
     */
    async listUsers(adId: string): Promise<string[]> {
        const result: SummaryModel[] = await this._model.query('hashKey').eq(`${this._adPrefix}-${adId}`).all().exec();
        return result.map(e => e.rangeKey);
    }
}
