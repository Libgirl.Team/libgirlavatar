import { getPushTaskModel } from "../utils/models";

export async function savePushTaskToDB(pushTask: any) {
    try {
        const PK = getPushTaskModel();
        const pk = new PK(pushTask);
        await pk.save();
    } catch (e) {
        console.error(e);
    }
}
