import { saveUserState } from "../utils/state";
import SummaryRepository from "./summary-repository";

export default class RedirectUrlRepository {
    private _eventPrefix = 'URL';
    private _rangeKey = 'COUNT';
    private readonly _summary: SummaryRepository;

    constructor() {
        this._summary = new SummaryRepository();
    }

    async increment(id: string) {
        const keys = { hashKey: this.hashKey(id), rangeKey: this._rangeKey };
        await this._summary.getOrCreate(keys, {
            hashKey: this.hashKey(id),
            rangeKey: this._rangeKey,
            urlId: id,
            count: 0,
        });
        return this._summary.update(keys, { $ADD: { count: 1 } });
    }

    saveUserClick(userId: string, id: string): Promise<{} | undefined> {
        const time = Date.now();
        return saveUserState(userId, `URL-${id}#ACTION-CLICK#CREATE_AT-${time}`, {
            urlId: id,
            action: 'CLICK',
            createdAt: time,
        });
    }

    async countUrl(urlId: string): Promise<{ id: string, count: number }> {
        const result = await this._summary.getUrlCount(urlId);
        return { id: urlId, count: result?.count || 0 };
    }

    private hashKey(id: string) {
        return `${this._eventPrefix}-${id}`;
    }
}
