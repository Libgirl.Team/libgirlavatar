import { Request, Response } from 'express';

export const verifyWebhook = (req: Request, res: Response) => {
    let VERIFY_TOKEN = 'blahblah';

    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];

    console.log(mode, token, challenge);

    if (mode && token === VERIFY_TOKEN) {
        res.status(200).send(challenge);
    } else {
        res.sendStatus(403);
    }
};

