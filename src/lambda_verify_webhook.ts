exports.handler = async function(event : any, context : any) {
  // 取得檢查用的 Verify Token，這個存放在 Lambda Function 的環境變數中。
  let token_answer = process.env.verify_token;
  
  console.log("ENVIRONMENT VARIABLES\n" + JSON.stringify(process.env, null, 2))
  console.log("EVENT\n" + JSON.stringify(event, null, 2))
  
  // 首先擷取輸入訊息判斷：這邊處理的是 webhook 送過來的挑戰。
  let params = event.queryStringParameters
  let challenge = ""
  let verify_token = ""
  
  if (params && params["hub.mode"]) {
    challenge = params["hub.challenge"]
    verify_token = params["hub.verify_token"]
    
    if (verify_token === token_answer) {
      // 答案正確，回傳 challenge 內容。
      console.log("Correct token!")
    } else {
      // 答案錯誤。
      console.log("Wrong challenge: verify_token = " + JSON.stringify(verify_token))
    }
  }
  
  // 臉書建議無論如何都要回傳 200 出去。
  let responseCode = 200

  // 建立回傳訊息。
  let response = {
    statusCode: responseCode,
    body: challenge
  }
  return response
}
