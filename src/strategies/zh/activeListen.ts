import { WeightedSentence, rndWeightedSentence } from '../../utils/sentence';

export function genActiveListening(sentimentScore: number): string {
    console.log(`sentiment score: ${sentimentScore}`)
    if (sentimentScore > 0.3) {
        return genPositiveFollower();
    } else if (sentimentScore < -0.3) {
        return genNegativeFollower();
    } else {
        return genNeutralFollower();
    }
}

export function genPositiveFollower(): string {
    return rndWeightedSentence(positiveFollowers);
}

export function genNeutralFollower(): string {
    console.log('genNeutralFollower');
    return rndWeightedSentence(neutralFollowers);
}

export function genNegativeFollower(): string {
    console.log('genNegativeFollower');
    return rndWeightedSentence(negativeFollowers);
}

const positiveFollowers: WeightedSentence[] = [
    { weight: 3, sentence: 'd(`･∀･)b' },
    { weight: 3, sentence: '(✪ω✪)' },
    { weight: 1, sentence: '(ㄏ￣▽￣)ㄏ ㄟ(￣▽￣ㄟ)' },
    { weight: 1, sentence: '(｡・ω・｡)' },
    { weight: 3, sentence: '(*´ω`)人(´ω`*)～' },
    { weight: 1, sentence: '✧*｡٩(ˊᗜˋ*)و✧*｡' },
    { weight: 1, sentence: 'd(d＇∀＇)' },
    { weight: 1, sentence: '(*´ω`)人(´ω`*)' },
    { weight: 1, sentence: '(∩^o^)⊃━☆ﾟ.*･｡ 水啦' },
    { weight: 1, sentence: '讚讚~ d(d＇∀＇)' },
    { weight: 3, sentence: '(*´∀`)~♥ 真好～' },
    { weight: 1, sentence: '♡(*´∀｀*)人(*´∀｀*)♡' },
    { weight: 1, sentence: '(・∀・)つ⑩～' },
    { weight: 1, sentence: 'ヾ(´︶`*)ﾉ♬' },
    { weight: 1, sentence: 'ʕ•̀ω•́ʔ✧' },
    { weight: 1, sentence: 'ヽ(✿ﾟ▽ﾟ)ノ' },
    { weight: 1, sentence: '(*´∀`)~♥' },
    { weight: 1, sentence: 'σ`∀´)σ' },
    { weight: 3, sentence: '(σﾟ∀ﾟ)σ..:*☆' },
    { weight: 3, sentence: 'd(`･∀･)b 真好！' },
    { weight: 3, sentence: '(＊゜ー゜)b' },
];

const neutralFollowers: WeightedSentence[] = [
    { weight: 2, sentence: '(☉д⊙)' },
    { weight: 1, sentence: '(´･_･`)' },
    { weight: 1, sentence: '( ¯•ω•¯ )' },
    { weight: 1, sentence: '嗯嗯 等等 先去洗澡                        沒有啦你繼續說' },
    { weight: 2, sentence: '( ºΔº )' },
    { weight: 1, sentence: '(つ´ω`)つ' },
    { weight: 1, sentence: '(´･ω･`)' },
    { weight: 1, sentence: '(❛◡❛)' },
    { weight: 1, sentence: '●﹏●' },
    { weight: 1, sentence: '欸～(|||ﾟдﾟ)' },
    { weight: 2, sentence: '等等 電鈴響了我去開一下 你繼續說' },
    { weight: 3, sentence: '( ˘•ω•˘ )' },
    { weight: 1, sentence: '( ¯•ω•¯ )v' },
    { weight: 3, sentence: '( ﾟ∀ﾟ)o彡ﾟ' },
    { weight: 1, sentence: '／/( ◕‿‿◕ )＼' },
    { weight: 1, sentence: '(`・ω・´)' },
];

const negativeFollowers: WeightedSentence[] = [
    { weight: 1, sentence: '討M～' },
    { weight: 1, sentence: '(｡ŏ_ŏ)..' },
    { weight: 1, sentence: '(´;ω;`)..' },
    { weight: 1, sentence: '(〒︿〒)' },
    { weight: 1, sentence: '｡ﾟヽ(ﾟ´Д`)ﾉﾟ｡..' },
    { weight: 1, sentence: '( ´ﾟДﾟ`)' },
    { weight: 1, sentence: '( ´•̥̥̥ω•̥̥̥` )......' },
    { weight: 1, sentence: '٩(ŏ﹏ŏ、)۶' },
    { weight: 1, sentence: '(つ´ω`)つ....' },
    { weight: 1, sentence: '( Φ ω Φ )..' },
    { weight: 1, sentence: 'Σ(ﾟДﾟ；≡；ﾟдﾟ)' },
    { weight: 1, sentence: '(;ﾟдﾟ)' },
    { weight: 1, sentence: 'ヽ(`Д´)ノ' },
    { weight: 1, sentence: '( ิ◕㉨◕ ิ)' },
    { weight: 1, sentence: 'Orz' },
    { weight: 1, sentence: '(／‵Д′)／~ ╧╧' },
    { weight: 1, sentence: '٩(ŏ﹏ŏ、)۶ 囧囧' },
    { weight: 1, sentence: '(°ﾛ°٥) 囧興' },
    { weight: 1, sentence: '٩(•ิ˓̭ •ิ )ง 囧rz' },
    { weight: 1, sentence: '(◔⊖◔)つ' },
    { weight: 2, sentence: '( ￣□￣)σ 論破!! ︴≡║██言彈██》' },
    { weight: 2, sentence: '龜派氣功(ﾟДﾟ)< ============O))' },
    { weight: 1, sentence: '∑(￣□￣;).......' },
    { weight: 1, sentence: '(´ー`)' },
    { weight: 1, sentence: 'T_T' },
    { weight: 1, sentence: '╮(╯_╰)╭' },
    { weight: 1, sentence: '(ﾟ⊿ﾟ) ' },
    { weight: 1, sentence: '(#`皿´) ' },
    { weight: 1, sentence: '(／‵Д′)／~ ╧╧' },
    { weight: 1, sentence: '(ಠ益ಠ)' },
    { weight: 1, sentence: '(|||ﾟдﾟ)' },
    { weight: 3, sentence: '( ´･･)ﾉ(._.`) 怎麼這樣....' },
    { weight: 3, sentence: '(ㆀ˘･з･˘) 奈欸啊捏..' },
    { weight: 3, sentence: 'ก็ʕ•͡ᴥ•ʔ ก้ 啊捏喔....' },
    { weight: 1, sentence: '◢▆▅▄▃崩╰(〒皿〒)╯潰▃▄▅▇◣' },
    { weight: 2, sentence: '魔貫光殺砲(ﾟДﾟ)σ━00000000000━●' },
    { weight: 3, sentence: '｡･ﾟ･(ﾉД`)ヽ(ﾟДﾟ )秀秀 這就是人蔘啊（嚼）' },
    { weight: 1, sentence: '╮(╯_╰)╭' },
    { weight: 1, sentence: '(╥﹏╥) 哭哭' },
    { weight: 1, sentence: '( ´•̥×•̥` )......' },
    { weight: 1, sentence: ' σ ﾟ∀ ﾟ) ﾟ∀ﾟ)σ（茶）' },
];
