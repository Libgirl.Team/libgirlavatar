

const nonsenseAnswers: string[] = [
    '我不知道。',
    '你這個問題，跟問我要不要孝順爸爸媽媽，不是一樣的嗎？',
    '下一題！',
    '我可以講話，但是你們（媒體）不能發問。',
    '等他們（媒體）出去，我們就可以自由交談！',
    '我沒有害怕，但是我不願意回答不確定不相關的問題。',
    '跟委員報告一下，英文有一句話就是說：『你不能把橘子跟香蕉來比』，所以我想這個不是誰大誰小的問題。',
    '它是個議題，但它是一個假議題。',
    '沒有，我跟你講，這就是...沒有，我的態度是這樣，我是外科醫生喔～一個SOP控，你知道，很簡單呀，外科醫生沒有SOP不會開刀呀，你那個皮膚切開幾公分，深入幾公分，傷口開創器要多大的，然後傷口要多大的，要怎麼開？我是外科醫生嘛！沒有SOP我不會開刀呀！',
    '這是問題嗎？若是要如何解決？',
    '追求這個問題的答案，就是這個問題的答案。',
    '抽號碼牌，一定讓你們問到飽。',
    'Reset 重新思考。',
    '靜思片刻。',
    '我也是看報紙才知道。',
]

const whoQuestionKeys = ['誰', '哪個人', '哪些人',];
const whatQuestionKeys = ['是什麼', '什麼是',];
const whatAnswers = [
    `${name}是最重要盟友`,
    `${name}永遠是心裡最柔軟的那一塊`,
    `${name}什麼都可以做，連總統都可以做，三軍統帥也可以做`,
    `${name}有大哥樣子`,
    `${name}是漂亮小姐與3男打麻將`,
    `${name}過去跟勞工站在一起，未來也會如此`,
    `之前我買了一個${name}，然後就`,
    `${name}在有和沒有之間`,
    '我覺得台灣最需要的是治理',
];
const whyQuestionKeys = ['為什麼', '怎麼會'];
// '因為';

const whyAnswers = [
    '不為民調，我是為台灣做事',
    '打壓讓我們更堅定更團結',
    '手心手背都是肉',
    '台灣的假真的休太多了',
    '政府無能、無心、無感，對人民的痛苦麻木不仁',
    '會特別關心農民與勞工',
    '什麼都可談，自由跟未來不可被妥協',
    '黨一碗滷肉飯都沒給',
    '為了中華民國，不惜粉身碎骨',
    '我覺得台灣最需要的是治理',
    '垃圾不分藍綠',
    '世間的榮華富貴，不過就是一坨大便',
    '抹黑抹紅抹黃很煩',
    '沒計畫沒準備',
    '公務員被說好乖是可恥',
];

const whereQuestionKeys = ['哪裏', '哪裡', '什麼地方', '哪個地方',];
const wherePreAns = ['在', '是']

const whichQuestionKeys = ['哪一個', '哪個', ];
const whenQuestionKeys = ['什麼時候', '幾點', '哪一天', '何時', '幾月', '幾號', '星期幾'];

const howQuestionKeys = ['怎麼', '如何',];
const howAnswer = [
    '只能回去跟太太說了',
    '鳳凰都飛走了，進來一大堆雞！',
    '怎麼瑪麗亞變老師了？',
    '台灣需要的是明治維新不是義和團',
]


const yesNoKeys = [
    "是不是",    
    "知不知",
]

const yesNoAnswqers = [
    '勞工永遠是心裡最柔軟的那一塊',
    '手心手背都是肉',
    '不為民調，我是為台灣做事',
    '是最重要盟友',
    '北市府是全國最大違建戶',
]

const existenceQuestionKeys =  ['有沒有', '是否有'];
const existenceAnswers = [
    '我的內心沒有一絲一毫的喜悅',
    '在有和沒有之間',
    '我的政見有白紙黑字出來？',
    '擁有了一碗滷肉飯與一瓶礦泉水的信心'
]

const howAboutQuestionKeys = ["要不要", "想不想", "好不好",];
const howAboutAnswers = [
    '不需要媽祖託夢，自己決定就可以了',
    '支持者想看到我們合作，我們就應該合作',
    '會全力支持',
    '急什麼',
    '不談政治，要衝經濟',
    '乾淨的輸，不要骯髒的贏',
];

const sentences = [
    '其實我……是接近的，只是你不知道我在接近你而已。',
    '',
    '',
    '',
    '',
]
