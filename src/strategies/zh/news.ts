import { NewsMedia, UNITED_DAILY_NEWS, LIBERTY_TIMES, QuickReply, NewsOutContent } from '../definitions';
import randomItem from 'random-item';
import libertyNews from './liberty.json';
import udnNews from './udn.json';
import { CMD_GEN_NEWS, CMD_NEXT_NEWS, CMD_ENOUGH, CMD_GOTO_TOP } from './cmdList';

interface NewsCollection {
    media: NewsMedia,
    collection: string[]
}

const NewsCollections: NewsCollection[] = [
    { media: UNITED_DAILY_NEWS, collection: udnNews },
    { media: LIBERTY_TIMES, collection: libertyNews }
]

export function genNews(cmd: string): NewsOutContent {
    const source: NewsCollection = randomItem(NewsCollections);
    const newsSplit = randomItem(source.collection).split(',');
    const quickReplies: QuickReply[] = (cmd === CMD_GEN_NEWS) ?
        [CMD_NEXT_NEWS, CMD_ENOUGH].map(cmd => { return { cmd }})
        : [CMD_GOTO_TOP].map(cmd => { return { cmd } });
    return new NewsOutContent(
        newsSplit[0],
        newsSplit[1] ? newsSplit[1] : '',
        source.media,
        quickReplies
    );
}
