import { QuickReply } from "../definitions"

export const CMD_ELECTION_DATE = 'CMD_ELECTION_DATE';
const TEXT_ELECTION_DATE = 'electionDate';
export const CMD_GET_STARTED = 'CMD_GET_STARTED';
const TEXTS_GET_STARTED = ["快來尬聊一波", 'getStarted'];
export const CMD_SHARE_QR = 'CMD_SHARE_QR';
const TEXT_SHARE_QR = 'shareAvatar';
export const CMD_SHARE_LINK = 'CMD_SHARE_LINK';
const TEXT_SHARE_LINK = '分享連結 Get Link';
export const CMD_GOTO_TOP = 'CMD_GOTO_TOP';
export const TEXTS_GOTO_TOP = ["召喚萌太子", '選單'];
export const CMD_REQUEST_POEM = 'CMD_REQUEST_POEM';
const TEXT_REQUEST_POEM = "萌太子測吉凶";
export const CMD_REQUEST_MODE = 'CMD_REQUEST_MODE';
const TEXT_REQUEST_MODE = "萌太子上身";
export const CMD_MODE_POSITIVE = 'CMD_MODE_POSITIVE';
export const TEXT_MODE_POSITIVE ='玻璃心求呵護';
export const CMD_MODE_NEGATIVE = 'CMD_MODE_NEGATIVE';
export const TEXT_MODE_NEGATIVE = '抖M想被踩';
export const CMD_GEN_JOKE = 'CMD_GEN_JOKE';
const TEXT_GEN_JOKE = "練笑話產生器";
export const CMD_GEN_NEWS = 'CMD_GEN_NEWS';
const TEXT_GEN_NEWS = '政治大小事';
export const CMD_NEXT_NEWS = 'CMD_NEXT_NEWS';
const TEXT_NEXT_NEWS = '看下一則';
export const CMD_NEXT_JOKE = 'CMD_NEXT_JOKE';
const TEXT_NEXT_JOKE = "再來一句";
export const CMD_ENOUGH = 'CMD_ENOUGH';
export const TEXT_ENOUGH = "看夠了";
export const CMD_GAME_ENOUGH = "CMD_GAME_ENOUGH";
const TEXT_GAME_ENOUGH = "我已經聽不下去了，拜託變回來吧！";
export const CMD_GIVEUP = 'CMD_GIVEUP';
const TEXT_GIVEUP = "放棄";
export const CMD_PLAY_GAME = 'CMD_PLAY_GAME';
const TEXT_PLAY_GAME = '萌太子玩遊戲';
export const CMD_PLAY_PERSISTENT_REPLY = 'CMD_PLAY_PERSISTENT_REPLY';
const TEXT_PLAY_PERSISTENT_REPLY = '誰是句點王';
export const CMD_GIVEUP_PERSISTENT_REPLY = 'CMD_GIVEUP_PERSISTENT_REPLY';
const TEXT_GIVEUP_PERSISTENT_REPLY = '我回不下去了我認輸';
export const CMD_SUMMON_LIBGIRL = 'CMD_SUMMON_LIBGIRL';
const TEXT_SUMMON_LIBGIRL = '萌太子我還你原形';
export const CMD_SUMMON_GIRLFRIEND = 'CMD_SUMMON_GIRLFRIEND';
const TEXT_SUMMON_GIRLFRIEND = '我空虛寂寞覺得冷萌萌子陪陪我';
export const CMD_TEXT_ME_LATER = 'CMD_TEXT_ME_LATER';
const TEXT_TEXT_ME_LATER = 'TEXT_ME_LATER';

export const CMD_LIFE_AID = 'CMD_LIFE_AID';
export const TEXT_LIFE_AID = ['我需要幫忙'];
export const CMD_END_AID = 'CMD_END_AID';
export const TEXT_END_AID = ['我已經沒事了','找萌太子玩'];
export const CMD_LIFE_CALL = 'CMD_LIFE_CALL'
export const TEXT_LIFE_CALL = ['張老師專線','生命專線','安心專線'];
export const defaultCmds = [CMD_PLAY_GAME, CMD_GEN_NEWS];

export const askingJoke: string[] = [CMD_GEN_JOKE, CMD_NEXT_JOKE];
export const askingNews: string[] = [CMD_GEN_NEWS, CMD_NEXT_NEWS];

const cmdTextsMap: {cmd: string, texts: string[]}[] = [
    // get start
    {cmd: CMD_GET_STARTED, texts: TEXTS_GET_STARTED },
    // persistent menu
    {cmd: CMD_ELECTION_DATE, texts: [TEXT_ELECTION_DATE]},
    {cmd: CMD_SHARE_QR, texts: [TEXT_SHARE_QR]},
    {cmd: CMD_SHARE_LINK, texts: [TEXT_SHARE_LINK]},
    // menu
    {cmd: CMD_GOTO_TOP, texts: TEXTS_GOTO_TOP},
    // news & joke
    {cmd: CMD_GEN_JOKE, texts: [TEXT_GEN_JOKE]},
    {cmd: CMD_GEN_NEWS, texts: [TEXT_GEN_NEWS]},
    {cmd: CMD_NEXT_JOKE, texts: [TEXT_NEXT_JOKE]},
    {cmd: CMD_NEXT_NEWS, texts: [TEXT_NEXT_NEWS]},
    {cmd: CMD_ENOUGH, texts: [TEXT_ENOUGH]},
    // game
    {cmd: CMD_PLAY_GAME, texts: [TEXT_PLAY_GAME]},
    {cmd: CMD_REQUEST_POEM, texts: [TEXT_REQUEST_POEM]},
    {cmd: CMD_REQUEST_MODE, texts: [TEXT_REQUEST_MODE]},
    {cmd: CMD_MODE_POSITIVE, texts: [TEXT_MODE_POSITIVE]},
    {cmd: CMD_MODE_NEGATIVE, texts: [TEXT_MODE_NEGATIVE]},
    {cmd: CMD_GIVEUP, texts: [TEXT_GIVEUP]},
    {cmd: CMD_GAME_ENOUGH, texts: [TEXT_GAME_ENOUGH]},
    // life aid
    {cmd: CMD_LIFE_AID, texts: TEXT_LIFE_AID},
    {cmd: CMD_END_AID, texts: TEXT_END_AID},
    // persistentReply game
    {cmd: CMD_PLAY_PERSISTENT_REPLY, texts: [TEXT_PLAY_PERSISTENT_REPLY]},
    {cmd: CMD_GIVEUP_PERSISTENT_REPLY, texts: [TEXT_GIVEUP_PERSISTENT_REPLY]},
    // role
    {cmd: CMD_SUMMON_LIBGIRL, texts: [TEXT_SUMMON_LIBGIRL]},
    {cmd: CMD_SUMMON_GIRLFRIEND, texts: [TEXT_SUMMON_GIRLFRIEND]},
    // girlfriend
    {cmd: CMD_TEXT_ME_LATER, texts: [TEXT_TEXT_ME_LATER]},
]

export function textToCmd(text: string | undefined): string | null {
    if (text) {
        const foundFromCmd = cmdTextsMap.find(pair => pair.cmd === text);
        if (foundFromCmd) {
            return foundFromCmd.cmd;
        } else {
            const foundFromText = cmdTextsMap.find(pair => pair.texts.includes(text));
            return foundFromText ? foundFromText.cmd : null;
        }
    } else {
        return null
    }
}

export function cmdToText(cmd: string): string {
    const found = cmdTextsMap.find(pair => pair.cmd === cmd);
    console.log('cmdToText found:');
    console.log(found);
    if (found) {
        if (found.texts[0]) {
            return found.texts[0];
        }
    }
    return cmd
}

export function cmdToTexts(cmd: string): string[] {
    const found = cmdTextsMap.find(pair => pair.cmd === cmd);
    console.log('cmdToText found:');
    console.log(found);
    if (found?.texts) {
        if (found.texts.length > 0)
        return found.texts
    }
    return [cmd]
}

export function makeQuickReplies(cmds: string[]): QuickReply[] {
    return cmds.map(cmd => { return { cmd }});
}

export const defaultQuickReply: QuickReply[] = defaultCmds.map(cmd => { return { cmd }});
