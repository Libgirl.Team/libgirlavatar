import { StrategyForNonText, NonTextStrategyOpt, finishStrategyForText, finishStrategyForNonText, StrategyForCmd, StrategyForText, TextStrategyOpt, finishStrategyForCmd } from '../../replier';
import { PLAYING, TextOutMsg, User, OMsg, PersistentReplyGameContent, UNTRIGGERED, PLAYED, CmdInMsg, PERSISTENT_REPLY_GAME, TextInMsg, TextOutContent } from '../definitions';
import { CMD_PLAY_PERSISTENT_REPLY, makeQuickReplies, CMD_GIVEUP_PERSISTENT_REPLY, defaultQuickReply } from './cmdList';
import { timeStampNow } from '../../utils/tools';
import { getUserState, saveUserState } from '../../utils/state';

const STRATEGY_PERSISTENT_REPLY_GAME = 'STRATEGY_PERSISTENT_REPLY_GAME';

export async function getPersistentReplyState(user: User): Promise<PersistentReplyGameContent> {
    const gotState = await getUserState(user.id, PERSISTENT_REPLY_GAME);
    if (gotState) {
        return <PersistentReplyGameContent>gotState;
    } else {
        const newState:PersistentReplyGameContent = {
            state: UNTRIGGERED,
            totalCount: 0,
            continuePlayingCount: 0
        };
        await saveUserState(user.id, PERSISTENT_REPLY_GAME, newState);
        return newState;
    }
}

export const persistentReplyGame = async (
    finisher: (preOutMsgs: OMsg[]) => Promise<string[]>,
    user: User,
    preOutMsgs: OMsg[],
) => {
    const gameState: PersistentReplyGameContent = await getPersistentReplyState(user);
    
    if (gameState.state === PLAYING) {

        if (gameState.continuePlayingCount >= 10) {
            const quickReplies = makeQuickReplies([CMD_GIVEUP_PERSISTENT_REPLY]);
            const outMsg = new TextOutMsg(
                user,
                [STRATEGY_PERSISTENT_REPLY_GAME],
                timeStampNow(),
                new TextOutContent(`你和我尬聊了${gameState.totalCount}回合，還撐得下去嗎？`, [], quickReplies)
            );
            await saveUserState(
                user.id,
                PERSISTENT_REPLY_GAME,
                {state: PLAYING,
                 totalCount: gameState.totalCount + 1,
                 continuePlayingCount: 0 }
            );
            return finisher(preOutMsgs.concat([outMsg]));
            
        } else {
            await saveUserState(
                user.id,
                PERSISTENT_REPLY_GAME,
                {state: PLAYING,
                 totalCount: gameState.totalCount + 1,
                 continuePlayingCount: gameState.continuePlayingCount + 1 }
            );
        }
    }
    return finisher(preOutMsgs);
}

export const persistentReplyGameForText: StrategyForText = (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => persistentReplyGame(
    (preOutMsgs: OMsg[]) => finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt),
    inMsg.user,
    preOutMsgs
);

export const persistentReplyForNontext: StrategyForNonText = (
    user: User,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: NonTextStrategyOpt
) => persistentReplyGame(
    (preOutMsgs: OMsg[]) => finishStrategyForNonText(user, sendAPI, preOutMsgs, opt),
    user,
    preOutMsgs
)

export const persistentReplyGameForCmd: StrategyForCmd = async (
    inMsg: CmdInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    falseCallback?: StrategyForCmd
) => {
    const cmd = inMsg.content.cmd;
    
    if (cmd === CMD_PLAY_PERSISTENT_REPLY) {
        await saveUserState(inMsg.user.id, PERSISTENT_REPLY_GAME, {
            state: PLAYING,
            totalCount: 0,
            continuePlayingCount: 0
        });
        const outMsg = new TextOutMsg(
            inMsg.user,
            [STRATEGY_PERSISTENT_REPLY_GAME],
            timeStampNow(),
            new TextOutContent(
                '我們來比賽吧！規則很簡單，先已讀不回的就輸了！你想怎麼開始呢？來聊些什麼，或者玩個遊戲？',
                [],
                defaultQuickReply
            )
        );
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
        
    } else if (cmd === CMD_GIVEUP_PERSISTENT_REPLY) {
        const outMsg = new TextOutMsg(
            inMsg.user,
            [STRATEGY_PERSISTENT_REPLY_GAME],
            timeStampNow(),
            new TextOutContent(`比賽放一邊，我們繼續聊。請你說點什麼，或是來玩個遊戲吧！`,
                               [],
                               defaultQuickReply)
        );
        await saveUserState(inMsg.user.id, PERSISTENT_REPLY_GAME, {
            state: PLAYED,
            totalCount: 0,
            continuePlayingCount: 0
        });

        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
    }
    return persistentReplyGame(
        (preOutMsgs: OMsg[]) => finishStrategyForCmd(inMsg, sendAPI, preOutMsgs, falseCallback),
        inMsg.user,
        preOutMsgs
    )
}
