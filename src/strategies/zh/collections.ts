export const personCollection = [
    '蔡英文',
    '柯文哲',
    '宋楚瑜',
    '賴清德',
    '黃國昌',
    '習近平',
    '韓國瑜',
    '馬英九',
    '王金平',
    '鄭文燦',
    '顏清標',
    '吳敦義',
    '陳水扁',
    '林昶佐',
    '郭台銘',
    '妙天',
    '妙禪',
    '館長',
    '蘇貞昌',
    '張善政',
    '川普',
    '范雲',
    '林鄭月娥',
];

export const locationCollection = [
    '內地',
    '高雄',
    '台北',
    '台南',
    '台灣',
    '中國',
    '南投',
    '中國台北',
    '內湖',
    '太平島',
    '香港',
    '北京',
    '美國',
];

export const organizationCollection = [
    '民進黨',
    '國民黨',
    '共產黨',
    '時代力量',
    '國會政黨聯盟',
    '釋迦摩尼佛救世基金會',
    '社會民主黨',
    '綠黨',
    '護家盟',
    '下一代幸福聯盟',
    '台灣民眾黨',
    '安麗',
    '親民黨',
    '歡樂無法黨',
    '國會黨',
    '台聯',
    '中華統一促進黨',
    '新黨',
    '興中會',
    '同盟會',
]
