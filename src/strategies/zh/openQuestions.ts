import randomItem from 'random-item';
import { NlpEntity, findMaxSalienceEntity } from '../../utils/google';
import { Option, None, Some } from '@usefultools/monads';
import { personCollection, locationCollection, organizationCollection } from './collections';

const probabilityPre = 1;

export const genOpenQuestion = function (entities: NlpEntity[]): string {
    const pre = (Math.random() < probabilityPre) ? randomItem(memePreSentences) : "";
    const conj = randomItem(conjunctions);
    const post: string = genEntityOpenQuestion(entities).match({
        some: (entOpenQ) => {
            return entOpenQ
        },
        none: () => randomItem(generalEndSentences),
    });
    return `${pre}${conj}${post}`;
}

const memePreSentences: string[] = [
    '有人說，我執政為什麼街上那麼多人、聲音那麼大，我的意思是說，你有事情就大聲講嘛。',
    '當政府幫你們時候，你們應該站出來大聲講，說難聽的話也沒關係，引人注意一點。',
    '拋磚是要引瑜。',
    '政治人物要收起傲慢，盡量對話。我會和你好好對話。',
    '我的政府，是有史以來最會溝通的政府。我都會聽你講。',
    '你這不是講出來了嗎？',
    '我不能講話太大聲，免得人家又說我是震怒。不過你不是主席，你可以大聲講。',
    '如果政府沒聽見，你可以拍桌子。再不然你可以跟我說。',
    '你怎麼不去電台講給全台灣知道？',
    '恁哪毋去電台講呼全台灣知？',
    '我只是看到怪怪的，忍不住要舉手。',
    '說出嘴的話語和吃進肚的東西同樣重要。',
    '政治語言、唯真不破。',
    '怎麼瑪麗亞變老師了？',
    '敢講就是務實。',
    '你去跟你老闆說嘛。',
];

const conjunctions: string[] = [
    '回到你說的，',
    '就你剛才說的，',
    '回到你剛才說的，',
    '回到正題，',
    '所以，',
    '所以',
    '那麼，',
]

const generalEndSentences: string[] = [
    '為什麼你會這麼覺得呢？',
    '是什麼讓你這麼認為？',
    '你什麼時候開始這樣子想的呢？',
    '為什麼你這麼想呢？',
    '是哪個部分給你這種感覺呢？',
    '你是怎麼知道的呢？',
    '你怎麼會有這種念頭？',
];


const generalEntityOpenQuestions: string[] = [
    '為什麼你對${eneity0}有這種看法呢？',
    '是${eneity0}的哪個部分讓你這麼覺得？',
    '你什麼時候開始這樣看${eneity0}的呢？',
    '是什麼讓你這樣看待${eneity0}的呢？',
];

function genEntityOpenQuestion(entities: NlpEntity[]): Option<string> {
    const generalEntities = entities.filter((ent) => {
        switch (ent.type) {
            case "PHONE_NUMBER":
            case "DATE":
            case "NUMBER":
            case "PRICE":
            case "ADDRESS":
                return false;
            case "PERSON":
            case "LOCATION":
            case "ORGANIZATION":
            default:
                return true;
        }
    });

    if (generalEntities.length > 0) {
        const memeEntities = generalEntities.filter((ent) => {
            switch (ent.type) {
                case "PERSON":
                case "LOCATION":
                case "ORGANIZATION":
                    return true;
                default:
                    return false;
            };
        });

        const genGeneralOpenQuestion = function (word: string): string {
            return randomItem(generalEntityOpenQuestions).replace('${eneity0}', word);
        };

        if (memeEntities.length > 0) {
            const probMemeOpenQuestion = 0.8;
            if (Math.random() < probMemeOpenQuestion) {
                const maxSalienceMemeEntity = findMaxSalienceEntity(generalEntities).unwrap();

                const genMemeOpenQuestion = function (entName: string, entType: string, collection: string[]): string {
                    const probSpecify = 0.9;
                    if (Math.random() < probSpecify) {
                        let another = randomItem(collection);
                        if (collection.length > 1) {
                            while (another === entName) {
                                another = randomItem(collection);
                            };
                        }
                        if (Math.random() < 0.5) {
                            return `你覺得${randomItem(collection)}怎麼樣？`;
                        } else {
                            return `你覺得${randomItem(collection)}和${entName}比起來呢？`;
                        }
                    } else {
                        return `你覺得有${entType}是這樣子的？`;
                    }
                };

                switch (maxSalienceMemeEntity.type) {
                    case "PERSON":
                        return Some(genMemeOpenQuestion(maxSalienceMemeEntity.name, '誰', personCollection));
                    case "LOCATION":
                        return Some(genMemeOpenQuestion(maxSalienceMemeEntity.name, '哪裡', locationCollection));
                    case "ORGANIZATION":
                        return Some(genMemeOpenQuestion(maxSalienceMemeEntity.name, '哪個黨', organizationCollection));
                }
            }
        }
        return Some(genGeneralOpenQuestion(findMaxSalienceEntity(generalEntities).unwrap().name));
    } else {
        return None
    };
}
