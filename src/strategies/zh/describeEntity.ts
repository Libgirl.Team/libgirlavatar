import zhBlurt from './blurt.json';
import randomItem from 'random-item';
import * as ggl from '../../utils/google';

//import randomItem = require('random-item');

export function newEntitySentence(entities: any[]): string {
    const maxSalienceEntity = ggl.findMaxSalienceEntity(entities).unwrap();
    const maxName = maxSalienceEntity.name;

    switch (maxSalienceEntity.type) {
        case "UNKNOWN":
            return unknownEntitySentence(maxName);
        case "PERSON":
            return personEnritySentence(maxName);
        case "LOCATION":
            return locationEntitySentence(maxName);
        case "ORGANIZATION":
            return orgEntitySentence(maxName);
        case "CONSUMER_GOOD":
            return consumerGoodEntitySentence(maxName);
        case "OTHER":
            return unknownEntitySentence(maxName);
        case "PHONE_NUMBER":
            return phoneNumberEntitySentence(maxName);
        case "DATE":
            return dateEntitySentence(maxName);
        case "EVENT":
            //return eventEntitySentence(maxName);
        case "WORK_OF_ART":
            // return artWorkEntitySentence(maxName);
        case "NUMBER":
            // return numberEntitySentence(maxName);
        case "PRICE":
            // return priceEntitySentence(maxName);
        case "ADDRESS":
            //return addressEntitySentence(maxName);
        default:
            return defaultSentence();
    }
}

function unknownEntitySentence(name: string): string {
    return randomItem([
        `台北的未來在${name}`,
        `${name}是什麼，能吃嗎？`,
        `${name}永遠是心裡最柔軟的那一塊`,
        `不會拿勞工的${name}，在選舉時開空頭支票作政治操作`,
        `${name}是個假議題`,
        `敢講${name}就是務實`,
        `${name}在有和沒有之間`,
        `${name}有那麼困難嗎？${name}是找回良心而已`,
    ])
}

function personEnritySentence(name: string): string {
    return randomItem([
        `${name}跟潘金蓮蓋一條棉被`,
        `武大郎跟${name}蓋一條棉被`,
        `${name}是最重要盟友`,
        `${name}永遠是心裡最柔軟的那一塊`,
        `${name}什麼都可以做，連總統都可以做，三軍統帥也可以做`,
        `如果${name}能投資創造一萬個工作機會，我就以身相許，晚上跟他睡覺`,
        `${name}有大哥樣子`,
    ])
}

function locationEntitySentence(name: string): string {
    return randomItem([
        `${name}的未來在手中`,
        `${name}又老又窮，各行各業蕭條的不得了`,
        `${name}是漂亮小姐與3男打麻將`,
        `${name}我覺得台灣最需要的是治理`,
        `${name}有大哥樣子`,
    ])
}

function orgEntitySentence(name: string): string {
    return randomItem([
        `過去跟${name}站在一起，未來也會如此`,
        `如果${name}能投資創造一萬個工作機會，我就以身相許，晚上跟你睡覺`,
        `貨賣得出去，人進得來，${name}發大財`,
        `${name}沒有為年輕人找到一條回家的道路`,
        `${name}有大哥樣子`,
    ])
}

// function eventEntitySentence(name: string): string {
//     return randomItem([
//         `${name}是一場活動，熱鬧嗎？`,
//         `${name}`,
//     ])
// }

// function artWorkEntitySentence(name: string): string {
//     return randomItem([
//         `${name}是一個藝術品或藝術創作。你喜歡嗎？`,
//         `${name}`,
//     ])
// }

function consumerGoodEntitySentence(name: string): string {
    return randomItem([
        `${name}是一個消費性產品，你有嗎？`,
        `之前我買了一個${name}，然後就`,
        `不會拿勞工的${name}，在選舉時開空頭支票作政治操作`,
        `總統交代，用「自然方式」跟習近平交換${name}`,
        `${name}不應做為統治者洗腦的工具`,
    ])
}

function phoneNumberEntitySentence(name: string): string {
    return randomItem([
        `我要把這個號碼告訴韓總機嗎？`,
    ])
}

function dateEntitySentence(name: string): string {
    return randomItem([
        `${name}將是「終局之戰」，我期待扮演「獨孤求敗」的角色`,
    ])
}

// function addressEntitySentence(name: string): string {
//     return randomItem([
//         `${name}`,
//     ])
// }

// function numberEntitySentence(name: string): string {
//     return randomItem([
//         `${name}`,
//     ])
// }

// function priceEntitySentence(name: string): string {
//     return randomItem([
//         `${name}`,
//     ])
// }

function defaultSentence(): string {
    return randomItem(zhBlurt.map((pair: { key: string, value: string }) => pair.value));
}
