import randomItem = require("random-item");

export const axios = require('axios').default;

const url = 'https://search-libgirlsearch-staging-7jeootbuyikw7ziaid7ho4fu7e.us-east-1.es.amazonaws.com/';

export async function elasticSearchReply(inText: string): Promise<string | undefined> {
    console.log('elasticSearchReply inText:\n' + inText);
    return await axios.post(url + 'messages/_search', {
        size: 5,
        query: {
            multi_match: {
                query: inText,
                fields: ['message', 'message.keyword']
            }
        }
    })
        .then((res: any) => {
            console.log('elasticSearch Axios res.data or res:\n' + JSON.stringify(res.data ? res.data : res));
            const hits = res.data?.hits?.hits;
            const rand :number = Math.floor(Math.random()*hits.length)
            const hit = {
                reply : hits[rand] ?._source?.reply ,
                recmd :  hits[rand] ?._source?.recmd 
            }
            return hit
        }).catch((err: any) => {
            console.log('elasticSearch Axios err:');
            console.log(err);
            return undefined
        });
}

function mode2Text(gameState: number) {
    if (gameState == 2) {
        const mode: string = 'comfort'
        const defaultString: string = '本萌還不了解這方面的你，而無意義的讚美是很失禮的。\n可以多說一點嗎？'
        return [mode, defaultString]
    } else {
        const mode: string = 'blaming'
        const defaultString: string = '講一句讓本宮聽得懂的話很難嗎？\n再不說點什麼，本宮都懶得罵你。'
        return [mode, defaultString]
    }
}

export async function elasticSearchGame(inText: string, gameState: number): Promise<string> {
    console.log('elasticSearchReply inText:\n' + inText);
    const [mode, defaultString]: string[] = mode2Text(gameState)
    return await axios.post(url + mode + '/_search', {
        size: 10,
        query: {
            multi_match: {
                query: inText,
                fields: ['message', 'message.keyword']
            }
        }
    })
        .then((res: any) => {
            console.log('elasticSearch Axios res.data or res:\n' + JSON.stringify(res.data ? res.data : res));
            if (res.data?.hits?.hits.length > 0) {
                const hits = res.data?.hits?.hits;
                return hits ? randomItem(hits as any[])?._source?.reply : undefined
            }else{
                return defaultString
            }
        }).catch((err: any) => {
            console.log('elasticSearch Axios err:');
            console.log(err);
            return defaultString
        });
}
