import { CmdInMsg, CouponOutMsg, UserState, OMsg, InMsg, TEXT_IN, TextInMsg, TextOutMsg, User, TextOutContent, genCouponOut, POEM_GAME, NormalGameContent } from './definitions';
import { getUserState, saveUserState } from '../utils/state';
import poems from './zh/poem.json';
import coupons from './zh/coupon.json';
import stringSimilarity from 'string-similarity';
import { finishStrategyForText, NonTextStrategyOpt, StrategyForCmd, StrategyForNonText, StrategyForText, TextStrategyOpt, finishStrategyForCmd } from '../replier';
import randomItem from 'random-item';
import { timeStampNow } from '../utils/tools';
import { CMD_GIVEUP, CMD_REQUEST_POEM, defaultQuickReply, makeQuickReplies } from './zh/cmdList';
import { alreadyGetCoupon, createUserCoupon } from "../utils/coupon";

const STRATEGY_POEM_GAME = 'STRATEGY_POEM_GAME';

async function getCurrentState(inMsg: InMsg): Promise<UserState> {
    const getState = await getUserState(inMsg.user.id, POEM_GAME);
    console.log('getState: ' + JSON.stringify(getState))
    if (typeof getState === undefined) {
        return {
            user: inMsg.user,
            game: POEM_GAME,
            content: {
                state: 0,
                timeStamp: inMsg.timeStamp
            }
        };
    }
    return {
        user: inMsg.user,
        game: POEM_GAME,
        content: <NormalGameContent>getState,
    };
}

const fortunelist: string[] = [
    "大凶", "凶", "小凶", "中平", "小吉", "吉", "大吉"
]

const getFortuneString = (matchIdx: number) => {
    const fortuneNum: number = poems[matchIdx].fortune
    var fortuneString: string = fortunelist[fortuneNum + 3]
    console.log(fortuneNum, poems[matchIdx].fortune, fortuneString)
    return fortuneString
}

const matchPoem = (text: string) => {
    let matchIdx: number;
    const matchRatings = stringSimilarity.findBestMatch(
        text,
        poems.map(keyValues => keyValues.key)
    );
    if (matchRatings.bestMatch.rating < 0.1) {
        let randId = Math.floor(Math.random() * poems.length)
        matchIdx = randId
    } else {
        matchIdx = matchRatings.bestMatchIndex
    }
    console.log(matchIdx)
    return matchIdx
};

const getCouponIndex = (key: string) => {
    const keys: any = coupons.map(c => c.key)
    return keys.indexOf(key)
};

const hasCouponChance = async (user: User, couponEvent?: string) => {
    if (couponEvent === undefined) {
        return false;
    }
    return !await alreadyGetCoupon(user, couponEvent);
};

export const makePoem: StrategyForText = async (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => {
    const currentState = await getCurrentState(inMsg);
    console.log('makePoem.\n inMsg.content: ');
    console.log(inMsg.content);
    console.log('current State: ');
    console.log(currentState);
    const text = inMsg.content.text;
    if ((currentState.game === POEM_GAME) && (currentState?.content?.state === 1)) {
        //const poemstring: string = randomItem(poem)
        const matchIdx: number = matchPoem(text);
        const poemstring: string = randomItem(poems[matchIdx].poem);
        const fortuneString: string = getFortuneString(matchIdx);
        const game1String: string = poemstring.replace(/ /g, "\n") + '\n\n今日總結：' + fortuneString;
        const couponIndex: number = getCouponIndex(poems[matchIdx].key);
        const coupon = coupons[couponIndex];
        const canGetCoupon: boolean = await hasCouponChance(inMsg.user, coupon?.event);
        
        if (couponIndex < 0 || !canGetCoupon) {
            const outMsg = new TextOutMsg(inMsg.user,
                                          [STRATEGY_POEM_GAME],
                                          timeStampNow(),
                                          new TextOutContent(game1String, [], defaultQuickReply));
            const updateUserState: UserState = {
                user: inMsg.user,
                game: POEM_GAME,
                content: {
                    state: 0,
                    timeStamp: inMsg.timeStamp
                }
            };
            console.log(updateUserState)
            saveUserState(updateUserState.user.id, updateUserState.game, updateUserState.content)
            return sendAPI([outMsg as OMsg].concat(preOutMsgs));
            
        } else {
            const url = await createUserCoupon(inMsg.user, coupon.event);
            const outMsg = new TextOutMsg(inMsg.user,
                                          [STRATEGY_POEM_GAME],
                                          timeStampNow(),
                                          new TextOutContent(game1String, [], []));
            const couponInfoMsg = new TextOutMsg(inMsg.user,
                                                 [STRATEGY_POEM_GAME],
                                                 timeStampNow(),
                                                 new TextOutContent(coupon.info, [], []));
            const couponMsg = new CouponOutMsg(inMsg.user,
                                               [STRATEGY_POEM_GAME],
                                               timeStampNow(),
                                               genCouponOut(couponIndex, url));
            
            const updateUserState: UserState = {
                user: inMsg.user,
                game: POEM_GAME,
                content: {
                    state: 0,
                    timeStamp: inMsg.timeStamp,
                }
            };
            console.log('updateUserState:' + JSON.stringify(updateUserState));
            saveUserState(updateUserState.user.id, updateUserState.game, updateUserState.content);
            //return sendAPI([outMsg as OMsg, couponInfoMsg as OMsg, couponMsg as OMsg].concat(preOutMsgs));
            return sendAPI([outMsg as OMsg, couponInfoMsg as OMsg, couponMsg as OMsg].concat(preOutMsgs));
        }
    }

    return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt);
};

export const poemCommand: StrategyForCmd = async (
    inMsg: CmdInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    falseCallback?: StrategyForCmd
) => {
    console.log('poemCommand inMsg:');
    console.log(inMsg);
    const currentState = await getCurrentState(inMsg);
    console.log('poemCommand currentState:');
    console.log(currentState)
    const cmd = inMsg.content.cmd;
    // have to add history API

    if (cmd === CMD_REQUEST_POEM) {
        const game1String = "只要輸入一段話，讓萌太子幫你測今日吉凶，還可以獲得折價券呢，快來試試看吧！ \n\n例如：\n高雄發大財\n棋子還是塞子"
        // write into api
        const updateUserState: UserState = {
            user: inMsg.user,
            game: POEM_GAME,
            content: {
                state: 1,
                timeStamp: inMsg.timeStamp
            }
        }
        console.log(updateUserState)
        saveUserState(updateUserState.user.id, updateUserState.game, updateUserState.content)
        const outMsg = new TextOutMsg(inMsg.user,
                                      [STRATEGY_POEM_GAME],
                                      timeStampNow(),
                                      new TextOutContent(game1String, [], makeQuickReplies([CMD_GIVEUP])));
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));

    } else if (cmd === CMD_GIVEUP && (currentState.game === POEM_GAME) && (currentState.content?.state === 1)) {
        const game1String = "人生可不要輕易放棄才好\n如果還想聽聽本萌太子的預測\n只要輸入關鍵字\"請示萌太子\""
        // write into api
        const updateUserState: UserState = {
            user: inMsg.user,
            game: POEM_GAME,
            content: {
                state: 0,
                timeStamp: inMsg.timeStamp
            }
        }
        console.log(updateUserState)
        saveUserState(updateUserState.user.id, updateUserState.game, updateUserState.content)
        const outMsg = new TextOutMsg(inMsg.user,
                                      [STRATEGY_POEM_GAME],
                                      timeStampNow(),
                                      new TextOutContent(game1String, [], defaultQuickReply));
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
    }
    return finishStrategyForCmd(inMsg, sendAPI, preOutMsgs, falseCallback);
}

export const makePoemForNontext: StrategyForNonText = async (
    user: User,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: NonTextStrategyOpt
) => {
    let fcb: StrategyForText | undefined;
    if (opt && opt.falseCallback) {
        let oldFcb = opt.falseCallback;
        fcb = (
            inMsg: TextInMsg,
            sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
            preOutMsgs: OMsg[],
            newOpt?: TextStrategyOpt
        ) => {
            return oldFcb(
                inMsg.user,
                sendAPI,
                preOutMsgs,
                { quickReplies: newOpt?.quickReplies }
            )
        }
    }

    return makePoem(
        {
            user: user,
            content: {
                type: TEXT_IN,
                text: '中華民國萬歲',
            },
            timeStamp: timeStampNow()
        },
        sendAPI,
        preOutMsgs,
        { quickReplies: opt?.quickReplies, falseCallback: fcb }
    )
}
