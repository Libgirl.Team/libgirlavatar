import { InMsg, UserState, TextInMsg, OMsg, TextOutMsg, CmdInMsg, STRATEGY_BASIC_CMD, TextOutContent, MODE_GAME, NormalGameContent } from './definitions';
import { getUserState, saveUserState } from '../utils/state';
import { StrategyForText, StrategyForCmd, TextStrategyOpt, finishStrategyForText, finishStrategyForCmd } from '../replier';
import { timeStampNow } from '../utils/tools';
import { elasticSearchGame } from './zh/elasticSearch'
import { CMD_REQUEST_MODE, CMD_MODE_POSITIVE, CMD_GIVEUP, CMD_MODE_NEGATIVE, makeQuickReplies, defaultQuickReply, CMD_GAME_ENOUGH } from './zh/cmdList';

async function getCurrentState(inMsg: InMsg): Promise<UserState> {
    const getState = await getUserState(inMsg.user.id, MODE_GAME);
    if (typeof getState === undefined) {
        return {
            user: inMsg.user,
            game: MODE_GAME,
            content: {
                state: 0,
                timeStamp: inMsg.timeStamp
            }
        };
    }
    return {
        user: inMsg.user,
        game: MODE_GAME,
        content: <NormalGameContent>getState,
    };
}

const STRATEGY_MODE_GAME_POSITIVE = 'STRATEGY_MODE_GAME_POSITIVE';
const STRATEGY_MODE_GAME_NEGATIVE = 'STRATEGY_MODE_GAME_NEGATIVE';
export const modeChange: StrategyForText = async (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => {
    const currentState = await getCurrentState(inMsg);
    console.log('mode.\n inMsg.content: ');
    console.log(inMsg.content);
    console.log('current State: ');
    console.log(currentState);
    const text = inMsg.content.text;
    if ((currentState.game === MODE_GAME) && (currentState?.content?.state === 2)) {
        const game2String: string = await elasticSearchGame(text,currentState.content.state)
        // write into api
        const updateUserState: UserState = {
            user: inMsg.user,
            game: MODE_GAME,
            content: {
                state: 2,
                timeStamp: inMsg.timeStamp
            }
        }

        console.log(updateUserState)
        saveUserState(updateUserState.user.id, updateUserState.game, updateUserState.content)
        const outMsg = new TextOutMsg(inMsg.user,
                                      [STRATEGY_MODE_GAME_POSITIVE],
                                      timeStampNow(),
                                      new TextOutContent(game2String, [], makeQuickReplies([CMD_GIVEUP])));
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));

    } else if ((currentState.game === MODE_GAME) && (currentState?.content?.state === 3)) {
        const game2String: string = await elasticSearchGame(text,currentState.content.state)
        // write into api
        const updateUserState: UserState = {
            user: inMsg.user,
            game: MODE_GAME,
            content: {
                state: 3,
                timeStamp: inMsg.timeStamp
            }
        }
        console.log(updateUserState)
        saveUserState(updateUserState.user.id, updateUserState.game, updateUserState.content)
        const outMsg = new TextOutMsg(inMsg.user,
                                      [STRATEGY_MODE_GAME_NEGATIVE],
                                      timeStampNow(),
                                      new TextOutContent(game2String, [], makeQuickReplies([CMD_GIVEUP])))        
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
    }
    return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt);
}


export const modeCommand: StrategyForCmd = async (
    inMsg: CmdInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    falseCallback?: StrategyForCmd
) => {
    console.log('modeCommand inMsg:');
    console.log(inMsg);
    const currentState = await getCurrentState(inMsg);
    console.log('modeCommand currentState:');
    console.log(currentState)
    const cmd = inMsg.content.cmd;
    console.log('=======>>>>>game2', cmd)
    // have to add history API 
    if (cmd === CMD_REQUEST_MODE) {
        const game2String = "萌太子上身可以任意調整萌太⼦屬性，也許會讓你在對話中產⽣生“特殊”的樂趣呦！\n⾸先告訴本宮，你是哪種人呢？"
        // write into api
        const updateUserState: UserState = {
            user: inMsg.user,
            game: MODE_GAME,
            content: {
                state: 1,
                timeStamp: inMsg.timeStamp
            }
        }
        console.log(updateUserState)
        saveUserState(updateUserState.user.id, updateUserState.game, updateUserState.content)
        const outMsg = new TextOutMsg(inMsg.user,
                                      [STRATEGY_BASIC_CMD],
                                      timeStampNow(),
                                      new TextOutContent(game2String, [], makeQuickReplies([CMD_MODE_POSITIVE, CMD_MODE_NEGATIVE])));        
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
        
    } else if (cmd === CMD_MODE_POSITIVE) {
        var game2String = "多久沒有聽到肉麻話了呢？\n跟我說說你今天過得如何吧！\n讓我們一起看看你有多棒！"
        // write into api
        const updateUserState: UserState = {
            user: inMsg.user,
            game: MODE_GAME,
            content: {
                state: 2,
                timeStamp: inMsg.timeStamp
            }
        }
        console.log(updateUserState)
        saveUserState(updateUserState.user.id, updateUserState.game, updateUserState.content)
        const outMsg = new TextOutMsg(inMsg.user,
                                      [STRATEGY_BASIC_CMD],
                                      timeStampNow(),
                                      new TextOutContent(game2String, [], makeQuickReplies([CMD_GIVEUP])));
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
        
    } else if (cmd === CMD_MODE_NEGATIVE) {
        var game2String = "本宮罵你可不會有任何憐憫，後果自負。\n怕玻璃心碎裂現在還可以離開，放棄並不可恥。\n留下來就說今天你又幹了甚麼蠢事吧！"
        // write into api
        const updateUserState: UserState = {
            user: inMsg.user,
            game: MODE_GAME,
            content: {
                state: 3,
                timeStamp: inMsg.timeStamp
            }
        }
        console.log(updateUserState)
        saveUserState(updateUserState.user.id, updateUserState.game, updateUserState.content)
        const outMsg = new TextOutMsg(inMsg.user,
                                      [STRATEGY_BASIC_CMD],
                                      timeStampNow(),
                                      new TextOutContent(game2String, [], makeQuickReplies([CMD_GIVEUP])));
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
        
    } else if ((cmd === CMD_GIVEUP || cmd === CMD_GAME_ENOUGH) &&
               (currentState.game === MODE_GAME) &&
               (currentState.content?.state > 0)) {
        const game2String = "才這樣就受不了啦？人生可不要輕易放棄才好～\n如果還想跟以往不同的萌太子對話，\n只要輸入關鍵字：\n\"萌太子上身\""
        // write into api
        const updateUserState: UserState = {
            user: inMsg.user,
            game: MODE_GAME,
            content: {
                state: 0,
                timeStamp: inMsg.timeStamp
            }
        }
        console.log(updateUserState)
        saveUserState(updateUserState.user.id, updateUserState.game, updateUserState.content)
        const outMsg = new TextOutMsg(inMsg.user,
                                      [STRATEGY_BASIC_CMD],
                                      timeStampNow(),
                                      new TextOutContent(game2String, [], defaultQuickReply));
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
        
    }
    
    return finishStrategyForCmd(inMsg, sendAPI, preOutMsgs, falseCallback);
}
