
export const greeting = [ 'Hi!'
                        , 'I\'m here'
    			        , 'How was your day?'
    			        , 'What\'s up?'
    				    , 'Hi, How is it going?'
    				    , 'It is a pleasure to meet you.'
    				    , 'I\'m doing well. How are you?'
    				    , 'How are you doing?'
    				    ];
export const bye      = [ 'Bye!'
                        , 'Goodbye!'
                        , 'I\'m always here!'
                        , 'Have a good day!'
                        , 'It is a pleasure to meet you.'
                        ] ;
export const thanks   = [ 'You\'re welcome.'
                        , 'Piece of cake!'
                        , 'My pleasure.'
                        , 'It is fine.'
                        , 'Come to me in any case.'
                        , 'Glad to help.'
                        , 'No problem!'
                        , 'No worries!'
                        , 'Any time'
                        , 'Sure!'
                        ] ;
export const opQ      = [ 'Tell me about more detail.'
                        , 'It sounds just like what you said.'
                        , 'I\'m still listening.'
                        , 'Of couse!'
                        , 'Show me more.'
				        ];
export const activeL  = [ 'Tell me about more detail.'
                        , 'I\'m still listening.'
					    , 'emmm... You\'re right.'
					    ];
export const QA       = [ 'I havn\'t learn that yet, but I found this on Wiki.'
                        , 'I have no clue about that, could you say more detail?'
					    ];
						
export const givenD   = [ 'What I know is '
                        , 'I have no clue about that, could you say more detail?'
					    ];
						