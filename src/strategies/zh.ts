import { getGoogleAnalysis } from '../utils/google';
import { StrategyForText, StrategyForNonText, TextStrategyOpt, NonTextStrategyOpt, finishStrategyForText } from '../replier';
import zhGreetings from './zh/greetings.json';
import zhQuestionAnswering from './zh/questionAnswering.json';
import zhBlurt from './zh/blurt.json';
import randomItem from 'random-item';
import stringSimilarity from 'string-similarity';
import { newEntitySentence } from './zh/describeEntity';
import { genOpenQuestion } from './zh/openQuestions';
import { genActiveListening } from './zh/activeListen';
import { TextInMsg, RecmdOutMsg, genRecmdOut, TextOutMsg, User, CmdInMsg, CMD_IN, QuickReply, ImgOutMsg, ImgOutContent, TextOutContent, OMsg } from './definitions';
import { timeStampNow } from '../utils/tools';
import { defaultQuickReply, textToCmd } from './zh/cmdList';
import { elasticSearchReply } from './zh/elasticSearch';
import { blurtImgs } from './img_collection';
import recmds from './zh/recmd.json';
import { generateUrl } from "../utils/redirect-url";


const callMainMenu: string = "召喚萌太子"
const replyMainMenu: string[] = ["這不是來了嗎？", "找本宮有何貴幹？", "說吧！你想要甚麼？"]
const yesNoQuestionChineseBeginner: string[] = ["是不是", "要不要", "想不想", "知不知"];
const whQuestionText: string[] = ["為什麼", "怎麼", "怎", "什麼", "哪裏", "哪裡", "如何", "誰"];
const questionMark: string[] = ["?", "？"];
const questionEndings: string[] = ["嗎", "好不好", "是不是", "要不要", "想不想", "知不知", "呢"];
const greetingsThreshold = 0.6;

function rndMenuQuickReplies(): QuickReply[] {
    console.log('use rndMenuQuickReplies.');
    return (Math.random() < 0.4) ? defaultQuickReply : [];
}

const STRATEGY_GREET = 'STRATEGY_GREET';
export function sendGreet(inText: string,
    user: User,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    strategy: string) {
    console.log('stretagy: greeting.');
    const greetingText = randomItem(zhGreetings[
        stringSimilarity.findBestMatch(
            inText,
            zhGreetings.map(keyValues => keyValues.key)).bestMatchIndex
    ].values);
    const text = randomItem([greetingText].concat(replyMainMenu))
    const outMsg = new TextOutMsg(
        user,
        (strategy === STRATEGY_GREET) ? [STRATEGY_GREET] : [strategy, STRATEGY_GREET],
        timeStampNow(),
        new TextOutContent(text, [], defaultQuickReply)
    )
    return sendAPI(([outMsg as OMsg]).concat(preOutMsgs));
}

export const greet: StrategyForText = async (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => {
    console.log('greet inMsg.content');
    console.log(inMsg.content);
    if (inMsg.content.fbNlp) {
        const nlpEntities = inMsg.content.fbNlp.entities;
        console.log('greet with fbNlp.');
        // console.log(nlpEntities);
        if (nlpEntities?.greetings && nlpEntities.greetings[0].confidence > greetingsThreshold ||
            nlpEntities?.bye && nlpEntities.bye[0].confidence > greetingsThreshold ||
            nlpEntities?.thanks && nlpEntities.thanks[0].confidence > greetingsThreshold ||
            callMainMenu.includes(inMsg.content.text)) {
            return sendGreet(inMsg.content.text, inMsg.user, sendAPI, preOutMsgs, STRATEGY_GREET);
        }
    }

    return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt);
}

const activeListeningTextCountThreshold = 30;
const zhActiveListeningTextCountThreshold = 0;

const STRATEGY_ACTIVE_LISTEN = 'STRATEGY_ACTIVE_LISTEN';
export const activeListen: StrategyForText = async (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => {
    console.log('activeListen inMsg.content');
    const inText = inMsg.content.text;

    const gglNlp = (opt?.googleNlpAnalysis) ? opt.googleNlpAnalysis :
        await getGoogleAnalysis(inMsg.content.text);

    if ((((gglNlp.entities.language == 'zh-Hant' &&
        inText.length > zhActiveListeningTextCountThreshold) ||
        (gglNlp.entities.language == 'en' &&
            inText.length > activeListeningTextCountThreshold)))) {

        console.log('stretagy: active listening.');
        const outText = genActiveListening(gglNlp.sentiment.documentSentiment.score);
        const outMsg = new TextOutMsg(
            inMsg.user,
            [STRATEGY_ACTIVE_LISTEN],
            timeStampNow(),
            new TextOutContent(outText, [], opt?.quickReplies ? opt.quickReplies : rndMenuQuickReplies())
        );
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
    }

    return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt, gglNlp);
}

const STRATEGY_ANSWER_QUESTION = 'STRATEGY_ANSWER_QUESTION';
export const answerQuestion: StrategyForText = async (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => {
    console.log('answerQuestion inMsg.content');
    console.log(inMsg.content);

    const gglNlp = (opt?.googleNlpAnalysis) ? opt.googleNlpAnalysis :
        await getGoogleAnalysis(inMsg.content.text);

    let pred: boolean;
    if (gglNlp.syntax.tokens instanceof Array) {
        let messageTokens: any[] = gglNlp.syntax.tokens;
        let firstToken = messageTokens[0].text.content.toLowerCase();
        let secondToken;
        let lastToken = messageTokens[messageTokens.length - 1].text.content.toLowerCase();

        if (messageTokens[1]) {
            if (messageTokens[2] &&
                messageTokens[2].lemma.length > 1 &&
                messageTokens[1].lemma.length === 1 &&
                messageTokens[1].lemma === messageTokens[2].lemma[1]) {
                //get X不X form such as 要不要, 想不想
                secondToken = messageTokens[1].lemma + messageTokens[2].lemma;
            } else {
                secondToken = messageTokens[1].text.content.toLowerCase();
            }
        };

        const isQuestion = yesNoQuestionChineseBeginner.includes(firstToken) ||
            yesNoQuestionChineseBeginner.includes(secondToken) ||
            whQuestionText.includes(firstToken) ||
            whQuestionText.includes(secondToken) ||
            whQuestionText.includes(lastToken) ||
            questionMark.includes(lastToken) ||
            questionEndings.includes(lastToken);
        pred = isQuestion;
    } else {
        pred = false;
    }

    if (pred) {
        console.log('stretagy: question answering.');
        let outText: string;
        const matchRatings = stringSimilarity.findBestMatch(
            inMsg.content.text,
            zhQuestionAnswering.map(keyValue => keyValue.key)
        );
        if (matchRatings.bestMatch.rating < 0.1) {
            outText = zhQuestionAnswering[Math.floor(Math.random() * zhQuestionAnswering.length)].value;;
        } else {
            outText = zhQuestionAnswering[matchRatings.bestMatchIndex].value;
        }

        const outMsg = new TextOutMsg(
            inMsg.user,
            [STRATEGY_ANSWER_QUESTION],
            timeStampNow(),
            new TextOutContent(outText, [], opt?.quickReplies ? opt.quickReplies : rndMenuQuickReplies())
        )
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
    }

    return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt, gglNlp);
}

const STRATEGY_ASK_OPEN_QUESTION = 'STRATEGY_ASK_OPEN_QUESTION';
export const askOpenQuestion: StrategyForText = async (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => {
    console.log('askOpenQuestion inMsg.content');
    console.log(inMsg.content);

    const gglNlp = (opt?.googleNlpAnalysis) ? opt.googleNlpAnalysis :
        await getGoogleAnalysis(inMsg.content.text);

    if (Math.abs(gglNlp.sentiment.documentSentiment.score) > 0.3) {
        console.log('stretagy: asking open ended questions.');

        const outText = genOpenQuestion(gglNlp.entities.entities);
        const outMsg = new TextOutMsg(
            inMsg.user,
            [STRATEGY_ASK_OPEN_QUESTION],
            timeStampNow(),
            new TextOutContent(outText, [], opt?.quickReplies ? opt.quickReplies : rndMenuQuickReplies())
        );
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));

    }

    return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt, gglNlp);
}

const STRATEGY_DESCRIBE_ENTITY = 'STRATEGY_DESCRIBE_ENTITY';
export const describeEntity: StrategyForText = async (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => {
    console.log('describeEneity inMsg.content');
    console.log(inMsg.content);

    const gglNlp = (opt?.googleNlpAnalysis) ? opt.googleNlpAnalysis :
        await getGoogleAnalysis(inMsg.content.text);

    if (gglNlp.entities.entities.length > 0) {
        console.log('stretagy: givingDescriptionAboutEntities.');
        const entityArray = <any[]>gglNlp.entities.entities;
        const outText = newEntitySentence(entityArray);

        const outMsg = new TextOutMsg(
            inMsg.user,
            [STRATEGY_DESCRIBE_ENTITY],
            timeStampNow(),
            new TextOutContent(outText, [], opt?.quickReplies ? opt.quickReplies : rndMenuQuickReplies())
        );
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
    }
    return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt, gglNlp);
}

const STRATEGY_ELASTIC_SEARCH = 'STRATEGY_ELASTIC_SEARCH';
export const elasticSearch: StrategyForText = async (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => {
    const elasticOut: any = await elasticSearchReply(inMsg.content.text)
    const outText = elasticOut.reply;
    const outRecmd = elasticOut.recmd;


    const getRecmdIndex = (key: string) => {
        const keys: any = recmds.map(c => c.key)
        return keys.indexOf(key)
    };
    const recmdIndex: number = getRecmdIndex(outRecmd);
    if (outText) {
        if (recmdIndex < 0) {
            const outMsg = new TextOutMsg(
                inMsg.user,
                [STRATEGY_ELASTIC_SEARCH],
                timeStampNow(),
                new TextOutContent(outText, [], opt?.quickReplies ? opt.quickReplies : rndMenuQuickReplies())
            );
            return sendAPI([outMsg as OMsg].concat(preOutMsgs));

        } else {
            const url: string = generateUrl(inMsg.user,recmds[recmdIndex].event)
            const outMsg = new TextOutMsg(
                inMsg.user,
                [STRATEGY_ELASTIC_SEARCH],
                timeStampNow(),
                new TextOutContent(outText, [], opt?.quickReplies ? opt.quickReplies : rndMenuQuickReplies())
            );
            const recmdMsg = new RecmdOutMsg(inMsg.user,
                [STRATEGY_ELASTIC_SEARCH],
                timeStampNow(),
                genRecmdOut(recmdIndex,url));

            //return sendAPI([outMsg as OMsg, couponInfoMsg as OMsg, couponMsg as OMsg].concat(preOutMsgs));
            return sendAPI([outMsg as OMsg, recmdMsg as OMsg].concat(preOutMsgs));
        }
    }

    return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt);
}

export const STRATEGY_BLURT = 'STRATEGT_BLURT';
export const blurt: StrategyForNonText = (
    user: User,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: NonTextStrategyOpt
) => {
    console.log('stretagy: blurt.');
    const qls = opt?.quickReplies ? opt.quickReplies : rndMenuQuickReplies();
    if (Math.random() < 0.2) {
        const outMsg = new ImgOutMsg(user, [STRATEGY_BLURT], timeStampNow(), new ImgOutContent(randomItem(blurtImgs), qls))
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
    } else {
        const outText = randomItem(zhBlurt.map((pair: { key: string, value: string }) => pair.value));
        const outMsg = new TextOutMsg(
            user,
            [STRATEGY_BLURT],
            timeStampNow(),
            new TextOutContent(outText, [], qls)
        );
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
    }
}


export function skipError(
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) {
    if (inMsg.content.text.match(/\bhttps?:\/\/\S+/gi)) {
        console.log('====>skip the url')
        return blurt(inMsg.user, sendAPI, preOutMsgs, { quickReplies: opt?.quickReplies })
    }
    return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt);
}

export function genTakeAsCmd(
    cmdReplier: (cmdInMsg: CmdInMsg, preOutMsgs: OMsg[]) => Promise<string[]>
): StrategyForText {
    return async (
        inMsg: TextInMsg,
        sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
        preOutMsgs: OMsg[],
        opt?: TextStrategyOpt
    ) => {
        const foundCmd = textToCmd(inMsg.content.text);

        if (foundCmd) {
            const cmdInMsg: CmdInMsg = {
                user: inMsg.user,
                timeStamp: inMsg.timeStamp,
                content: {
                    type: CMD_IN,
                    cmd: foundCmd,
                    fbNlp: inMsg.content.fbNlp
                },
            };
            return cmdReplier(cmdInMsg, preOutMsgs);
        }
        return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt);
    }
}
