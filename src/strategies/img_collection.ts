export type ImgLabel =
    typeof IMG_AVATAR_QR_CODE;

export const IMG_AVATAR_QR_CODE = 'IMG_AVATAR_QR_CODE';
export const IMG_AVATAR_BROKEN = 'IMG_AVATAR_BROKEN';
export const IMG_AVATAR_ANGRY = 'IMG_AVATAR_ANGRY';
export const IMG_AVATAR_HAPPY = 'IMG_AVATAR_HAPPY';
export const IMG_AVATAR_NORMAL = 'IMG_AVATAR_NORMAL;';
export const IMG_AVATAR_CRY = 'IMG_AVATAR_CRY;';
export const IMG_AVATAR_THINK = 'IMG_AVATAR_THINK;';
export const IMG_AVATAR_BLACK = 'IMG_AVATAR_BLACK;';
export const IMG_COUPON = 'IMG_COUPON';

export const blurtImgs = [ IMG_AVATAR_BLACK, IMG_AVATAR_BROKEN, IMG_AVATAR_HAPPY, IMG_AVATAR_NORMAL, IMG_AVATAR_THINK];

const avartarImgs: { label: string, url: string }[] = [
    { label: IMG_AVATAR_BROKEN, url: 'https://scontent.ftpe7-2.fna.fbcdn.net/v/t1.0-9/80484053_1175289692677165_3530269659100086272_n.png?_nc_cat=111&_nc_ohc=IN8iy47SxjcAQnEM2k1P5kuKvkoo8z4LmwuxQ3Xd2P0derg8BhTuhlrCQ&_nc_ht=scontent.ftpe7-2.fna&oh=045e51bd7ec970ae6a84e8d3a73d8c1c&oe=5EB38396'},
    { label: IMG_AVATAR_ANGRY, url: 'https://scontent.ftpe7-1.fna.fbcdn.net/v/t1.0-9/79618356_1175289719343829_716020659410436096_n.png?_nc_cat=100&_nc_ohc=9A8jyKs8KoMAQmlZiei87OYWuTn6zvb5O2CFtjYiwwv0sXCEKqu6InOvA&_nc_ht=scontent.ftpe7-1.fna&oh=4a18046dacda36ec541b8f427baddab7&oe=5EB44AB2'},
    { label: IMG_AVATAR_HAPPY, url: 'https://scontent.ftpe7-4.fna.fbcdn.net/v/t1.0-9/80969681_1175289746010493_2476975439956934656_n.png?_nc_cat=107&_nc_ohc=zRSjrJfhwM0AQnMRlbz7WsUDJVZP0ilE947IPoL_Nr3tokF3JBAwiVr4w&_nc_ht=scontent.ftpe7-4.fna&oh=eb705afc77793529778d55347fe659aa&oe=5EABB05B'},
    { label: IMG_AVATAR_NORMAL, url: 'https://scontent.ftpe7-1.fna.fbcdn.net/v/t1.0-9/80043165_1175289769343824_5966488775700774912_n.png?_nc_cat=110&_nc_ohc=1IPxNHXhxHkAQmLDP67yhVGgsksbzys9DqgEuXr8GcCiSJs73ul2yHsyg&_nc_ht=scontent.ftpe7-1.fna&oh=75d8975a92ca070999854514584feeb0&oe=5EA9AFA4'},
    { label: IMG_AVATAR_CRY, url: 'https://scontent.ftpe7-1.fna.fbcdn.net/v/t1.0-9/80425545_1175289762677158_880410648628953088_n.png?_nc_cat=106&_nc_ohc=7BsXXAl_faUAQmcLWVXW7O72Q9IB0ktQzM3OF2h65hKZtH2JJlvbDYFPw&_nc_ht=scontent.ftpe7-1.fna&oh=3de813dd325d4493633f11c780dc3553&oe=5E6F8CAD'},
    { label: IMG_AVATAR_THINK, url: 'https://scontent.ftpe7-3.fna.fbcdn.net/v/t1.0-9/80478998_1175290372677097_2649217604350115840_n.png?_nc_cat=108&_nc_ohc=hRLlJx8S8IYAQmULg94Hoikr-xOSjN9rcpdPsuevid3DlVEZOIvc2ixFg&_nc_ht=scontent.ftpe7-3.fna&oh=a9a505bbdd4096c2ee885f3989dfed35&oe=5E76E039'},
    { label: IMG_AVATAR_BLACK, url: 'https://scontent.ftpe7-4.fna.fbcdn.net/v/t1.0-9/80034013_1175290256010442_9021566431447220224_o.png?_nc_cat=101&_nc_ohc=KvsMDrFNYLMAQm-5xnF-M4Iv7FZ7IIxcx7018OjTHaZi-EWNQtks9uysw&_nc_ht=scontent.ftpe7-4.fna&oh=9de84bd3b289da125077b4e714c7ca68&oe=5E6712CA'},
]



const fbDefaultImgItem = { label: IMG_AVATAR_NORMAL, url: 'https://scontent.ftpe7-1.fna.fbcdn.net/v/t1.0-9/80043165_1175289769343824_5966488775700774912_n.png?_nc_cat=110&_nc_ohc=1IPxNHXhxHkAQmLDP67yhVGgsksbzys9DqgEuXr8GcCiSJs73ul2yHsyg&_nc_ht=scontent.ftpe7-1.fna&oh=75d8975a92ca070999854514584feeb0&oe=5EA9AFA4'};

export const fbImgCollection: { label: string, url: string }[] = [
    { label: IMG_AVATAR_QR_CODE,
      url: "https://scontent.ftpe7-4.fna.fbcdn.net/v/t1.0-9/79670169_1174304466109021_4959673240935989248_n.png?_nc_cat=101&_nc_ohc=xZcYCs1jARIAQkzDOhMmkzhN54xH1V2G0ZJE11AZTRyentjPFtL_D1ScA&_nc_ht=scontent.ftpe7-4.fna&oh=be3e9c6119bb7bb37e3757a90e651134&oe=5E7413F4"},
    {label: IMG_COUPON, url: "https://images.squarespace-cdn.com/content/5c05f9eff8370aabeed9bdbd/1567392207611-798ON1AE4QEIZNGHFJ5D/2019%25E6%258B%2589%25E4%25BA%259ELOGO-02.jpg?content-type=image%2Fjpeg"}
    
].concat(avartarImgs);

export function fbImgUrl(label: string): string {
    const found = fbImgCollection.find(item => (item.label === label));
    if (found) {
        return found.url;
    } else {
        return fbDefaultImgItem.url;
    }
}

const lineDefaultImgItem = { label: IMG_AVATAR_NORMAL, url: 'https://scontent.ftpe7-1.fna.fbcdn.net/v/t1.0-9/80043165_1175289769343824_5966488775700774912_n.png?_nc_cat=110&_nc_ohc=1IPxNHXhxHkAQmLDP67yhVGgsksbzys9DqgEuXr8GcCiSJs73ul2yHsyg&_nc_ht=scontent.ftpe7-1.fna&oh=75d8975a92ca070999854514584feeb0&oe=5EA9AFA4'};

export const lineImgCollection: { label: string, url: string }[] = [
    { label: IMG_AVATAR_QR_CODE, url: "https://qr-official.line.me/sid/L/836qxzrg.png"},
    {label: IMG_COUPON, url: "https://images.squarespace-cdn.com/content/5c05f9eff8370aabeed9bdbd/1567392207611-798ON1AE4QEIZNGHFJ5D/2019%25E6%258B%2589%25E4%25BA%259ELOGO-02.jpg?content-type=image%2Fjpeg"}
].concat(avartarImgs);

export function lineImgUrl(label: string): string {
    const found = lineImgCollection.find(item => (item.label === label));
    if (found) {
        return found.url;
    } else {
        return lineDefaultImgItem.url;
    }
}
