export const UNITED_DAILY_NEWS = 'UNITED_DAILY_NEWS';
export const LIBERTY_TIMES = 'LIBERTY_TIMES';
import coupons from './zh/coupon.json';
import recmds from './zh/recmd.json';

export function getZhNewsMediaName(newsMedia: NewsMedia): string {
    switch (newsMedia) {
        case UNITED_DAILY_NEWS:
            return "聯合新聞網";
        case LIBERTY_TIMES:
            return "自由時報";
    }
}

export function getNewsMediaPreviewUrl(newsMedia: NewsMedia): string {
    switch (newsMedia) {
        case UNITED_DAILY_NEWS:
            return "https://udn.com/static/img/UDN_BABY.png";
        case LIBERTY_TIMES:
            return "https://cache.ltn.com.tw/images/rwd_ltnlogo.png";
    }
}

export type NewsMedia =
    typeof UNITED_DAILY_NEWS |
    typeof LIBERTY_TIMES;

//export type InMsg = TextInMsg | CmdInMsg | OtherInMsg;
export type InMsg = {
    user: User,
    content: InContent,
    timeStamp: number,
}

export type InContent = TextIn | CmdIn | OtherIn;
export const TEXT_IN = 'TEXT_IN';
export const OTHER_IN = 'OTHER_IN';
export const CMD_IN = 'CMD_IN';

export interface TextInMsg {
    user: User,
    content: TextIn,
    timeStamp: number,
}

export interface TextIn {
    type: typeof TEXT_IN,
    text: string,
    fbNlp?: any,
}

export interface CmdInMsg {
    user: User,
    content: CmdIn,
    timeStamp: number,
}

export interface CmdIn {
    type: typeof CMD_IN,
    cmd: string,
    fbNlp?: any,
}

export interface OtherInMsg {
    user: User,
    content: OtherIn,
    timeStamp: number,
}

export interface OtherIn {
    type: typeof OTHER_IN,
    body: any,
}

// methods to transform OutMsg types to platform-specific request body.
// when null, no request to the platform will be performed.
export interface OutParser {
    text(msg: TextOutMsg): any[] | null,
    news(msg: NewsOutMsg): any[] | null,
    lifeOut(msg: LifeOutMsg): any[] | null,
    lifeCall(msg: LifeCallMsg): any[] | null,
    img(msg: ImgOutMsg): any[] | null,
    typing(msg: ImitateTypingOutMsg): any[] | null,
    coupon(msg: CouponOutMsg): any[] | null,
    recmd(msg: RecmdOutMsg): any[] | null,
}

class BaseOutMsg {
    constructor(public user: User,
                public strategy: string[],
                public timeStamp: number) { };
}

export type OMsg = OutMsg<OutContent>;

export interface OutMsg<T extends OutContent> {
    outBodies(outParser: OutParser): any[] | null, // generate the request bodies to be sent.
    user: User;
    timeStamp: number; // creation time by milli second
    strategy: string[]; // strategies labels for data analysis
    content: T,
    copyForQuickReplies(quickReplies: QuickReply[]): OutMsg<T>, // for sending multi msgs simultaneously.
}

export interface OutContent {
    type: string,
    // for sending multi msgs simultaneously.
    getQuickReplies(): QuickReply[],
}

export class LifeOutMsg extends BaseOutMsg implements OutMsg<LifeOutContent> {
    constructor(user: User,
                strategy: string[],
                timeStamp: number,
                public content: LifeOutContent) {
        super(user, strategy, timeStamp);
    };
    outBodies = (outParser: OutParser) => outParser.lifeOut(this);
    copyForQuickReplies(quickReplies: QuickReply[]): LifeOutMsg {
        return new LifeOutMsg(this.user,
                              this.strategy,
                              this.timeStamp,
                              new LifeOutContent(this.content.text,
                                                 this.content.cmds,
                                                 quickReplies));
    };
}

export const LIFE_OUT = 'LIFE_OUT';
export class LifeOutContent implements OutContent {
    type: string = LIFE_OUT;
    constructor(public text: string,
                public cmds: string[],
                public quickReplies: QuickReply[]) { };
    getQuickReplies(): QuickReply[] {
        return this.quickReplies;
    }
}

export class LifeCallMsg extends BaseOutMsg implements OutMsg<LifeCallContent> {
    constructor(user: User,
                strategy: string[],
                timeStamp: number,
                public content: LifeCallContent) {
        super(user, strategy, timeStamp);
    };
    outBodies = (outParser: OutParser) => outParser.lifeCall(this);
    copyForQuickReplies(quickReplies: QuickReply[]): LifeCallMsg {
        return new LifeCallMsg(this.user,
                               this.strategy,
                               this.timeStamp,
                               new LifeCallContent(this.content.text,
                                                   this.content.phones,
                                                   quickReplies))
    }
}

export const LIFE_CALL = 'LIFE_CALL';
export class LifeCallContent implements OutContent {
    type: string = LIFE_CALL;
    constructor(public text: string,
                public phones: LifeNumber[],
                public quickReplies: QuickReply[]) {
    };
    getQuickReplies = () => this.quickReplies;
}

export interface LifeNumber {
    name: string,
    number: string
}

export class NewsOutMsg extends BaseOutMsg implements OutMsg<NewsOutContent> {
    constructor(user: User,
                strategy: string[],
                timeStamp: number,
                public content: NewsOutContent) {
        super(user, strategy, timeStamp);
    }
    outBodies = (outParser: OutParser) => outParser.news(this);
    copyForQuickReplies(quickReplies: QuickReply[]): NewsOutMsg {
        return new NewsOutMsg(this.user,
                              this.strategy,
                              this.timeStamp,
                              new NewsOutContent(this.content.title,
                                                 this.content.url,
                                                 this.content.newsMedia,
                                                 quickReplies));
    };
}

export const NEWS_OUT = 'NEWS_OUT';
export class NewsOutContent implements OutContent {
    type: string = NEWS_OUT;
    constructor(public title: string,
                public url: string,
                public newsMedia: NewsMedia,
                public quickReplies: QuickReply[]) { };
    getQuickReplies = () => this.quickReplies;
}

export function newsOutstring(newsOutContent: NewsOutContent): string {
    return `${getZhNewsMediaName(newsOutContent.newsMedia)}, ${newsOutContent.title}, ${newsOutContent.url}`;
}

export class TextOutMsg extends BaseOutMsg implements OutMsg<TextOutContent> {
    constructor(user: User, strategy: string[], timeStamp: number, public content: TextOutContent) {
        super(user, strategy, timeStamp);
    }
    outBodies = (outParser: OutParser) => outParser.text(this);
    copyForQuickReplies(quickReplies: QuickReply[]): TextOutMsg {
        return new TextOutMsg(this.user,
                              this.strategy,
                              this.timeStamp,
                              new TextOutContent(this.content.text,
                                                 this.content.ref,
                                                 quickReplies));
    }
}

export const TEXT_OUT = 'TEXT_OUT';
export class TextOutContent implements OutContent {
    type: string = TEXT_OUT;
    constructor(public text: string,
                public ref: MemeRef[],
                public quickReplies: QuickReply[]) { };
    getQuickReplies = () => this.quickReplies;
}

export interface MemeRef {
    originalText: string,
    person: string,
    url: string,
}

export class ImgOutMsg extends BaseOutMsg implements OutMsg<ImgOutContent> {
    content: ImgOutContent;
    constructor(user: User, strategy: string[], timeStamp: number, content: ImgOutContent) {
        super(user, strategy, timeStamp);
        this.content = content;
    }
    outBodies = (outParser: OutParser) => outParser.img(this);
    copyForQuickReplies(quickReplies: QuickReply[]): ImgOutMsg {
        return new ImgOutMsg(this.user,
                             this.strategy,
                             this.timeStamp,
                             new ImgOutContent(this.content.label,
                                               quickReplies));
    };
}

export const IMG_OUT = 'IMG_OUT';
export class ImgOutContent implements OutContent {
    type: string = IMG_OUT;
    constructor(public label: string,
                public quickReplies: QuickReply[]) { }
    getQuickReplies = (): QuickReply[] => this.quickReplies;
}

export class ImitateTypingOutMsg extends BaseOutMsg implements OutMsg<ImitateTypingContent> {
    content: ImitateTypingContent;
    constructor(user: User, strategy: string[], timeStamp: number, content: ImitateTypingContent) {
        super(user, strategy, timeStamp);
        this.content = content;
    }
    outBodies = (outParser: OutParser) => outParser.typing(this);
    copyForQuickReplies = (_: QuickReply[]) => this;
}

export const IMITATE_TYPING_OUT = 'IMITATE_TYPING_OUT';
export class ImitateTypingContent implements OutContent {
    type: string = IMITATE_TYPING_OUT;
    constructor(public milliSeconds: number) { };
    getQuickReplies = (): QuickReply[] => [];
}

export class CouponOutMsg extends BaseOutMsg implements OutMsg<CouponOutContent> {
    content: CouponOutContent;
    constructor(user: User, strategy: string[], timeStamp: number, content: CouponOutContent) {
        super(user, strategy, timeStamp);
        this.content = content
    }
    outBodies = (outParser: OutParser) => outParser.coupon(this);
    copyForQuickReplies(quickReplies: QuickReply[]) {
        return new CouponOutMsg(this.user,
                                this.strategy,
                                this.timeStamp,
                                new CouponOutContent(this.content.title,
                                                     this.content.url,
                                                     this.content.img_url,
                                                     this.content.action,
                                                     this.content.subtitle,
                                                     quickReplies));
    }
}

export const COUPON_OUT = 'COUPON_OUT';
export class CouponOutContent implements OutContent {
    type: string = COUPON_OUT;
    constructor(public title: string,
                public url: string,
                public img_url: string,
                public action: string,
                public subtitle: string,
                public quickReplies: QuickReply[]) { };
    getQuickReplies = () => this.quickReplies;
}

export function genCouponOut(index: number, url: string): CouponOutContent {
    return new CouponOutContent(coupons[index].title,
                                url,
                                coupons[index].img_url,
                                coupons[index].action,
                                coupons[index].subtitle,
                                []);
}


export class RecmdOutMsg extends BaseOutMsg implements OutMsg<RecmdOutContent> {
    content: RecmdOutContent;
    constructor(user: User, strategy: string[], timeStamp: number, content: RecmdOutContent) {
        super(user, strategy, timeStamp);
        this.content = content
    }
    outBodies = (outParser: OutParser) => outParser.recmd(this);
    copyForQuickReplies(quickReplies: QuickReply[]) {
        return new RecmdOutMsg(this.user,
                               this.strategy,
                               this.timeStamp,
                               new RecmdOutContent(this.content.title,
                                                   this.content.url,
                                                   this.content.img_url,
                                                   this.content.action,
                                                   this.content.subtitle,
                                                   quickReplies));
    }
}

export const RECMD_OUT = 'RECMD_OUT';
export class RecmdOutContent implements OutContent {
    type: string = RECMD_OUT;
    constructor(public title: string,
                public url: string,
                public img_url: string,
                public action: string,
                public subtitle: string,
                public quickReplies: QuickReply[]) { };
    getQuickReplies = () => this.quickReplies;
}
export function genRecmdOut(index: number,url: string): RecmdOutContent {
    return new RecmdOutContent(recmds[index].title,
                               url,
                               recmds[index].img_url,
                               recmds[index].action,
                               recmds[index].subtitle,
                               []);
}

export const UNTRIGGERED = 'UNTRIGGERED';
export const PLAYING = 'PLAYING';
export const PLAYED = 'PLAYED';

export interface SessionState {
    user: User,
}

export type UserState = NormalGameState | PersistentReplyGameState | LifeState | RoleState;

export const AVATAR_ROLE = 'AVATAR_ROLE';
export type RoleState = {
    user: User,
    game: typeof AVATAR_ROLE,
    content: RoleContent
}

export type Roles = typeof ROLE_LIBGIRL | typeof ROLE_GIRLFRIEND;
export const ROLE_LIBGIRL = 'ROLE_LIBGIRL'; // the main general role.
export const ROLE_GIRLFRIEND = 'ROLE_GIRLFRIEND';

export interface RoleContent {
    role: Roles
}

export const POEM_GAME = 'game1';
export const MODE_GAME = 'game2';
export interface NormalGameState {
    user: User,
    game: typeof POEM_GAME | typeof MODE_GAME,
    content: NormalGameContent
}

export interface NormalGameContent {
    state: number,
    timeStamp: number,
}

export const PERSISTENT_REPLY_GAME = 'PERSISTENT_REPLY_GAME';

export interface PersistentReplyGameState {
    user: User,
    game: typeof PERSISTENT_REPLY_GAME,
    content: PersistentReplyGameContent
}

export interface PersistentReplyGameContent {
    state: typeof UNTRIGGERED | typeof PLAYING | typeof PLAYED,
    totalCount: number,
    continuePlayingCount: number
}

export const LIFE_GAME = 'LIFE_GAME';

export type LifeStateContent = {
    state: string,
    call?: boolean,
    reply: number,
};

export interface LifeState {
    user: User,
    game: typeof LIFE_GAME,
    content: LifeStateContent,
}

export interface QuickReply {
    cmd: string,
}

export type MsgPlatform =
    typeof FB |
    typeof LINE;

export const FB = 'FB';
export const LINE = 'LINE';

export type User = FbUser | LineUser;

export interface FbUser {
    platform: typeof FB,
    id: string,
}

export interface LineUser {
    platform: typeof LINE,
    id: string,
    //replyToken: string
}

export const STRATEGY_BASIC_CMD = 'STRATEGY_BASIC_CMD';
export const STRATEGY_IMITATE_TYPING = 'STRATEGY_IMITATE_TYPING';

export type SummaryModel = {
    rangeKey: string,
    hashKey: string,
    event?: string,
    executeTo?: number,
    start?: number,
    end?: number,
    switchTurnThreshold?: number,
    sessionThreshold?: number,
} | RedirectUrlModel;

export type RedirectUrlModel = {
    rangeKey: string,
    hashKey: string,
    count: number,
    urlId: string,
}

export type UserCoupon = {
    userId: string,
    event: string,
    serial: string,
    token: string,
    isUsed: boolean,
    createdAt: number,
    updatedAt: number
}

export type CouponContent = {
    event: string,
    serial: string,
    token: string,
    isUsed: boolean,
    createdAt: number,
    updatedAt: number
}

export type MessageModel = {
    chatId: string,
    time: number,
    senderId: 'BOT' | string,
    recipientId: 'BOT' | string,
    type: 'TEXT' | 'POSTBACK' | 'ATTACHMENT' | 'REFERRAL' | string,
    data: string,
    platform?: 'FB' | 'LINE',
    text?: string,
    inMsg?: string,
    outMsg?: string,
    payload?: string,
    postback?: any,
    referral?: {
        ad_id?: string,
        ref?: string,
        source: string,
        type: string,
    },
}

export type SessionModel = {
    chatId: string,
    start: number,
    end: number,
    switchTurnCount: number,
    lastMessageAt: number,
}

export type StateModel = {
    userId:string,
    key:string,
    data: StateData,
};

export type Interval = {
    index: number,
    start: number,
    end: number,
}

export type StateData = CouponContent | NormalGameContent | LifeStateContent | PersistentReplyGameContent | RoleContent;
