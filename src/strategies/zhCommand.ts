import queryString from 'query-string';
import { genNews } from './zh/news';
import { StrategyForCmd, finishStrategyForCmd } from '../replier';
import { CmdInMsg, TextOutMsg, NewsOutMsg, TextInMsg, TEXT_IN, QuickReply, MsgPlatform, FB, STRATEGY_BASIC_CMD, PersistentReplyGameContent, PLAYING, OMsg, TextOutContent, ImgOutMsg, ImgOutContent, AVATAR_ROLE, ROLE_LIBGIRL, ROLE_GIRLFRIEND } from './definitions';
import { CMD_LIFE_AID, CMD_END_AID, CMD_GET_STARTED, defaultQuickReply, askingJoke, CMD_NEXT_JOKE, CMD_ENOUGH, askingNews, CMD_GEN_JOKE, CMD_GOTO_TOP, CMD_SHARE_QR, CMD_SHARE_LINK, CMD_ELECTION_DATE, makeQuickReplies, TEXTS_GOTO_TOP, TEXT_ENOUGH, CMD_PLAY_GAME, CMD_REQUEST_POEM, CMD_REQUEST_MODE, CMD_PLAY_PERSISTENT_REPLY, CMD_SUMMON_LIBGIRL, CMD_SUMMON_GIRLFRIEND } from './zh/cmdList';
import { timeStampNow } from '..//utils/tools';
import zhBlurt from './zh/blurt.json';
import randomItem from 'random-item';
import { sendGreet } from './zh';
import { IMG_AVATAR_QR_CODE } from './img_collection';
import moment from 'moment';
import {getLifeState, uploadLifeState } from './lifeGame';
import { getPersistentReplyState } from './zh/persistentReplyGame';
import { saveUserState } from '../utils/state';

export const electionDate = moment("2020-01-11 08:00:00");

function getShareLink(platForm: MsgPlatform) {
    return (platForm === FB) ? "https://m.me/libgirldotcom?ref=activeshare" :
        'https://line.me/R/ti/p/@libgirl_bot'
}

const STRATEGY_SAY_JOKE = 'STRATEGY_SAY_JOKE';
const STRATEGY_GET_NEWS = 'STRATEGY_GET_NEWS';
export function genCmdFollowUp(
    textReplier: (inMsg: TextInMsg, preOutMsgs: OMsg[], quickReplies: QuickReply[]) => Promise<string[]>
): StrategyForCmd {

    return async (
        inMsg: CmdInMsg,
        sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
        preOutMsgs: OMsg[],
        falseCallback?: StrategyForCmd
    ) => {
        console.log('cmdFollowup inMsg.content:');
        console.log(inMsg.content);
        const cmd = inMsg.content.cmd;
        const currentState = await getLifeState(inMsg);
        console.log(currentState)

        if (cmd === CMD_LIFE_AID) {
            console.log('Life Aids.');
            const outMsg = new TextOutMsg(
                inMsg.user,
                [STRATEGY_BASIC_CMD],
                timeStampNow(),
                new TextOutContent("找個安全地方休息一下，跟本萌聊聊嗎？", [], [])
            );
            uploadLifeState(inMsg,'ALERT')
            return sendAPI([outMsg as OMsg].concat(preOutMsgs));

        } else if  (cmd === CMD_END_AID && (currentState?.content?.state != 'off')) {
            console.log('END Life Aids.');
            const outMsg = new TextOutMsg(
                inMsg.user,
                [STRATEGY_BASIC_CMD],
                timeStampNow(),
                new TextOutContent(
                    "很高興聽到你沒事～\n如果你想找人說說話，本萌都在這。\n也可以輸入\"選單\"找萌太子玩喔！",
                    [],
                    defaultQuickReply
                )
            );
            uploadLifeState(inMsg,'off')
            return sendAPI([outMsg as OMsg].concat(preOutMsgs));

        } else if (cmd === CMD_GET_STARTED) {
            console.log('user from FB Ads.');
            const outMsg = new TextOutMsg(
                inMsg.user,
                [STRATEGY_BASIC_CMD],
                timeStampNow(),
                new TextOutContent(
                    "輸入任何訊息，或按下方按鈕，本 Libgirl 萌太子就會回覆你喔！",
                    [],
                    defaultQuickReply
                )
            );
            return sendAPI([outMsg as OMsg].concat(preOutMsgs));
            
        } else if (askingJoke.includes(cmd)) {
            const jokeString = randomItem(zhBlurt.map((pair: { key: string, value: string }) => pair.value));
            const jokeQueryString = queryString.stringify({ q: jokeString });
            const ref = [{
                originalText: '',
                person: '',
                url: `https://www.google.com/search?${jokeQueryString}`
            }];
            const quickReplies = (cmd === CMD_GEN_JOKE) ? makeQuickReplies([CMD_NEXT_JOKE, CMD_ENOUGH])
                : makeQuickReplies([CMD_GOTO_TOP]);
            const outMsg = new TextOutMsg(
                inMsg.user,
                [STRATEGY_SAY_JOKE],
                timeStampNow(),
                new TextOutContent(jokeString, ref, quickReplies)
            );
            return sendAPI([outMsg as OMsg].concat(preOutMsgs));
        } else if (askingNews.includes(inMsg.content.cmd)) {
            console.log('stretagy: news.');
            const outMsg = new NewsOutMsg(
                inMsg.user,
                [STRATEGY_GET_NEWS],
                timeStampNow(),
                genNews(inMsg.content.cmd)
            );
            return sendAPI([outMsg as OMsg].concat(preOutMsgs));
            
        } else if (cmd === CMD_GOTO_TOP) {
            console.log("返回主選單");
            return sendGreet(TEXTS_GOTO_TOP[0], inMsg.user, sendAPI, preOutMsgs, STRATEGY_BASIC_CMD);

        } else if (cmd === CMD_ENOUGH) {
            return textReplier(
                {
                    user: inMsg.user,
                    content: {
                        type: TEXT_IN,
                        text: TEXT_ENOUGH,
                        fbNlp: inMsg.content.fbNlp
                    },
                    timeStamp: timeStampNow()
                },
                preOutMsgs,
                makeQuickReplies([CMD_GOTO_TOP])
            );

        } else if (cmd === CMD_SHARE_QR) {
            console.log("分享萌卷醬 QR Code");
            return sendAPI([new ImgOutMsg(
                inMsg.user,
                [STRATEGY_BASIC_CMD],
                timeStampNow(),
                new ImgOutContent(IMG_AVATAR_QR_CODE, [])
            ) as OMsg].concat(preOutMsgs));
            
        } else if (cmd === CMD_SHARE_LINK) {
            console.log("分享連結 Get Link");
            return sendAPI([new TextOutMsg(
                inMsg.user,
                [STRATEGY_BASIC_CMD],
                timeStampNow(),
                new TextOutContent(getShareLink(inMsg.user.platform), [], [])
            ) as OMsg].concat(preOutMsgs));
            
        } else if (cmd === CMD_SUMMON_LIBGIRL) {
            saveUserState(inMsg.user.id, AVATAR_ROLE, {role: ROLE_LIBGIRL});
            return sendAPI([new TextOutMsg(
                inMsg.user,
                [STRATEGY_BASIC_CMD],
                timeStampNow(),
                new TextOutContent(`萌太子呼應你的召喚！`, [], [])
            ) as OMsg].concat(preOutMsgs));
        } else if (cmd === CMD_SUMMON_GIRLFRIEND) {
            saveUserState(inMsg.user.id, AVATAR_ROLE, {role: ROLE_GIRLFRIEND});
            return sendAPI([new TextOutMsg(
                inMsg.user,
                [STRATEGY_BASIC_CMD],
                timeStampNow(),
                new TextOutContent(`腦公我來了～（啾）`, [], [])
            ) as OMsg].concat(preOutMsgs));
        }

        return finishStrategyForCmd(inMsg, sendAPI, preOutMsgs, falseCallback)
    }
}

export const playGameCmd: StrategyForCmd = async (
    inMsg: CmdInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    falseCallback?: StrategyForCmd
) => {
    console.log('playGameCmd inMsg.content:');
    console.log(inMsg.content);
    const cmd = inMsg.content.cmd;

    if (cmd === CMD_PLAY_GAME) {
        console.log("plauGameCmd CMD_PLAY_GAME:");
        const gameState: PersistentReplyGameContent = await getPersistentReplyState(inMsg.user);
        const cmds = [CMD_REQUEST_POEM, CMD_REQUEST_MODE]
            .concat((gameState.state === PLAYING) ? [] : [CMD_PLAY_PERSISTENT_REPLY]);
        console.log(cmds);
        const quickReplies = makeQuickReplies(cmds);

        const outMsg = new TextOutMsg(
            inMsg.user,
            [STRATEGY_BASIC_CMD],
            timeStampNow(),
            new TextOutContent("嘻嘻！你想玩什麼遊戲呢？", [], quickReplies)
        );
        
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));
    }
    return finishStrategyForCmd(inMsg, sendAPI, preOutMsgs, falseCallback)
}
