import {
    InMsg,
    TextInMsg,
    OMsg,
    TextOutMsg,
    LifeOutMsg,
    LifeState,
    LifeCallMsg,
    LifeOutContent,
    LifeCallContent,
    TextOutContent,
    LIFE_GAME,
    LifeStateContent
} from './definitions';
import { getUserState, saveUserState } from '../utils/state';
import randomItem from 'random-item';
import { timeStampNow } from '../utils/tools';
import { defaultQuickReply, CMD_LIFE_AID, CMD_END_AID } from './zh/cmdList';
import { StrategyForText, TextStrategyOpt, finishStrategyForText } from '../replier';
import lifeReply from './zh/lifeReply.json'

const keyWords: string[] = ['自殺', '想死', '結束生命', '救命', '救我', '不想活', '難過', '難受', '心情', '跳樓', '割腕']
const finishword: string[] = ['謝謝', '好多了', '舒坦', '好受', '沒事了', '離開']
const callP: number = 0.1

export const STRATEGY_LIFE_AID = 'STRATEGY_LIFE_AID'
export const LIST_LIFE_NUMBER = [
    { 'name': '張老師專線 1980', 'number': '1980' },
    { 'name': '生命專線 1995', 'number': '1995' },
    { 'name': '安心專線 24H 1925', 'number': '1925' }
];

export async function getLifeState(inMsg: InMsg): Promise<LifeState> {
    const getState = await getUserState(inMsg.user.id, LIFE_GAME);
    if (!getState) {
        return {
            user: inMsg.user,
            game: "LIFE_GAME",
            content: {
                state: 'off',
                reply: 0,
            }
        };
    }
    return {
        user: inMsg.user,
        game: "LIFE_GAME",
        content: <LifeStateContent>getState,
    };
}

export async function uploadLifeState(inMsg: InMsg, state: string, reply: number = 0, call: boolean = false) {
    const updateGameState: LifeState = {
        user: inMsg.user,
        game: "LIFE_GAME",
        content: {
            state: state,
            call: call,
            reply: reply,
        }
    };
    console.log(updateGameState);
    saveUserState(updateGameState.user.id, updateGameState.game, updateGameState.content);
}

function getLifeString(currentState: LifeState) {
    let lifeString: string;
    if (currentState.content.reply) {
        const replyState: number = (currentState.content.reply) % 3;
        const replylist: string[] = lifeReply.map(listLifeString => listLifeString.string)[replyState];
        lifeString = randomItem(replylist);
    } else {
        const replylist: string[] = lifeReply.map(listLifeString => listLifeString.string)[0];
        lifeString = randomItem(replylist);
    }
    return lifeString;
}

function callProbability(reply: number = 0) {
    const probability: number = reply * 0.025 + callP;
    return probability;
}

export const lifeAid: StrategyForText = async (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => {
    const currentState = await getLifeState(inMsg);
    console.log('lifeAid.\n inMsg.content: ');
    console.log(inMsg.content);
    console.log('current State: ');
    console.log(currentState);
    const text = inMsg.content.text;
    var check: number = -1
    for (var key of finishword) {
        if (text.search(key) != -1) {
            check = text.search(key)
            break
        }
    }

    if ((check) != -1 && (currentState?.content?.state == 'ALERT')) {
        const lifeString = "不管甚麼都可以跟我說，本萌隨時在這"
        // write into api
        await uploadLifeState(inMsg, 'OBSERVED', currentState.content.reply)

        const outMsg = new LifeOutMsg(
            inMsg.user,
            [STRATEGY_LIFE_AID],
            timeStampNow(),
            new LifeOutContent(lifeString, [CMD_END_AID], [])
        )
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));

    } else if ((check != -1 && currentState?.content?.state == 'OBSERVED')) {
        const lifeString = "沒事就好～不過隨時歡迎回來找本萌玩喔！"
        // write into api
        await uploadLifeState(inMsg, 'off', currentState.content.reply)
        const outMsg = new TextOutMsg(
            inMsg.user,
            [STRATEGY_LIFE_AID],
            timeStampNow(),
            new TextOutContent(lifeString, [], defaultQuickReply)
        );
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));

    } else if ((currentState.game == "LIFE_GAME") && (currentState?.content?.state != 'off')) {
        const phoneCall: number = Math.random()
        const lifeString: string = getLifeString(currentState)
        if (phoneCall < callProbability(currentState.content.reply) && currentState?.content?.call == false) {
            await uploadLifeState(inMsg, 'ALERT', currentState.content.reply % 3, true)

            const outMsg = new LifeCallMsg(
                inMsg.user,
                [STRATEGY_LIFE_AID],
                timeStampNow(),
                new LifeCallContent('對了！別忘記有一群人也會幫助到你。\n他們人很好，也很願意傾聽。\n但想繼續跟本萌講話的話本萌永遠在這裡！',
                    LIST_LIFE_NUMBER,
                    [])
            );

            return sendAPI([outMsg as OMsg].concat(preOutMsgs));

        } else if (currentState?.content?.call == true) {
            await uploadLifeState(inMsg, 'ALERT', currentState.content.reply, false)
            const outMsg = new TextOutMsg(
                inMsg.user,
                [STRATEGY_LIFE_AID],
                timeStampNow(),
                new TextOutContent('本萌一直都在喔！不過...可以提醒本萌剛剛講到哪了嗎？', [], [])
            );
            return sendAPI([outMsg as OMsg].concat(preOutMsgs));

        } else {
            await uploadLifeState(inMsg, 'ALERT', currentState.content.reply + 1)

            const outMsg = new TextOutMsg(inMsg.user,
                [STRATEGY_LIFE_AID],
                timeStampNow(),
                new TextOutContent(lifeString, [], []));
            return sendAPI([outMsg as OMsg].concat(preOutMsgs));
        }
    }
    return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt);
}

export const lifeAlert: StrategyForText = async (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => {
    const currentState = await getLifeState(inMsg);
    console.log('lifeAlert.\n inMsg.content: ');
    console.log(inMsg.content);
    console.log('current State: ');
    console.log(currentState);
    const text = inMsg.content.text;
    console.log(text);
    let check: number = -1;
    for (let key of keyWords) {
        const searched = text.search(key);
        if (searched !== -1) {
            check = searched;
            break
        }
    }
    ;

    if (check !== -1) {
        const lifeString = "你還好嗎？";
        await uploadLifeState(inMsg, 'ALERT');
        const outMsg = new LifeOutMsg(inMsg.user,
            [STRATEGY_LIFE_AID],
            timeStampNow(),
            new LifeOutContent(lifeString, [CMD_LIFE_AID, CMD_END_AID], []));
        return sendAPI([outMsg as OMsg].concat(preOutMsgs));

    }
    return finishStrategyForText(inMsg, sendAPI, preOutMsgs, opt);
}
