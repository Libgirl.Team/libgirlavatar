import { StrategyForNonText, NonTextStrategyOpt, StrategyForCmd, finishStrategyForCmd } from '../replier';
import { TextOutMsg, User, TextOutContent, OMsg, CmdInMsg } from './definitions';
import { timeStampNow } from '../utils/tools';
import { CMD_TEXT_ME_LATER } from './zh/cmdList';
import { savePushTask } from '../utils/push-queue';

export const STRATEGY_GIRLFRIEND_BLURT = 'STRATEGY_GIRLFRIEND_BLURT';
export const zhGirlfriendBlurt: StrategyForNonText = (
    user: User,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: NonTextStrategyOpt
) => {
    const outMsg = new TextOutMsg(
        user,
        [STRATEGY_GIRLFRIEND_BLURT],
        timeStampNow(),
        new TextOutContent('愛你呦～', [], [])
    );
    return sendAPI([outMsg as OMsg].concat(preOutMsgs));
}

const STRATEGY_TEXT_ME_LATER = 'STRATEGY_TEXT_ME_LATER';
export const textMeLater: StrategyForCmd = (
    inMsg: CmdInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    falseCallback?: StrategyForCmd
) => {
    if (inMsg.content.cmd === CMD_TEXT_ME_LATER) {
        savePushTask([new TextOutMsg(
            inMsg.user,
            [STRATEGY_TEXT_ME_LATER],
            timeStampNow(),
            new TextOutContent(`傳訊息給逆～`, [], [])
        ) as OMsg]);
        
        return sendAPI([new TextOutMsg(
            inMsg.user,
            [STRATEGY_TEXT_ME_LATER],
            timeStampNow(),
            new TextOutContent(`等等傳訊息給泥～`, [], [])
        ) as OMsg].concat(preOutMsgs));
    }
    return finishStrategyForCmd(inMsg, sendAPI, preOutMsgs, falseCallback)
}
