import { GoogleNlpAnalysis } from '../utils/google';
import * as aswlist from './en_answer';
import request from 'request-promise-native';

const activeListeningTextCountThreshold = 30;
const yesNoQuestionEnglishBeginner: string[] = ["is", "are", "was", "were", "did", "do", "does", "am", "can", "could", "may", "should", "shall"];
const negateLemmas: string[] = ["n't", "n’t"];
const whQuestionEnglishBeginner: string[] = ["what", "when", "why", "which", "who", "how"];
const questionMark: string[] = ["?", "？"];
const greetingsThreshold = 0.6;

// current chatbot(Dec 3 2019) use no en strategies, so comment the code temparorily
// to avoid warnings on the deprecated type signatures.

// const greetings: Strategy = {
//     matchCondition: function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): boolean {
//         return ((message.nlp.entities.greetings && message.nlp.entities.greetings[0].confidence > greetingsThreshold) ||
//             (message.nlp.entities.bye && message.nlp.entities.bye[0].confidence > greetingsThreshold) ||
//             (message.nlp.entities.thanks && message.nlp.entities.thanks[0].confidence > greetingsThreshold));
//     },
//     getReplyMessage: async function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): Promise<string> {

//         if (message.nlp.entities.greetings) {
//             var greeting = message.nlp.entities.greetings[0].confidence;
//         } else {
//             var greeting: any = 0;
//         }
//         if (message.nlp.entities.bye) {
//             var bye = message.nlp.entities.bye[0].confidence;
//         } else {
//             var bye: any = 0;
//         }
//         if (message.nlp.entities.thanks) {
//             var thanks = message.nlp.entities.thanks[0].confidence;
//         } else {
//             var thanks: any = 0;
//         }

//         if (greeting > bye) {
//             if (greeting > thanks) {
//                 return aswlist.greeting[Math.floor(Math.random() * aswlist.greeting.length)]
//             } else {
//                 return aswlist.thanks[Math.floor(Math.random() * aswlist.thanks.length)]
//             }
//         } else if (bye > thanks) {
//             return aswlist.bye[Math.floor(Math.random() * aswlist.bye.length)]
//         } else {
//             return aswlist.thanks[Math.floor(Math.random() * aswlist.thanks.length)]
//         }
//     }

// }

// const activeListening: Strategy = {
//     matchCondition: function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): boolean {
//         return Math.abs(googleNlpAnalysis.sentiment.documentSentiment.score) > 0.2 &&
//             message.text.length > activeListeningTextCountThreshold;

//     },
//     getReplyMessage: async function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): Promise<string> {
//         return aswlist.opQ[Math.floor(Math.random() * aswlist.opQ.length)];
//     }

// }

// const questionAnswering: Strategy = {
//     matchCondition: function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): boolean {
//         if (googleNlpAnalysis.syntax.tokens instanceof Array) {
//             let messageTokens: any[] = googleNlpAnalysis.syntax.tokens;
//             let firstToken = messageTokens[0].text.content.toLowerCase();
//             let secondToken;
//             let lastToken = messageTokens[messageTokens.length - 1].text.content.toLowerCase();

//             if (messageTokens[1]) {
//                 secondToken = messageTokens[1].text.content.toLowerCase();
//             }
//             return yesNoQuestionEnglishBeginner.includes(firstToken) ||
//                 negateLemmas.includes(secondToken) ||
//                 whQuestionEnglishBeginner.includes(firstToken) ||
//                 questionMark.includes(lastToken)
//         } else {
//             return false;
//         }
//     },
//     getReplyMessage: async function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): Promise<string> {
//         var entities: string = "";
//         for (var entity of googleNlpAnalysis.entities.entities) {
//             if (entity.metadata.wikipedia_url) {
//                 entities = entities + '\n' + entity.metadata.wikipedia_url;
//             }
//         }
//         if (entities.length) {
//             return aswlist.QA[0] + entities;
//         } else {
//             return aswlist.QA[1] + entities;
//         }
//     }

// }

// const askingOpenEndedQuestions: Strategy = {
//     matchCondition: function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): boolean {
//         return Math.abs(googleNlpAnalysis.sentiment.documentSentiment.score) > 0.3;
//     },
//     getReplyMessage: async function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): Promise<string> {
//         return aswlist.opQ[Math.floor(Math.random() * aswlist.opQ.length)];
//     }

// }

// const givingDescriptionAboutEntities: Strategy = {
//     matchCondition: function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): boolean {
//         return googleNlpAnalysis.entities.entities.length > 0;
//     },
//     getReplyMessage: async function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): Promise<string> {

//         async function CheckDictionary() {
//             var discribe;
//             try {
//                 let dictBody: any = await request('https://mydictionaryapi.appspot.com/?define=' + googleNlpAnalysis.entities.entities[0].name);
//                 //      (error: any, response: any, body: any) => {
//                 //     if (error) reject(error);
//                 //     if (response.statusCode != 200) {
//                 //         reject('Invalid status code <' + response.statusCode + '>');
//                 //     }
//                 //     resolve(body);
//                 // });
//                 console.log('SHOULD WORK:');
//                 let dict = JSON.parse(dictBody);
//                 console.log(dict);
//                 console.log(dict[0]["meaning"]);
//                 for (let i of Object.keys(dict[0]["meaning"])) {
//                     var j = dict[0]["meaning"][i][0].definition;
//                     discribe = j
//                     break;
//                 }
//                 console.log("********" + discribe + "********");
//             }
//             catch (err) {
//                 console.log('Check failed', err);
//             }
//             return discribe;
//         }
//         var discribe = await CheckDictionary();
//         console.log(discribe + "--------------------------");
//         return typeof discribe === "string" ?
//             aswlist.givenD[0] + discribe :
//             aswlist.givenD[1];
//     }

// }

// const blurting: Strategy = {
//     matchCondition: function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): boolean {
//         return true;
//     },
//     getReplyMessage: async function (message: any, googleNlpAnalysis: GoogleNlpAnalysis): Promise<string> {
//         return "blurting";
//     }

// }

// export const enStrategies = [greetings, activeListening, questionAnswering, askingOpenEndedQuestions, givingDescriptionAboutEntities, blurting];

