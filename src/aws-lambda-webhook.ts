const awsServerlessExpress = require('aws-serverless-express')
import { server as expressServer } from './server';
const server = awsServerlessExpress.createServer(expressServer)
exports.handler = (event: any, context: any) => { awsServerlessExpress.proxy(server, event, context) }
console.log('end of aws-lambea-webhook.');
