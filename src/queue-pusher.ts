import { getTableName } from './utils/models';
import AWS from "aws-sdk";
import { LineClient } from 'messaging-api-line';
import { OMsg, LifeOutMsg, LifeOutContent, LIFE_OUT, TextOutMsg, TextOutContent, TEXT_OUT, LINE, LIFE_CALL, IMG_OUT, NEWS_OUT, LifeCallMsg, LifeCallContent, NewsOutMsg, NewsOutContent, ImgOutMsg, ImgOutContent, COUPON_OUT, CouponOutMsg, CouponOutContent, RECMD_OUT, RecmdOutMsg, RecmdOutContent } from './strategies/definitions';
import { timeStampNow } from './utils/tools';
import { outParser as lineOutParser } from './utils/line/line_out_parser';
import { saveLineReply } from './utils/history';
require('dotenv').config();

export function pushQueue() {

    console.log('quene-pusher start.');
    AWS.config.update({ region: 'us-east-1' });
    const docClient = new AWS.DynamoDB.DocumentClient();

    const tableName = getTableName('PushQueue');

    const params = {
        TableName: tableName,
        ProjectionExpression: "chatID, platform, messages, rangeKey",
        Limit: 1400,
    };

    const lineAccessToken = process.env.LINE_ACCESS_TOKEN;
    const lineChannelSecret = process.env.LINE_CHANNEL_SECRET;
    if (lineAccessToken && lineChannelSecret) {
        console.log('get line env.');
        const lineClient = LineClient.connect(lineAccessToken, lineChannelSecret);

        const onScan = (err: any, data: AWS.DynamoDB.DocumentClient.ScanOutput) => {
            if (err) {
                console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                console.log('scan data:');
                console.log(data);
                const items = data.Items;
                
                if (items) {
                    const tasks: { platform: string, chatID: string, rangeKey: string, msgs: OMsg[]}[] = items.map((item: any) => {
                        return {
                            platform: item.platform,
                            chatID: item.chatID,
                            rangeKey: item.rangeKey,
                            msgs: JSON.parse(item.messages).map((raw: any) => {
                                console.log(raw);
                                return reGenOutMsg(raw);
                            }).filter((msg: any) => msg)
                        }
                    });
                    console.log(tasks);
                    tasks.forEach((task) => {
                        
                        const msg0 = task.msgs[0];
                        if (msg0) {
                            if (task.platform === LINE) {
                                try {
                                    lineClient.push(msg0.user.id, task.msgs.reduce((acc: any[], msg) => {
                                        const bodies = msg.outBodies(lineOutParser);
                                        saveLineReply(bodies, msg);
                                        return acc.concat(bodies);
                                    }, []));
                                    const deleteParam = {
                                        TableName: tableName,
                                        Key: {
                                            chatID: task.chatID,
                                            rangeKey: task.rangeKey
                                        }
                                    }

                                    docClient.delete(deleteParam, function(err: any, data: any) {
                                        if (err) {
                                            console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
                                        } else {
                                            console.log("DeleteItem succeeded:", JSON.stringify(data, null, 2));
                                        }
                                    });
                                } catch (e) {
                                    console.error(e);
                                }
                            }
                        }
                    });
                } else {
                    console.error('items not exist!');
                }
            }
        }
        docClient.scan(params, onScan);
    } else {
        console.error('cannot get line env variable.');
    }
}

// deserialized OutMsg from the task queue.
function reGenOutMsg(raw: any): OMsg | null {
    if (raw.user && raw.strategy && raw.content) {
        switch (raw.content.type) {
            case LIFE_OUT:
                return new LifeOutMsg(raw.user, raw.strategy, timeStampNow(),
                                      new LifeOutContent(raw.content.text,
                                                         raw.content.cmds,
                                                         raw.content.quickReplies));
            case TEXT_OUT:
                return new TextOutMsg(raw.user, raw.strategy, timeStampNow(),
                                      new TextOutContent(raw.content.text,
                                                         raw.content.ref,
                                                         raw.content.quickReplies));
            case LIFE_CALL:
                return new LifeCallMsg(raw.user, raw.strategy, timeStampNow(),
                                       new LifeCallContent(raw.content.text,
                                                           raw.content.phones,
                                                           raw.content.quickReplies));
            case NEWS_OUT:
                return new NewsOutMsg(raw.user, raw.strategy, timeStampNow(),
                                      new NewsOutContent(raw.content.title,
                                                         raw.content.url,
                                                         raw.content.newsMedia,
                                                         raw.content.quickReplies));
            case IMG_OUT:
                return new ImgOutMsg(raw.user, raw.strategy, timeStampNow(),
                                     new ImgOutContent(raw.content.label,
                                                       raw.content.quickReplies));
            case COUPON_OUT:
                return new CouponOutMsg(raw.user, raw.strategy, timeStampNow(),
                                        new CouponOutContent(raw.content.title,
                                                             raw.content.url,
                                                             raw.content.img_url,
                                                             raw.content.action,
                                                             raw.content.subtitle,
                                                             raw.content.quickReplies));
            case RECMD_OUT:
                return new RecmdOutMsg(raw.user, raw.strategy, timeStampNow(),
                                       new RecmdOutContent(raw.content.title,
                                                           raw.content.url,
                                                           raw.content.img_url,
                                                           raw.content.action,
                                                           raw.content.subtitle,
                                                           raw.content.quickReplies));
        }
    }
    return null;
}
