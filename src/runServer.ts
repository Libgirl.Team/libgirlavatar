import { preparedApp, server } from './server';
require('dotenv').config();

const port = Number(process.env.PORT) || 5000;

preparedApp.then(() => {
    server.listen(port, (err: any) => {
        if (err) throw err;
        console.log(`> Ready on http://localhost:${port}`);
    });
})


