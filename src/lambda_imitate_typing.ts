import { handleMarkSeen  } from './requestHandler';

// FB Page 的 Access Token 存放在 Environment Variable 裡面。
exports.imitateTyping = function (event: any) {
    console.log("ENVIRONMENT VARIABLES\n" + JSON.stringify(process.env, null, 2))
    console.log("EVENT\n" + JSON.stringify(event, null, 2))
    // 首先擷取輸入訊息判斷：目前只處理從粉絲頁傳過來的訊息。
    let body = event.Records[0].Sns.Message;
    if (body) {
        body = JSON.parse(body);
        console.log("BODY\n" + JSON.stringify(body, null, 2));
        handleMarkSeen(body);
    }
}
