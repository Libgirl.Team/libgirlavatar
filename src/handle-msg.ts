import { InMsg, TEXT_IN, CMD_IN } from './strategies/definitions';
import { Replier } from './replier';

export async function handleMessage (
    inMsg: InMsg,
    replier: Replier
) {
    console.log('handleMessage inMsg: ======================')
    console.log(inMsg);
    const content = inMsg.content;

    let handleMsgPromise: Promise<string[]>;
    if (content.type === TEXT_IN) {
        handleMsgPromise = replier.forText({ ...inMsg, content }, []);
    } else if (content.type === CMD_IN) {
        handleMsgPromise = replier.forCmd({ ...inMsg, content }, []);
    } else {
        console.log('handleMessage else route.')
        handleMsgPromise = replier.forNonText(inMsg.user, []);
    }
    return handleMsgPromise;
}
