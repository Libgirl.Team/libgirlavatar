import CouponRepository from "./repositories/coupon-repository";
import couponEvents from './strategies/zh/coupon.json';
import { isStaff } from "./utils/history";
import { isEarly, isLate } from "./utils/tools";

const headers = {
    "Access-Control-Allow-Origin": "*"
};

function response(statusCode: number, message: string, data: any = null) {
    return {
        headers,
        statusCode: statusCode,
        body: JSON.stringify({ message, data })
    }
}

/**
 * Sensitive data should not show in response
 */
function filterSensitive(couponEvent: any) {
    return { ...couponEvent, code: undefined };
}

exports.handler = async (sourceEvent: any) => {
    const body = JSON.parse(sourceEvent.body);
    console.log(body);
    const { userId, event, serial, token, password, action } = body;

    if (!userId || !event || !serial || !token) {
        console.log('Validation error');
        return response(422, '出錯囉');
    }

    const repo = new CouponRepository();

    const coupon = await repo.find(userId, event, serial);
    console.log(coupon);

    if (!coupon) {
        console.log('Can not find coupon');
        return response(404, '沒有這個 Coupon 唷！');
    }
    if (token !== coupon.token) {
        console.log('Token invalid');
        return response(403, '你是不是想做壞壞的事！');
    }

    const couponEvent = couponEvents.find(e => e.event === coupon.event);
    if (!couponEvent) {
        console.log('Undefined coupon event');
        return response(500, '歐歐，出錯了');
    }

    if (couponEvent.start && isEarly(couponEvent.start)) {
        console.log('Too early to redeem');
        return response(403, '兌換時間還沒開始唷！');
    }
    if (couponEvent.end && isLate(couponEvent.end)) {
        console.log('Too late to redeem');
        return response(403, '兌換時間已經結束了唷！');
    }
    if (coupon.isUsed) {
        console.log('Coupon is used');
        return response(403, '此 Coupon 已用過囉！');
    }

    if (action === 'REDEEM') {
        if (password !== couponEvent.code) {
            console.log('Password Error');
            return response(401, '認證碼錯誤唷！');
        }

        console.log('Redeem success');
        if (!isStaff(userId)) {
            await repo.increment(event);
        }
        await repo.use(userId, event, serial);
        return response(200, "兌換成功！");
    }

    console.log('Can redeem');
    return response(200, '可換', filterSensitive(couponEvent));
};
