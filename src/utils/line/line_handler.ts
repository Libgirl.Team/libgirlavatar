import { InMsg, TEXT_IN, OTHER_IN, OMsg, LINE, LineUser, InContent, CMD_IN, QuickReply } from '../../strategies/definitions';
import { saveLineReply } from '../history';
import { outParser } from './line_out_parser';
import { CMD_GET_STARTED } from '../../strategies/zh/cmdList';
import { LineEvent, LineContext } from 'bottender';

export async function postAPI(pairs: {outMsg: OMsg, body: any}[], context: LineContext): Promise<string> {
    console.log('line_handler postAPI. {outMsg, body}:');
    console.log(pairs);
    pairs.forEach((pair => saveLineReply(pair.body, pair.outMsg)));
    const replyMsgs = pairs.reduce((acc: any[], pair) => {
        if (pair.body instanceof Array) {
            return acc.concat(pair.body);
        } else {
            return acc.concat([pair.body]);
        }
    }, []);
    const requestCall = new Promise<string>(resolve => {
        context.reply(replyMsgs);
        resolve('line postAPI done.');
    });
    return requestCall;
}

export function parseIn(evt: LineEvent): InMsg | null {
    console.log(evt);
    if (evt.source.type === 'user') {
        
        const id = evt.source.userId;
        const inMsg = (content: InContent): InMsg => {
            const user: LineUser = {
                platform: LINE,
                id,
            };
            return {
                user,
                timeStamp: evt.rawEvent.timestamp,
                content,
            }
        }
        
        let content: InContent;
        if (evt.message) {
            if (evt.text) {
                content = {
                    type: TEXT_IN,
                    text: evt.text,
                };
            } else {
                content = { type: OTHER_IN, body: evt };
            }
            return inMsg(content);
        } else if (evt.postback) {
            content = {
                type: CMD_IN,
                cmd: evt.postback.data,
            };
            return inMsg(content);
        } else if (evt.isFollow) {
            content = {
                type: CMD_IN,
                cmd: CMD_GET_STARTED
            };
            return inMsg(content);
        } 
    }
    return null;
}

export async function sendAPI(outMsgs: OMsg[], context: LineContext): Promise<string[]> {
    console.log('line_handler sendAPI init.');
    console.log(outMsgs);

    const sumQlOutMsgs = outMsgs.reduce((acc: {outMsgs: OMsg[], ql: QuickReply[]}, x) => {
        const newQl = x.content.getQuickReplies().concat(acc.ql);
        return { outMsgs: acc.outMsgs.concat([x.copyForQuickReplies(newQl)]), ql: newQl};
    }, { outMsgs: [], ql: []});

    return Promise.all([
        postAPI(sumQlOutMsgs.outMsgs.reduce((acc: {outMsg: OMsg, body: any}[], outMsg) => {
            const outBodies = outMsg.outBodies(outParser);
            console.log('line_handler outBodies:');
            console.log(outBodies);
            if (outBodies) {
                return acc.concat(outBodies.map(body => { return { outMsg, body }}));
            } else {
                return acc;
            }
        }, []), context)
    ])
}
