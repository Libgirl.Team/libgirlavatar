import { TextMessage, TemplateMessage, ImageMessage } from '@line/bot-sdk';
import { QuickReply, getNewsMediaPreviewUrl, getZhNewsMediaName, LifeOutMsg, LifeCallMsg, OutParser, TextOutMsg, NewsOutMsg, CouponOutMsg, ImgOutMsg, ImitateTypingOutMsg, RecmdOutMsg } from '../../strategies/definitions';
import { cmdToText, cmdToTexts } from '../../strategies/zh/cmdList';
import { lineImgUrl, IMG_COUPON } from '../../strategies/img_collection';

export const outParser: OutParser = {
    text: (msg: TextOutMsg) => {
        let replyText: TextMessage = {
            type: 'text',
            text: msg.content.text,
        };
        const qlItems = genQuickReplies(msg.content.quickReplies);
        console.log('line textBody:');
        if (qlItems.length > 0) {
            replyText = { ...replyText, quickReply: { items: qlItems } }
        }
        console.log(replyText);
        return [replyText];
    },

    lifeCall: (msg: LifeCallMsg): TextMessage[] => {
        return [{ type: 'text', text: msg.content.text } as TextMessage].concat(
            msg.content.phones.map(phn => {
                return { type: 'text', text: phn.name + ' 撥打 ' + phn.number };
            })
        )
    },

    lifeOut: (msg: LifeOutMsg): TemplateMessage[] => {
        let tmpMsg: any = {
            type: 'template',
            altText: msg.content.text,
            template: {
                type: 'buttons',
                text: msg.content.text,
                actions: msg.content.cmds.map(cmd => cmdToTexts(cmd).map(txt => {
                    return {
                        type: 'postback',
                        label: txt,
                        data: cmd
                    }
                })).reduce((acc, btns) => acc.concat(btns), [])
            }
        };

        const qlItems = genQuickReplies(msg.content.quickReplies);

        if (qlItems.length > 0) {
            tmpMsg = { ...tmpMsg, quickReply: { items: qlItems } }
        }
        return [tmpMsg];
    },

    news: (msg: NewsOutMsg): TemplateMessage[] => {
        let tmpMsg: TemplateMessage = {
            type: 'template',
            //quickReply: { items: genQuickReplies(outContent.quickReplies) },
            altText: msg.content.title,
            template: {
                type: 'buttons',
                thumbnailImageUrl: getNewsMediaPreviewUrl(msg.content.newsMedia),
                title: msg.content.title,
                text: getZhNewsMediaName(msg.content.newsMedia),
                actions: [{
                    type: 'uri',
                    label: '點我看原文',
                    uri: msg.content.url
                }]
            }
        };

        const qlItems = genQuickReplies(msg.content.quickReplies);

        if (qlItems.length > 0) {
            tmpMsg = { ...tmpMsg, quickReply: { items: qlItems } }
        }
        return [tmpMsg];
    },

    coupon: (msg: CouponOutMsg): TemplateMessage[] => {
        let tmpMsg: TemplateMessage = {
            type: 'template',
            altText: msg.content.title,
            template: {
                type: 'buttons',
                thumbnailImageUrl: msg.content.img_url,
                title: msg.content.title,
                text: msg.content.subtitle,
                actions: [{
                    type: 'uri',
                    label: msg.content.action,
                    uri: msg.content.url,
                }]
            }
        };

        const qlItems = genQuickReplies(msg.content.quickReplies);
        if (qlItems.length > 0) {
            tmpMsg = { ...tmpMsg, quickReply: { items: qlItems } }
        }
        return [tmpMsg];
    },

    recmd: (msg: RecmdOutMsg): TemplateMessage[] => {
        let tmpMsg: TemplateMessage = {
            type: 'template',
            altText: msg.content.title,
            template: {
                type: 'buttons',
                thumbnailImageUrl: msg.content.img_url,
                title: msg.content.title,
                text: msg.content.subtitle,
                actions: [{
                    type: 'uri',
                    label: msg.content.action,
                    uri: msg.content.url,
                }]
            }
        };


        const qlItems = genQuickReplies(msg.content.quickReplies);

        if (qlItems.length > 0) {
            tmpMsg = { ...tmpMsg, quickReply: { items: qlItems } }
        }
        return [tmpMsg];
    },

    img: (msg: ImgOutMsg): ImageMessage[] => {
        return [{
            type: 'image',
            originalContentUrl: lineImgUrl(msg.content.label),
            previewImageUrl: lineImgUrl(msg.content.label)
        }]
    },

    typing: (_: ImitateTypingOutMsg) => null,
}

function genQuickReplies(quickReplies: QuickReply[]): any[] {
    return quickReplies.map(ql => {
        const qlText = cmdToText(ql.cmd);
        return {
            type: 'action',
            action: {
                type: "postback",
                label: qlText,
                data: ql.cmd,
                displayText: qlText,
            }
        }
    });
}
