import { StrategyForText, StrategyForCmd, StrategyForNonText, rndWrapForText, replyForCmd, replyForNonText, replyForText, Replier, TextStrategyOpt } from '../../replier';
import { activeListen, answerQuestion, askOpenQuestion, describeEntity, blurt, elasticSearch, skipError, genTakeAsCmd } from '../../strategies/zh';
import { CmdInMsg, TextInMsg, User, OutMsg, OMsg, QuickReply, OutContent, ROLE_GIRLFRIEND } from '../../strategies/definitions';
import * as fbHdl from './fb_handler';
import { genCmdFollowUp, playGameCmd } from '../../strategies/zhCommand';
import { makePoem, poemCommand, makePoemForNontext } from '../../strategies/poemGame';
import { modeChange, modeCommand } from '../../strategies/modeGame';
import { lifeAlert, lifeAid } from '../../strategies/lifeGame';
import { persistentReplyGameForText, persistentReplyGameForCmd, persistentReplyForNontext } from '../../strategies/zh/persistentReplyGame';
import { getRoleState } from '../../utils/state';
import { zhGirlfriendBlurt } from '../../strategies/zh_girlfriend';

const probabilityElasticSearch = 0.7;
const probabilityActiveListen = 0.3;
//const probabilityGreeting = 0.9;
const probabilityOpenQuestion = 0.7;
const probabilityDescribeEntity = 0.7;
const probabilityQA = 0.5;

// for testing conveniently.
// const probabilityGreeting = 1;
// const probabilityActiveListen = 1;
// const probabilityQA = 1;
// const probabilityOpenQuestion = 1;
// const probabilityDescribeEntity = 1;

export const strategiesForText: StrategyForText[] = [
    lifeAid,
    lifeAlert,
    persistentReplyGameForText,
    skipError,
    makePoem,
    modeChange,
    rndWrapForText(probabilityElasticSearch, elasticSearch),
    rndWrapForText(probabilityActiveListen, activeListen),
    rndWrapForText(probabilityOpenQuestion, askOpenQuestion),
    rndWrapForText(probabilityDescribeEntity, describeEntity),
    rndWrapForText(probabilityQA, answerQuestion),
];

export const girlfriendStrategiesForText: StrategyForText[] = [
    lifeAid,
    lifeAlert,
    skipError,
];

export const defauStrategyForText: StrategyForText = (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => blurt(inMsg.user, sendAPI, preOutMsgs, {quickReplies: opt?.quickReplies});

export const girlDefaultStrategyForText: StrategyForText = (
    inMsg: TextInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    opt?: TextStrategyOpt
) => zhGirlfriendBlurt(inMsg.user, sendAPI, preOutMsgs, {quickReplies: opt?.quickReplies});

export const defauStrategyForCmd: StrategyForCmd = async (
    inMsg: CmdInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    _?: StrategyForCmd
) => blurt(inMsg.user, sendAPI, preOutMsgs);

export const girlfriendDefauStrategyForCmd: StrategyForCmd = async (
    inMsg: CmdInMsg,
    sendAPI: (outMsgs: OMsg[]) => Promise<string[]>,
    preOutMsgs: OMsg[],
    _?: StrategyForCmd
) => zhGirlfriendBlurt(inMsg.user, sendAPI, preOutMsgs);

export const strategiesForNonText: StrategyForNonText[] = [
    persistentReplyForNontext,
    makePoemForNontext
];
export const defauStrategyForNonText: StrategyForNonText = blurt;

export function genReplier(context: any): Replier {

    const genReplierForText = (preStrategies: StrategyForText[]) => async (
        inMsg: TextInMsg,
        preOutMsgs: OMsg[],
        quickReplies?: QuickReply[]
    ) => {
        console.log('fb_replier forText.');
        const roleContent = await getRoleState(inMsg.user);
        
        if (roleContent.role === ROLE_GIRLFRIEND) {
            return replyForText(inMsg,
                                preStrategies.concat(girlfriendStrategiesForText),
                                girlDefaultStrategyForText,
                                (outMsg) => fbHdl.sendAPI(outMsg, context),
                                preOutMsgs,
                                quickReplies)
        } else {
            return replyForText(inMsg,
                                preStrategies.concat(strategiesForText),
                                defauStrategyForText,
                                (outMsg) => fbHdl.sendAPI(outMsg, context),
                                preOutMsgs,
                                quickReplies)
        }
    };

    const replierForTextAfterCmd = genReplierForText([]);

    const strategiesForCmd: StrategyForCmd[] = [
        persistentReplyGameForCmd,
        genCmdFollowUp(replierForTextAfterCmd),
        playGameCmd,
        poemCommand,
        modeCommand,
    ];

    const girlfriendStrategiesForCmd: StrategyForCmd[] = [
        genCmdFollowUp(replierForTextAfterCmd),
    ];

    const replierForCmd = async (
        inMsg: CmdInMsg,
        preOutMsgs: OMsg[],
    ) => {
        const roleContent = await getRoleState(inMsg.user);
        if (roleContent.role === ROLE_GIRLFRIEND) {
            return replyForCmd(inMsg,
                               girlfriendStrategiesForCmd,
                               girlfriendDefauStrategyForCmd,
                               (outMsg) => fbHdl.sendAPI(outMsg, context),
                               preOutMsgs);
        } else {
            return replyForCmd(inMsg,
                               strategiesForCmd,
                               defauStrategyForCmd,
                               (outMsg) => fbHdl.sendAPI(outMsg, context),
                               preOutMsgs);
        }
    };
    
    return {
        forText: genReplierForText([genTakeAsCmd(replierForCmd)]),
        forCmd: replierForCmd,
        
        forNonText: async (
            user: User,
            preOutMsgs: OMsg[],
        ) => {
            console.log('fb_replier forNonText.');
            const roleContent = await getRoleState(user);
            if (roleContent.role === ROLE_GIRLFRIEND) {
                return replyForNonText(user,
                                       [],
                                       zhGirlfriendBlurt,
                                       (outMsg) => fbHdl.sendAPI(outMsg, context),
                                       preOutMsgs)
            } else {
                return replyForNonText(user,
                                       strategiesForNonText,
                                       defauStrategyForNonText,
                                       (outMsg) => fbHdl.sendAPI(outMsg, context),
                                       preOutMsgs)
            }
        },
    }
}
