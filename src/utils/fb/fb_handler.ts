import { InMsg, OutMsg, FB, OTHER_IN, User, OtherIn, ImitateTypingOutMsg, CMD_IN, CmdIn, OtherInMsg, STRATEGY_IMITATE_TYPING, QuickReply, OutContent, ImitateTypingContent } from '../../strategies/definitions';
import { saveReply } from "../history";
import { textToCmd, CMD_GET_STARTED, CMD_SUMMON_GIRLFRIEND } from '../../strategies/zh/cmdList';
import { timeStampNow } from '../tools';
import { outParser } from './fb_out_msg_parser';
import * as inParser from './fb_in_parser';
import { MessengerEvent } from 'bottender';

export function isFbRequest(body: any): boolean {
    if (body.object === 'page') {
        console.log("It's a page event!\n");
        return true;
    } else {
        return false;
    }
}

export function parseIn(evt: MessengerEvent): InMsg | null {
    if (evt.rawEvent.sender?.id) {
        const user: User = {
            platform: FB,
            id: evt.rawEvent.sender.id,
        };
        const timeStamp = evt.rawEvent.timestamp ? evt.rawEvent.timestamp : timeStampNow();
        const otherContent: OtherIn = { type: OTHER_IN, body: evt };
        const otherInMsg: OtherInMsg = { user, timeStamp, content: otherContent };
        
        const genCmdIn = (cmdText: string | undefined) => {
            const foundCmd = textToCmd(cmdText);
            if (foundCmd) {
                const content: CmdIn = { type: CMD_IN, cmd: foundCmd };
                return { user, timeStamp, content };
            } else {
                return otherInMsg;
            }
        };
        
        if (evt.message) {
            if (evt.text) {
                const evtText = evt.text;
                return inParser.parseEvtText(user, timeStamp, evtText, evt);
            } else {
                return otherInMsg;
            }
            
        } else if (evt.postback) {
            console.log(evt.postback);
            if (evt.postback.payload === 'getStarted' &&
                evt.postback.referral?.ref === CMD_SUMMON_GIRLFRIEND) {
                const content: CmdIn = { type: CMD_IN, cmd: CMD_SUMMON_GIRLFRIEND };
                return { user, timeStamp, content };
            }
            return genCmdIn(evt.postback.payload);
        } else if (evt.referral) {
            return genCmdIn(evt.referral.ref);
        }
    }
    return null;
}

export async function sendAPI<T extends OutContent>(outMsgs: OutMsg<T>[], context: any): Promise<string[]> {
    const sumQl = outMsgs.reduce((acc: QuickReply[], outMsg) => {
        return acc.concat(outMsg.content.getQuickReplies());
    }, []);
    
    const sumQlOutMsgs = outMsgs.map(outMsg => {
        return outMsg.copyForQuickReplies(sumQl);
    })

    return sumQlOutMsgs.reduce(async (acc1: Promise<string[]>, outMsg) => {
        const outBodies = outMsg.outBodies(outParser);
        if (outBodies) {
            const promises: Promise<string[]> = outBodies
                .reduce(async (acc2: Promise<string[]>, body) => {
                    return body ? (await acc2).concat(await postAPI(outMsg, body, context)) : acc2;
                }, new Promise(resolve => resolve([])));
            return (await acc1).concat(await promises);
        } else {
            return await acc1;
        }
    }, new Promise<string[]>(_ => []))
}

export async function postAPI<T extends OutContent>(outMsg: OutMsg<T>, request_body: any, context: any): Promise<string> {
    console.log('fb_handler postAPI request_body. request_body:');
    console.log(request_body);
    saveReply(request_body, outMsg);
    const requestCall = new Promise<string>(resolve => {
        if (request_body.typingTime) {
            context.typing(request_body.typingTime)
        } else {
            context.sendMessage(request_body);
            resolve('fb postAPI done.');
        }
    });
    return requestCall;
}

// export async function imitateTyping(id: string, ms: number, context: any, strategy: string[]): Promise<string[]> {
//     //await delay(ms);
//     const outMsg = new ImitateTypingOutMsg({ platform: FB, id },
//                                            strategy,
//                                            timeStampNow(),
//                                            new ImitateTypingContent(ms));
//     const outBodies = outMsg.outBodies(outParser);
//     if (outBodies) {
//         return Promise.all(outBodies.map((body: any) => postAPI(outMsg, body, context)));
//     } else {
//         return new Promise<string[]>(_ => []);
//     }
// }

// const TYPEING_ON = "typing_on";
// const MARK_SEEN = "mark_seen";

// export async function imitateTyping(id: string, ms: number, context: any, strategy: string[]): Promise<string[]> {
//     //await delay(ms);
//     const typing = actionAPI(TYPEING_ON, id, context);
//     const markSeen = actionAPI(MARK_SEEN, id, context);
//     return Promise.all([typing, markSeen])
// }

// export async function actionAPI(action: string, sender_psid: string, context: any) {
//     console.log("sender activity event")

//     const outMsg: ImitateTypingOutMsg = {
//         user: { platform: FB, id: sender_psid },
//         content: { type: IMITATE_TYPING_OUT },
//         timeStamp: timeStampNow(),
//         strategy: [STRATEGY_IMITATE_TYPING]
//     };
    
//     // Construct the message body
//     let request_body = {
//         "recipient": {
//             "id": sender_psid
//         },
//         "sender_action": action
//     };
//     console.log(request_body);
//     // return sendAPI({ type: IMITATE_TYPING_OUT });
//     return postAPI(outMsg, request_body, context);
// }
