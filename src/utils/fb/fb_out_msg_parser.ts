import { getZhNewsMediaName, getNewsMediaPreviewUrl, QuickReply, CouponOutMsg, RecmdOutMsg, OutParser, NewsOutMsg, TextOutMsg, LifeOutMsg, LifeCallMsg, ImgOutMsg, ImitateTypingOutMsg } from '../../strategies/definitions';
import { fbImgUrl as imgUrl, IMG_AVATAR_QR_CODE, IMG_COUPON } from '../../strategies/img_collection';
import { cmdToText, cmdToTexts } from '../../strategies/zh/cmdList';

export interface FbQuickReply {
    content_type: string,
    title: string,
    payload: string,
}

function genFbQuickReplies(quickReplies: QuickReply[]): FbQuickReply[] {
    return quickReplies.map((ql) => {
        return {
            content_type: 'text',
            title: cmdToText(ql.cmd),
            payload: "<POSTBACK_PAYLOAD>"
        }
    });
}

function messageWithQuickReplies(message: any, quickReplies: QuickReply[]): any {
    if (quickReplies.length > 0) {
        return message = {
            ...message,
            quick_replies: genFbQuickReplies(quickReplies)
        };
    } else {
        return message;
    }
}

export const outParser: OutParser = {
    coupon: (msg: CouponOutMsg) => {
        let message: any = {
            attachment: {
                type: "template",
                payload: {
                    template_type: "generic",
                    elements: [{
                        title: msg.content.title,
                        subtitle: msg.content.subtitle,
                        image_url: msg.content.img_url,
                        buttons: [{
                            type: 'web_url',
                            title: msg.content.action,
                            url: msg.content.url,
                        }]
                    }]
                }
            }
        };
        return [messageWithQuickReplies(message, msg.content.quickReplies)];
    },
    recmd: (msg: RecmdOutMsg) => {
        let message: any = {
            attachment: {
                type: "template",
                payload: {
                    template_type: "generic",
                    elements: [{
                        title: msg.content.title,
                        subtitle: msg.content.subtitle,
                        image_url: msg.content.img_url,
                        buttons: [{
                            type: 'web_url',
                            title: msg.content.action,
                            url: msg.content.url,
                        }]
                    }]
                }
            }
        };
        return [messageWithQuickReplies(message, msg.content.quickReplies)];
    },

    news: (msg: NewsOutMsg) => {
        let message: any = {
            attachment: {
                type: "template",
                payload: {
                    template_type: "generic",
                    elements: [{
                        title: msg.content.title,
                        subtitle: getZhNewsMediaName(msg.content.newsMedia),
                        item_url: msg.content.url,
                        image_url: getNewsMediaPreviewUrl(msg.content.newsMedia)
                    }]
                }
            }
        };
        return [messageWithQuickReplies(message, msg.content.quickReplies)];
    },

    text: (msg: TextOutMsg) => {
        let message: any;
        if (msg.content.ref.length !== 0) {
            message = {
                attachment: {
                    type: 'template',
                    payload: {
                        template_type: 'button',
                        text: msg.content.text,
                        buttons: msg.content.ref.map((rf) => {
                            return {
                                type: 'web_url',
                                url: rf.url,
                                title: `典故在此`,
                            };
                        })
                    }
                }
            }
        } else {
            message = {
                text: msg.content.text,
            };
        }
        return [messageWithQuickReplies(message, msg.content.quickReplies)];
    },

    lifeOut: (msg: LifeOutMsg) => {
        const message = {
            attachment: {
                type: 'template',
                payload: {
                    template_type: 'button',
                    text: msg.content.text,
                    buttons: msg.content.cmds.map(cmd => cmdToTexts(cmd).map(text => {
                        return {
                            type: 'postback',
                            title: text,
                            payload: cmd,
                        }
                    })).reduce((acc, btns) => acc.concat(btns), [])
                }
            }
        }
        return [messageWithQuickReplies(message, msg.content.quickReplies)];
    },

    lifeCall: (msg: LifeCallMsg) => {
        const message = {
            attachment: {
                type: 'template',
                payload: {
                    template_type: 'button',
                    text: msg.content.text,
                    buttons: msg.content.phones.map((rf) => {
                        return {
                            type: 'phone_number',
                            title: rf.name,
                            payload: rf.number,
                        }
                    })
                }
            }
        };
        return [messageWithQuickReplies(message, msg.content.quickReplies)];
    },

    img: (msg: ImgOutMsg) => {
        const message = (msg.content.label === IMG_AVATAR_QR_CODE) ? {
            attachment: {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "萌卷醬 QR code",
                            "subtitle": "轉寄這個訊息，把萌卷醬分享給你的朋友吧！",
                            "image_url": imgUrl(msg.content.label),
                            "default_action": {
                                "type": "web_url",
                                "url": imgUrl(msg.content.label)
                            },
                            "buttons": [
                                {
                                    "type": 'web_url',
                                    "url": 'https://m.me/libgirldotcom',
                                    "title": '快來聊天'
                                }
                            ]
                        }
                    ]
                }
            }
        } : {
                "attachment": {
                    "type": "image",
                    "payload": {
                        "is_reusable": true,
                        "url": imgUrl(msg.content.label)
                    }
                }
            };
        return [messageWithQuickReplies(message, msg.content.quickReplies)];
    },

    typing: (msg: ImitateTypingOutMsg) => {
        return [{ typingTime: msg.content.milliSeconds }];
    }
}
