//import { textToCmd } from '../../strategies/zh/cmdList';
import { CmdInMsg, User, CMD_IN, TextInMsg, TextIn, TEXT_IN } from '../../strategies/definitions';

export function parseEvtText(user: User, timeStamp: number, evtText: string, messageEvent: any) {
    // const foundCmd = textToCmd(evtText);
    // if (foundCmd) {
    //     const inMsg: CmdInMsg = {
    //         user,
    //         timeStamp,
    //         content: {
    //             type: CMD_IN,
    //             cmd: foundCmd,
    //             fbNlp: messageEvent.message.nlp
    //         },
    //     };
    //     return inMsg;
    // } else {
    //     let inMsg: TextInMsg;
    //     let content: TextIn;
        
    //     if (messageEvent.message.nlp) {
    //         content = {
    //             type: TEXT_IN,
    //             text: messageEvent.message.text,
    //             fbNlp: messageEvent.message.nlp,
    //         };
            
    //     } else {
    //         content = {
    //             type: TEXT_IN,
    //             text: messageEvent.message.text,
    //         };
    //     };
        
    //     inMsg = { user, timeStamp, content };
    //     console.log('fb parse inMsg:');
    //     console.log(inMsg);
    //     return inMsg;
    // }

    let inMsg: TextInMsg;
    let content: TextIn;
    
    if (messageEvent.message.nlp) {
        content = {
            type: TEXT_IN,
            text: messageEvent.message.text,
            fbNlp: messageEvent.message.nlp,
        };
        
    } else {
        content = {
            type: TEXT_IN,
            text: messageEvent.message.text,
        };
    };
    
    inMsg = { user, timeStamp, content };
    console.log('fb parse inMsg:');
    console.log(inMsg);
    return inMsg;
}
