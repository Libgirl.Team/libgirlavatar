import { OMsg } from '../../strategies/definitions';
import { savePushTaskToDB } from "../../repositories/push-queue";
import { timeStampNow } from '../tools';
const randomToken = require('random-token');

export function savePushTask(outMsgs: OMsg[]) {
    const msg0 = outMsgs[0];
    if (msg0) {
        savePushTaskToDB({
            chatID: `${msg0.user.platform}:${msg0.user.id}`,
            rangeKey: `${timeStampNow()}:${randomToken(8)}`,
            platform: msg0.user.platform,
            messages: outMsgs
        })
    }
}
