import { getTableName } from "./models";
import AWS from "aws-sdk";
import { AVATAR_ROLE, ROLE_LIBGIRL, RoleContent, StateData, StateModel, User, } from '../strategies/definitions';

AWS.config.update({ region: 'us-east-1' });
const docClient = new AWS.DynamoDB.DocumentClient();

async function saveUserState(userId: string, key: string, data: any): Promise<{} | undefined> {
    try {
        return await docClient.put({
            TableName: getTableName('State'),
            Item: { userId, key, data }
        }).promise();
    } catch (e) {
        console.error(e);
    }
}

async function getUserState(userId: string, key: string): Promise<StateData | undefined> {
    try {
        const result: any = await docClient.get({
            TableName: getTableName('State'),
            Key: {
                userId: userId,
                key: key,
            },
        }).promise();
        console.log(JSON.stringify(result))
        return result.Item?.data;
    } catch (e) {
        console.error(e);
    }
}

/**
 * Dynamodb may not return all data if more than 1MB
 * if lastEvaluatedKey exists, recursive call query
 */
async function queryDB(userId: string, keyBeginsWith: string, lastEvaluatedKey?: any): Promise<StateModel[]> {
    const states = await docClient.query({
        TableName: getTableName('State'),
        KeyConditionExpression: 'userId = :userId and begins_with(#key, :key)',
        ExpressionAttributeNames: {
            "#key": "key"
        },
        ExpressionAttributeValues: {
            ':userId': userId,
            ':key': keyBeginsWith
        },
        ExclusiveStartKey: lastEvaluatedKey,
    }).promise();

    let result = states.Items || [];
    if (states.LastEvaluatedKey) {
        const moreStates = await queryDB(userId, keyBeginsWith, states.LastEvaluatedKey);
        result = [...result, ...moreStates];
    }
    return <[]>result;
}

async function listUserState(userId: string, keyBeginsWith: string): Promise<StateModel[] | undefined> {
    try {
        return await queryDB(userId, keyBeginsWith);
    } catch (e) {
        console.error(e);
    }
}

/**
 * Dynamodb may not return all data if more than 1MB
 * if lastEvaluatedKey exists, recursive call scan
 */
async function scanDB(keyBeginsWith: string, lastEvaluatedKey?: any): Promise<StateModel[]> {
    const states = await docClient.scan({
        TableName: getTableName('State'),
        FilterExpression: 'begins_with(#key, :key)',
        ExpressionAttributeNames: {
            "#key": "key"
        },
        ExpressionAttributeValues: {
            ':key': keyBeginsWith
        },
        ExclusiveStartKey: lastEvaluatedKey,
    }).promise();

    let result = states.Items || [];
    if (states.LastEvaluatedKey) {
        const moreStates = await scanDB(keyBeginsWith, states.LastEvaluatedKey);
        result = [...result, ...moreStates];
    }
    return <[]>result;
}

async function scanState(keyBeginsWith: string): Promise<StateModel[] | undefined> {
    try {
        return await scanDB(keyBeginsWith);
    } catch (e) {
        console.error(e);
    }
}

export async function getRoleState(user: User): Promise<RoleContent> {
    const gotState = await getUserState(user.id, AVATAR_ROLE);
    if (gotState) {
        return <RoleContent>gotState;
    } else {
        const newState: RoleContent = {
            role: ROLE_LIBGIRL
        };
        await saveUserState(user.id, AVATAR_ROLE, newState);
        return newState;
    }
}

export { getUserState, saveUserState, listUserState, scanState }
