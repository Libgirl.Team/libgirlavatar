import AWS from "aws-sdk";

export function delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export function getRandomInt(max: any, min: any) {
    return Math.floor(Math.random() * Math.floor(max - min) + Math.floor(min));
}

// on the array, await the items 1 by 1, until find 1 true.
export async function asyncFind(fns: Array<() => Promise<boolean>>): Promise<boolean> {
    // let fn;
    for (let fn of fns) {
        //console.log('asyncFind before pred await.');
        const pred = await fn();
        //console.log(`asyncFind pred: ${pred}`);
        if (pred) {
            //console.log('asyncFind true.');
            return true;
        }
    }
    //console.log('asyncFind false.');
    return false;
}

export function timeStampNow(): number {
    return (new Date()).getTime();
}

export function isProduction(): boolean {
    return process.env.NODE_ENV === 'production';
}

export function isEarly(time: string | number): boolean {
    return Date.now() < new Date(time).getTime();
}

export function isLate(time: string | number): boolean {
    return Date.now() > new Date(time).getTime();
}

/**
 * WARNING: Execution role should have S3 read permission
 * Generate url for private file in S3
 * @param bucket S3 bucket name
 * @param key File path key
 * @param expiration Expires in seconds, default 15 * 60 = 900
 */
export function generateS3Url(bucket: string, key: string, expiration: number = 900): string {
    const s3 = new AWS.S3();
    return s3.getSignedUrl('getObject', {
        Bucket: bucket,
        Key: key,
        Expires: expiration
    });
}
