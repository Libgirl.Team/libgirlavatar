import dynamoose from "dynamoose";

const createDynamooseInstance = () => {
    dynamoose.AWS.config.update({
        region: 'us-east-1',
    });

    dynamoose.setDefaults({
        create: false, // Create table in DB, if it does not exist
        waitForActive: false, // Wait for table to be created before trying to use it
    });
};

export { createDynamooseInstance }
