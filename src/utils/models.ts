import dynamoose from "dynamoose";
import { isProduction } from "./tools";
import { createDynamooseInstance } from "./db";

createDynamooseInstance();

function getTableName(modelName: string) {
    return isProduction() ? modelName : `Dev${modelName}`;
}

const getMessageModel = () => {
    return dynamoose.model(getTableName('Message'), new dynamoose.Schema({
        chatId: {
            type: String,
            hashKey: true,
        },
        time: {
            type: Number,
            rangeKey: true,
        },
        senderId: {
            type: String,
            required: true,
        },
        recipientId: {
            type: String,
            required: true,
        },
        platform: {
            type: String,
            required: true,
            index: {
                global: true,
                rangeKey: 'time',
                name: 'platform-time-index',
                project: true, // ProjectionType: ALL
            }
        },
        type: String,
        text: String,
        payload: String,
        postback: Object,
        referral: Object,
        data: String,
        inMsg: String,
    }, { saveUnknown: true }));
};

const getSessionModel = () => {
    return dynamoose.model(getTableName('Session'), new dynamoose.Schema({
        chatId: {
            type: String,
            hashKey: true,
        },
        start: {
            type: Number,
            rangeKey: true,
        },
        end: {
            type: Number,
            required: true,
        },
        SwitchTurnCount: Number,
        lastMessageAt: Number,
    }, { saveUnknown: true }));
};

const getSummaryModel = () => {
    return dynamoose.model(getTableName('Summary'), new dynamoose.Schema({
        hashKey: {
            type: String,
            hashKey: true,
        },
        rangeKey: {
            type: String,
            rangeKey: true,
        },
        data: Object,
        count: Number,
        event: String,
    }, { saveUnknown: true }));
};

function getPushTaskModel() {
    return dynamoose.model(getTableName('PushQueue'), new dynamoose.Schema({
        chatID: {
            type: String,
            hashKey: true,
        },
        rangeKey: {
            type: String,
            rangeKey: true,
        },
        platform: {
            type: String,
            required: true,
        },
        messages: {
            type: Object,
            required: true,
        }
    }))
}

export { getMessageModel, getSessionModel, getSummaryModel, getTableName, getPushTaskModel };
