import { Option, Some, None } from "@usefultools/monads";

let { Translate } = require('@google-cloud/translate').v2;
let language = require('@google-cloud/language');
const translationClient = new Translate();

export const TRANSLATE_LANG = {
    zhCN: 'zh-CN',
    zhTW: 'zh-TW',
    ja: 'ja',
}

export const NLP_LANG = {
    zhTrad: 'zh-hant',
    en: 'en',
}

export const zhFamily = [TRANSLATE_LANG.zhCN,
                         TRANSLATE_LANG.zhTW,
                         TRANSLATE_LANG.ja];

export const languageClient = new language.LanguageServiceClient();

export interface GoogleNlpAnalysis {
    sentiment: any,
    syntax: any,
    entities: any
};

export class LanguageDocument {
    readonly type: string = 'PLAIN_TEXT';
    constructor(public readonly content: any, public readonly language: string) {
    }
}

export async function newLanguagedocument(content: string) {    
    const detect = await detectLanguage(content);
    console.log(`DETECT: ${detect}`, detect);
    if (zhFamily.includes(detect.language)) {
        return new LanguageDocument(content, NLP_LANG.zhTrad);
    } else {
        return new LanguageDocument(content, NLP_LANG.en);
    }
}

async function detectLanguage(content: string) {
    const result = (await translationClient.detect(content))[0];
    console.log('detect result:');
    console.log(result);
    return result
}

export async function translateToEn(textInput: string): Promise<string> {
    // Run request
    const [response] = await translationClient.translate(textInput, 'en');
    // console.log(response);
    let translations = Array.isArray(response) ? response : [response];
    // in request content, we only send length 1 array. So, the translations array length is also 1 according to the google cloud translate documentation
    // console.log(translations);
    return translations[0];
}

export async function translateToZhTW(textInput: string): Promise<string> {
    // Run request
    const [response] = await translationClient.translate(textInput, TRANSLATE_LANG.zhTW);
    // console.log(response);
    let translations = Array.isArray(response) ? response : [response];
    // in request content, we only send length 1 array. So, the translations array length is also 1 according to the google cloud translate documentation
    // console.log(translations);
    return translations[0];
}

export interface NlpEntity {
    mentions: NlpEntityMention[],
    metadata: {
        mid: string,
        wikipedia_url: string,
    },
    name: string,
    type: string,
    salience: number,
    sentiment: any,
};

export function findMaxSalienceEntity(entities: NlpEntity[]): Option<NlpEntity> {
    if (entities.length > 0) {
        const maxSalienceEntity = entities.reduce(
            (entitiyCandidate, currentEntity) => {
                return entitiyCandidate.salience > currentEntity.salience ?
                    entitiyCandidate :
                    currentEntity;
            },
            entities[0]
        );
        return Some(maxSalienceEntity);
    } else {
        return None;
    }
}

interface NlpEntityMention {
    text: { content: string, beginOffset: number },
    type: string,
    sentiment: any,
}

export async function getGoogleAnalysis(text: string): Promise<GoogleNlpAnalysis> {
    console.log('getGoogleAnalysis init');
    // guide all English to Chinese
    const originalDocument = await newLanguagedocument(text);
    console.log('Original document:');

    const document = await (async () => {
        if (originalDocument.language == NLP_LANG.zhTrad) {
            return originalDocument;
        } else {
            return newLanguagedocument(await translateToZhTW(originalDocument.content));
        }
    })()
    console.log('DOCUMENT:');

    const [sentiment] = await (async () => {
        if (originalDocument.language === NLP_LANG.zhTrad) {
            // Google NLP API performed bad on sentiment task of Chinse. Translating to En first
            const asEnDocument = new LanguageDocument(await translateToEn(text), 'en');
            return await languageClient.analyzeSentiment({ document: asEnDocument });
        } else {
            return await languageClient.analyzeSentiment({ document: originalDocument });
        };
    })();
    const [syntax] = await languageClient.analyzeSyntax({ document });
    const [entities] = await languageClient.analyzeEntities({ document });

    let googleNlpAnalysis: GoogleNlpAnalysis = {
        sentiment: sentiment,
        syntax: syntax,
        entities: entities
    };
    console.log('GoogleNlpAnalysis Sentiment + Syntax + Entities:');
    console.log(googleNlpAnalysis);

    return googleNlpAnalysis;

}
