import RedirectUrlRepository from "../repositories/redirect-url-repository";
import { RedirectUrlModel, User } from "../strategies/definitions";
import Token from "./token";
import recmds from "../strategies/zh/recmd.json";
import { scanState } from "./state";

const repository = new RedirectUrlRepository();

const incrementUrlCount = async (id: any): Promise<RedirectUrlModel> => {
    return repository.increment(id);
};

const generateUrl = (user: User, recmdId: string): string => {
    return composeUrl(user, recmdId, generateToken(user.id, recmdId));
};

const composeUrl = (user: User, id: string, token: string): string => {
    return `${(process.env.REDIRECT_URL)}?user=${user.id}&id=${id}&token=${token}`;
};

const generateToken = (user: string, id: string): string => {
    return Token.generateNormalToken(JSON.stringify({ user, id }), 'F319F71617DBC')
};

const isValid = (user: string, id: string, token: string): boolean => {
    return token === generateToken(user, id);
};

const saveUserClick = (userId: string, id: string): Promise<{} | undefined> => {
    return repository.saveUserClick(userId, id);
};

const countUrlsClicks = async (): Promise<{ id: string, count: number }[]> => {
    const repo = new RedirectUrlRepository();
    const result = await Promise.all(recmds.map((e) => repo.countUrl(e.event)));
    return result.filter(e => e);
};

const allUrl = () => {
    return recmds;
};

/**
 * If url clicks wrong, recount from user state
 */
const recountUrlClicks = async (urlId: string): Promise<number | undefined> => {
    const result = await scanState(`URL-${urlId}#ACTION-CLICK`);
    return result?.length;
};

export { incrementUrlCount, generateUrl, isValid, saveUserClick, countUrlsClicks, allUrl, recountUrlClicks };
