import CouponRepository from "../repositories/coupon-repository";
import { User } from "../strategies/definitions";
import uuidv4 from "uuid/v4";
import _, { Dictionary } from "lodash";
import coupons from '../strategies/zh/coupon.json';
import Token from "./token";
import { isStaff } from "./history";

const repository = new CouponRepository();
const oneDay = 24 * 60 * 60 * 1000;

function generateUrl(user: User, couponEvent: string, serial: string, token: string): string {
    return `${(process.env.COUPON_URL)}?id=${user.id}&event=${couponEvent}&serial=${serial}&token=${token}`;
}

function generateToken(user: User, couponEvent: string, serial: any): string {
    return Token.generateNormalToken(JSON.stringify({ id: user.id, couponEvent, serial }), '2BB9177691929')
}

const createUserCoupon = async (user: User, couponEvent: string): Promise<string> => {
    const serial = uuidv4();
    const token = generateToken(user, couponEvent, serial);
    await repository.create(user.id, couponEvent, serial, token);
    return generateUrl(user, couponEvent, serial, token);
};

const alreadyGetCoupon = async (user: User, couponEvent: string): Promise<boolean> => {
    const list = await repository.list(user.id, couponEvent);
    if (list === undefined) {
        throw Error('Unable to get coupon list');
    }
    return list.find(coupon => coupon.createdAt > Date.now() - oneDay) !== undefined;
};

const countCoupon = async (event: string, start: number = 0, end: number = Date.now()): Promise<{ count: number | undefined, daily: Dictionary<number> }> => {
    const coupons = (await repository.all())?.filter(c =>
        c.isUsed &&
        !isStaff(c.userId) &&
        c.event === event &&
        c.updatedAt >= start &&
        c.updatedAt <= end);
    const result = _.groupBy(coupons, c => Math.floor((c.updatedAt - start) / oneDay));
    return {
        count: coupons?.length,
        daily: _.mapValues(result, 'length'),
    };
};

const countRedeemed = async (): Promise<{ event: string, count: number }[]> => {
    const result = await Promise.all(coupons.map((e: any) => repository.count(e.event)));
    return result.filter(e => e);
};

const listCouponEvent = () => {
    return coupons;
};

export { createUserCoupon, alreadyGetCoupon, countCoupon, listCouponEvent, countRedeemed };
