const cnchar = require('cnchar');
const poly = require('cnchar-poly');
const order = require('cnchar-order');
const trad = require('cnchar-trad');
cnchar.use(poly, order, trad);

export function simpleToTrad(s: string): string {
    const trad = cnchar.convert.simpleToTrad(s);
    console.log(`simple to trad: ${s} to ${trad}`);
    return cnchar.convert.simpleToTrad(s);
}
