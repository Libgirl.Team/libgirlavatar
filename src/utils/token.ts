import hmacSHA256 from 'crypto-js/hmac-sha256';

export default class Token {
    /**
     * WARNING: sha is not strong enough, for password please use bcrypt instead
     */
    static generateNormalToken(data: string, secret: string): string {
        return hmacSHA256(data, secret).toString();
    }
}
