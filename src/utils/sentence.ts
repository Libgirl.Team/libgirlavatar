export interface WeightedSentence {
    sentence: string,
    weight: number,
}

export function rndWeightedSentence(wss: WeightedSentence[]): string {
    const sum: number = wss.reduce((acc: number, ws) => (acc + ws.weight), 0);
    const selectorNum = Math.random() * sum;
    return wss.reduce(
        (acc: {accSum: number, sentence: string}, ws) => {
            const accLow = acc.accSum;
            const accHigh = acc.accSum + ws.weight;
            if (accLow <= selectorNum && selectorNum < accHigh) {
                return {accSum: accHigh, sentence: ws.sentence};
            } else {
                return {accSum: accHigh, sentence: acc.sentence};
            }
        },
        {accSum: 0, sentence: wss[0].sentence}
    ).sentence
}


