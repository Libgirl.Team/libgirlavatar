import { InMsg } from "../../../../strategies/definitions";
import MessageTransformer from "./message_transformer";

export class LineMessageTransformer extends MessageTransformer {
    constructor(event: any, inMsg?: InMsg) {
        super(event, inMsg);
    }

    protected getSenderId(): string {
        return this._event.source.userId;
    }

    protected getTime(): string {
        return this._event.timestamp;
    }

    protected getType(): string {
        return this._event.message?.type?.toUpperCase() || this._event.type?.toUpperCase() || 'UNDEFINED';
    }

    protected getText(): string | undefined {
        return this._event.message?.text;
    }

    protected getPlatform(): string {
        return 'LINE';
    }
}
