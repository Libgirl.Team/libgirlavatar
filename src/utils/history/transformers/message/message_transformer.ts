import ITransformer from "../i_transformer";
import { InMsg } from "../../../../strategies/definitions";

export default abstract class MessageTransformer implements ITransformer {
    protected readonly _event: any;
    private readonly _inMsg: InMsg | undefined;

    protected constructor(event: any, inMsg?: InMsg) {
        this._event = event;
        this._inMsg = inMsg;
    }

    toModel(): any {
        return {
            chatId: this.getSenderId(),
            senderId: this.getSenderId(),
            recipientId: 'BOT',
            time: this.getTime(),
            type: this.getType(),
            text: this.getText(),
            data: JSON.stringify(this._event),
            inMsg: JSON.stringify(this._inMsg),
            platform: this.getPlatform(),
        };
    };

    protected abstract getSenderId(): string;

    protected abstract getTime(): string;

    protected abstract getType(): string;

    protected abstract getText(): string | undefined;

    protected abstract getPlatform(): string;
}
