import { InMsg } from "../../../../strategies/definitions";
import MessageTransformer from "./message_transformer";

export default class FbMessageTransformer extends MessageTransformer {
    private readonly _entry: any;
    private readonly _message: any;

    constructor(entry: any, inMsg?: InMsg) {
        super(entry, inMsg);
        this._entry = entry;
        // modify for bottender pre-parsed msg event
        //this._message = this._entry.messaging[0];
        this._message = entry;
    }

    toModel(): any {
        return {
            ...super.toModel(),
            ...this.getPostbackFields(),
            referral: this.getReferralFields(),
        };
    };

    protected getSenderId(): string {
        return this._message.sender?.id;
    }

    protected getTime(): string {
        return this._message.timestamp;
    }

    protected getType() {
        if (this._message.message?.text) {
            return 'TEXT';
        }
        if (this._message.message?.attachments) {
            return 'ATTACHMENT';
        }
        if (this._message.postback) {
            return 'POSTBACK';
        }
        if (this._message.referral) {
            return 'REFERRAL';
        }
        return 'UNDEFINED';
    }

    protected getText(): string | undefined {
        return this._message.message?.text;
    }

    protected getPlatform(): string {
        return 'FB';
    }

    private getReferralFields(): string | undefined {
        return this._message.referral;
    }

    private getPostbackFields() {
        const postback = this._message.postback;
        if (!postback) {
            return {};
        }
        return { postback, payload: postback.payload };
    }
}
