import { OutMsg, OutContent } from "../../../../strategies/definitions";
import ReplyTransformer from "./reply_transformer";

export default class LineReplyTransformer<T extends OutContent> extends ReplyTransformer<T> {
    constructor(response: any, outMsg: OutMsg<T>) {
        super(response, outMsg);
    }

    protected getType(): string {
        return this._response.type?.toUpperCase() || 'UNDEFINED';
    }

    protected getText(): string {
        return this._response.text;
    }

    protected getData(): string {
        return JSON.stringify(this._response);
    }

    protected getPlatform(): string {
        return 'LINE';
    }
}
