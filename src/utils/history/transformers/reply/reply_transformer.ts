import { OutMsg, OutContent } from "../../../../strategies/definitions";
import ITransformer from "../i_transformer";
import { getRandomInt } from "../../../tools";

export default abstract class ReplyTransformer<T extends OutContent> implements ITransformer {
    private readonly _recipientId: string;
    protected readonly _response: any;
    private readonly _outMsg: OutMsg<T>;
    public constructor(response: any, outMsg: OutMsg<T>) {
        this._response = response;
        this._outMsg = outMsg;
        this._recipientId = this._outMsg.user.id;
    }

    toModel(): any {
        return {
            chatId: this._recipientId,
            senderId: 'BOT',
            recipientId: this._recipientId,
            time: this.getTime(),
            type: this.getType(),
            text: this.getText(),
            data: this.getData(),
            outMsg: JSON.stringify(this._outMsg),
            platform: this.getPlatform(),
            strategy: this._outMsg.strategy,
        };
    };

    private getTime() {
        return (this._outMsg.timeStamp || Date.now()) + getRandomInt(100, 0);
    }

    protected abstract getType(): string;

    protected abstract getText(): string;

    protected abstract getData(): string;

    protected abstract getPlatform(): string;
}
