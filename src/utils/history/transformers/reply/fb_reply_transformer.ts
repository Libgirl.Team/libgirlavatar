import { OutMsg, OutContent } from "../../../../strategies/definitions";
import ReplyTransformer from "./reply_transformer";

export default class FbReplyTransformer<T extends OutContent> extends ReplyTransformer<T> {
    private readonly _message: any;

    constructor(response: any, outMsg: OutMsg<T>) {
        super(response, outMsg);
        this._message = response.message;
    }
    protected getType(): string {
        if (this._response?.text) {
            return 'TEXT';
        }
        if (this._response?.attachment) {
            return 'ATTACHMENT';
        }
        return 'UNDEFINED';
    }

    protected getText(): string {
        return this._response.text;
    }

    protected getData(): string {
        return JSON.stringify(this._response);
    }

    protected getPlatform(): string {
        return 'FB';
    }
};
