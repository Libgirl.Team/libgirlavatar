import FbMessageTransformer from "./transformers/message/fb_message_transformer";
import FbReplyTransformer from "./transformers/reply/fb_reply_transformer";
import { ImitateTypingOutMsg, InMsg, Interval, MessageModel, OutContent, OutMsg } from '../../strategies/definitions';
import { staffIds } from "./user";
import { LineMessageTransformer } from "./transformers/message/line_message_transformer";
import LineReplyTransformer from "./transformers/reply/line_reply_transformer";
import MessageRepository from "../../repositories/message-repository";
import SessionRepository from "../../repositories/session-repository";
import AdvertisementRepository from "../../repositories/advertisement-repository";
import SummaryRepository from "../../repositories/summary-repository";

const adRepo = new AdvertisementRepository();
const messageRepo = new MessageRepository();
const sessionRepo = new SessionRepository();
const summaryRepo = new SummaryRepository();

function shouldSkipSaving<T extends OutContent>(outMsg: OutMsg<T>) {
    return outMsg instanceof ImitateTypingOutMsg;
}

async function saveMessage(messageModel: MessageModel) {
    try {
        return await messageRepo.save(messageModel);
    } catch (e) {
        console.error(e);
    }
}

function saveReply<T extends OutContent>(response: any, outMsg: OutMsg<T>) {
    if (shouldSkipSaving(outMsg)) {
        return;
    }
    const transformer = new FbReplyTransformer(response, outMsg);
    return saveMessage(transformer.toModel());
}

function saveLineReply<T extends OutContent>(response: any, outMsg: OutMsg<T>) {
    if (shouldSkipSaving(outMsg)) {
        return;
    }
    const transformer = new LineReplyTransformer(response, outMsg);
    return saveMessage(transformer.toModel());
}

const saveFBMessage = (entry: any, inMsg?: InMsg) => {
    const transformer = new FbMessageTransformer(entry, inMsg);
    return saveMessage(transformer.toModel());
};

const saveLineMessage = (event: any, inMsg?: InMsg) => {
    const transformer = new LineMessageTransformer(event, inMsg);
    return saveMessage(transformer.toModel());
};

function isUserInteracting(last: { senderId?: string; type?: string } | undefined) {
    return last && !isBot(last) && !shouldBeSkipped(last);
}

function isBot(message: any) {
    return message.senderId === 'BOT';
}

function isSwitchTurn(message: any, last: { senderId?: string; type?: string } | undefined) {
    return isBot(message) && isUserInteracting(last);
}

async function getUsersMessagesMap(start: number, end: number) {
    const messages = await messageRepo.all(start, end);
    const map = new Map();
    messages.forEach((e: any) => {
        const { chatId } = e;
        if (map.has(chatId)) {
            map.get(chatId).push(e);
        } else {
            map.set(chatId, [e]);
        }
    });
    return map;
}

function shouldBeSkipped(message: any) {
    return ['REFERRAL', 'FOLLOW', 'UNFOLLOW'].includes(message.type) || message.payload === 'getStarted';
}

async function updateReferral(message: any) {
    const ad = message.referral?.ad_id;
    if (!ad) {
        return;
    }

    let adList = await adRepo.all();
    if (!adList.includes(ad)) {
        await adRepo.save(ad);
    }
    return adRepo.saveUser(ad, message.chatId);
}

const updateSession = async function (start: number, end: number) {
    console.log('Start get users messages', new Date());
    const map = await getUsersMessagesMap(start, end);
    console.log('Start count switch turn', new Date());
    for (let messages of map.values()) {
        let lastMessage: any = undefined;
        for (let message of messages) {
            if (shouldBeSkipped(message)) {
                await updateReferral(message);
                continue;
            }

            const after = message.time - 300000;
            const startFrom = message.time - 24 * 60 * 60 * 1000;
            const session: any = await sessionRepo.findLast(message.chatId, startFrom, after);
            if (message.senderId !== 'BOT') {
                await sessionRepo.createOrUpdate(session, message);
                lastMessage = message;
                continue;
            }

            if (session && isSwitchTurn(message, lastMessage)) {
                if (message.time > session.lastMessageAt) {
                    await sessionRepo.addSwitchTurn(message.chatId, session.start, message.time);
                }
            }
            lastMessage = message;
        }
    }
    console.log('End', new Date());
};

const scheduleUpdateSessions = async function () {
    let summary = await summaryRepo.getOrNewScheduleSession();

    console.log('Start from ', summary.executeTo + 1);
    // Due to message webhook not real time, only calculate 15 min before
    const end = Date.now() - 15 * 60 * 1000;
    await updateSession(summary.executeTo + 1, end);
    summary.executeTo = end;
    await summary.save();
};

const getSessionsWithoutStaffs = async () => {
    const result = await sessionRepo.all();
    return result.filter((e: any) => !isStaff(e.chatId));
};

function isInterval(end: any, start: any) {
    return (e: any) => (end >= e.start && e.start >= start) || (end >= e.end && e.end >= start);
}

function groupByUser(userSessionMap: Map<string, any>, session: any) {
    if (userSessionMap.has(session.chatId)) {
        userSessionMap.get(session.chatId).push(session);
    } else {
        userSessionMap.set(session.chatId, [session]);
    }
    return userSessionMap;
}

const scheduleUpdateUserCanShare = async () => {
    const { start, end: configEnd, switchTurnThreshold, sessionThreshold } = await summaryRepo.getOrCreateShare('CONFIG');
    const end = configEnd || Date.now();

    const data: any = await getSessionsWithoutStaffs();
    const userSessionMap = data.filter(isInterval(end, start)).reduce(groupByUser, new Map());

    for (let [userId, userSessions] of userSessionMap) {
        if (userSessions.length >= sessionThreshold) {
            console.log('session pass', userSessions.length, userId);
            await summaryRepo.saveUserCanShare(userId);
            continue;
        }
        for (let each of userSessions) {
            if (each.switchTurnCount >= switchTurnThreshold) {
                console.log('switchTurn pass', each.switchTurnCount, userId);
                await summaryRepo.saveUserCanShare(userId);
                break;
            }
        }
    }
};

function taiwanDate(time: number): string {
    const options = { year: 'numeric', month: '2-digit', day: '2-digit', timeZone: 'Asia/Taipei' };
    return new Date(time).toLocaleDateString('en-US', options);
}

const calculateAllCount = async () => {
    const data: any = await getSessionsWithoutStaffs();
    const userSessionMap = data.reduce(groupByUser, new Map());
    let userCount = 0;
    let sessionCount = 0;
    let switchTurnCount = 0;

    const switchTurnUserMap: any = {};
    const dailyNewUserMap: any = {};
    userSessionMap.forEach((sessions: any[]) => {
        userCount++;
        sessionCount += sessions.length;

        const comingDate = taiwanDate(sessions[0].start);
        if (dailyNewUserMap.hasOwnProperty(comingDate)) {
            dailyNewUserMap[comingDate]++;
        } else {
            dailyNewUserMap[comingDate] = 1;
        }

        const count = sessions.reduce((prev: number, curr: any) => prev + curr.switchTurnCount, 0);
        if (switchTurnUserMap.hasOwnProperty(count)) {
            switchTurnUserMap[count]++;
        } else {
            switchTurnUserMap[count] = 1;
        }
        switchTurnCount += count;
    });

    const dateSwitchTurnMap: { [propName: string]: any } = data.reduce((prev: any, session: any) => {
        const date = taiwanDate(session.start);
        const duration = session.end - session.start;
        if (prev.hasOwnProperty(date)) {
            prev[date].users.add(session.chatId);
            prev[date].switchTurnCount += session.switchTurnCount;
            prev[date].duration += duration;
        } else {
            prev[date] = {
                users: new Set([session.chatId]),
                switchTurnCount: session.switchTurnCount,
                duration,
            };
        }
        return prev;
    }, {});

    const dailyData: { [propName: string]: any } = {};
    for (let [date, each] of Object.entries(dateSwitchTurnMap)) {
        dailyData[date] = {
            turnCount: each.switchTurnCount,
            userCount: each.users.size,
            avgDuration: Math.round(each.duration / each.users.size),
        };
    }

    return {
        userCount,
        sessionCount,
        switchTurnCount,
        switchTurnUserMap,
        dailyData,
        retention: await calculateRetention(allWeeks(), userSessionMap),
        dailyRetention: await calculateRetention(allDays(), userSessionMap),
        dailyNewUserMap,
        updateAt: Date.now(),
    }
};

function allWeeks() {
    const weeks = [];
    const start = new Date('2019-11-28 10:00:00z').getTime();

    const weekInterval = 7 * 24 * 60 * 60 * 1000;
    let weekStart = start;
    for (let i = 1; weekStart < Date.now(); i++) {
        weeks.push({ index: i, start: weekStart, end: weekStart + weekInterval });
        weekStart += weekInterval;
    }
    return weeks;
}

function allDays() {
    const days: any[] = [];
    const start = new Date('2019-11-28 10:00:00z').getTime();

    const dayInterval = 24 * 60 * 60 * 1000;
    let dayStart = start;
    for (let i = 1; dayStart < Date.now(); i++) {
        days.push({ index: i, start: dayStart, end: dayStart + dayInterval });
        dayStart += dayInterval;
    }
    return days;
}

function findInterval(intervals: Interval[], time: number): Interval {
    const result = intervals.find((w: any) => w.end >= time && time >= w.start);
    if (result === undefined) {
        throw new Error(`undefined interval: ${time}`);
    }
    return result;
}

function calculateRetention(intervals: Interval[], userSessionMap: Map<string, any>) {
    const result = new Array(intervals.length);
    for (let i = 0; i < result.length; i++) {
        result[i] = new Array(result.length).fill(0);
    }

    for (let [, sessions] of userSessionMap) {
        const firstSession = sessions[0];
        const startInterval = findInterval(intervals, firstSession.start);
        const userIntervalCount = new Array(result.length).fill(0);
        sessions.forEach((session: any) => {
            const w: Interval = findInterval(intervals, session.start);
            userIntervalCount[w.index - startInterval.index] = 1;
        });

        const targetInterval: number[] = result[startInterval.index - 1];
        for (let index in targetInterval) {
            targetInterval[index] += userIntervalCount[index];
        }
    }
    return result;
}

const isStaff = (userId: string): boolean => {
    return staffIds.includes(userId);
};

export {
    saveFBMessage,
    saveReply,
    scheduleUpdateSessions,
    saveLineMessage,
    saveLineReply,
    scheduleUpdateUserCanShare,
    calculateAllCount,
    isStaff,
}
