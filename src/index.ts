const { router, platform } = require('bottender/router');
import * as lineHdl from './utils/line/line_handler';
import { handleMessage } from './handle-msg';
import * as lineRpl from './utils/line/line_replier';
import { LineContext, MessengerContext } from 'bottender';
import { saveFBMessage, saveLineMessage } from "./utils/history";
import * as fbHdl from './utils/fb/fb_handler';
import * as fbRpl from './utils/fb/fb_replier';

console.log('Import Action');

async function App(context: LineContext | MessengerContext) {
    console.log('context', context);
    if (process.env.FUNCTIONALITY === 'typing') {
        return context.typing(2000);
    } else {
        // // for testing session
        // const ses = context.state;
        // if (ses.note) {
        //     console.log(`note from session: ${ses.note}.`);
        // } else {
        //     context.setState({ note: 'SESSION_INITIALIZED'});
        //     console.log('set sessions.note for new user.');
        // }
        return router([
            platform('line', LineAction),
            platform('messenger', FbAction),
        ] as any[])
    }
}

async function LineAction(context: any): Promise<any> {
    const parsed = lineHdl.parseIn(context.event);

    if (parsed) {
        saveLineMessage(context.event.rawEvent, parsed);
        return handleMessage(parsed, lineRpl.makeReplier(context));
    }
}

async function FbAction(context: any): Promise<any> {
    const parsed = fbHdl.parseIn(context.event);

    if (parsed) {
        saveFBMessage(context.event.rawEvent, parsed);
        return handleMessage(parsed, fbRpl.genReplier(context));
    }
}

module.exports = App;
