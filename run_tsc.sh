### commands:
### response - for messsage-response
### verify - for messenger-webhook
### express - for run aws-serverless-express
### typing - for imitate-typing
### summary - for summary
### coupon - for updain coupon
### coupon-server - for updae coupon server only
### redirect - for redirect-url
### queue - for push-queue
### modules - for layer nodejs

if [ "$1" == "" ]
then
    opt=all
else
    opt=$1
fi

# clean
rm -rf dist/strategies dist/utils dist/config dist/repositories dist/*.js dist/*.zip
mkdir -p dist/config
cp config/production.json dist/config/

## compile

BOT_NOT_COMPILED=true
TscBotIndex () {
    if $BOT_NOT_COMPILED
    then
        tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/index.ts --outDir dist
        cp bottender.config.js  ./dist/.
        BOT_NOT_COMPILED=false
        echo "tsc bot index compile done"
    else
        echo "tsc bot compiled"
    fi
}

# tsc src/lambda_verify_webhook.ts --outDir dist
# tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/aws-lambda-webhook.ts --outDir dist
# tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/lambda_imitate_typing.ts --outDir dist
# tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/summary.ts --outDir dist
# tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/coupon.ts --outDir dist
# tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/redirect-url.ts --outDir dist
# tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/index.ts --outDir dist
# tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/lambda.ts --outDir dist


if [ $opt == "response" ] || [ $opt == "all" ] || [ $opt == "modules" ] || [ $opt == "coupon" ]  # for message-response
then
    echo "compile for response"
    tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/lambda.ts --outDir dist
    TscBotIndex
fi

if [ $opt == "verify" ] || [ $opt == "all" ] || [ $opt == "modules" ]
then
    tsc src/lambda_verify_webhook.ts --outDir dist
fi

if [ $opt == "express" ] || [ $opt == "all" ] || [ $opt == "modules" ]
then
    tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/aws-lambda-webhook.ts --outDir dist
    TscBotIndex
fi

if [ $opt == "typing" ] || [ $opt == "all" ] || [ $opt == "modules" ]
then
    tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/lambda_imitate_typing.ts --outDir dist
    TscBotIndex
fi

if [ $opt == "summary" ] || [ $opt == "all" ] || [ $opt == "modules" ]
then
    tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/summary.ts --outDir dist
fi

if [ $opt == "coupon" ] || [ $opt == "all" ] || [ $opt == "modules" ] || [ $opt == "coupon-server" ]
then
    tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/coupon.ts --outDir dist
fi

if [ $opt == "redirect" ] || [ $opt == "all" ] || [ $opt == "modules" ]
then
    tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/redirect-url.ts --outDir dist
fi

if [ $opt == "queue" ] || [ $opt == "all" ] || [ $opt == "modules" ]
then
   tsc --esModuleInterop --resolveJsonModule --downlevelIteration src/lambda-queue-pusher.ts --outDir dist
fi

## make zips

if [ $opt == "response" ] || [ $opt == "all" ] || [ $opt == "coupon" ] 
then
    cd dist && zip -r message-webhook.zip lambda.js index.js bottender.config.js handle-msg.js replier.js libgirl-avatar-* strategies utils config repositories && cd ..
fi

if [ $opt == "verify" ] || [ $opt == "all" ]
then
    cd dist && zip fb-verify-webhook.zip lambda_verify_webhook.js && cd ..
fi


if [ $opt == "typing" ] || [ $opt == "all" ]
then
    cd dist && zip -r fb-imitate-typing.zip lambda_imitate_typing.js bottender.config.js handle-msg.js replier.js requestHandler.js libgirl-avatar-* strategies utils config repositories  && cd ..
fi

if [ $opt == "summary" ] || [ $opt == "all" ]
then
    cd dist && zip -r summary.zip summary.js strategies utils repositories && cd ..
fi

if [ $opt == "coupon" ] || [ $opt == "all" ] || [ $opt == "coupon-server" ]
then
    cd dist && zip -r coupon.zip coupon.js strategies repositories utils && cd ..
fi

if [ $opt == "redirect" ] || [ $opt == "all" ]
then
    cd dist && zip -r redirect-url.zip redirect-url.js strategies repositories utils && cd ..
fi

if [ $opt == "queue" ] || [ $opt == "all" ]
then
    cd dist && zip -r push-queue.zip lambda-queue-pusher.js queue-pusher.js strategies utils repositories && cd ..
fi

if [ $opt == "modules" ] || [ $opt == "all" ]
then
    create layer for node_modules/
    cd dist
    mkdir nodejs
    mv ../node_modules/ nodejs/.
    zip -r nodejs.zip nodejs -x "*/ngrok/*"
    mv nodejs/node_modules ../.
    cd ..
fi





