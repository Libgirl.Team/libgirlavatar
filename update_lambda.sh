#!/usr/bin/env bash
bash run_tsc.sh
aws lambda update-function-code \
  --function-name test-function \
  --zip-file fileb://dist/fb-verify-webhook.zip

aws lambda update-function-code \
  --function-name message-repeater \
  --zip-file fileb://dist/fb-message-webhook.zip

